<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelOrder extends CI_Model
{
    // start datatables staff
    var $column_order = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $column_search = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $order = array('a.id' => 'DESC');

    private function _get_dt_query_order_staff($mode)
    {
        $this->db->select("	b.name AS nama_sales_kedai,
                            c.package AS name_package,
                            d.`value` AS name_type,
                        IF
                            ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                            DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                            a.* ");
        $this->db->from('tb_eform as a');
        $this->db->join('tb_user as b', 'a.name_kedai = b.id', 'left');
        $this->db->join('tb_package as c', 'a.package = c.id_pkg', 'left');
        $this->db->join('tb_type d', 'a.type = d.id', 'left');
        if ($mode == 'waiters-list') {
            $wl = array('port-full', 'upcoming-projects');
            $this->db->where_in('a.status', $wl);
        }else{
            $this->db->where('a.status', $mode);
        }
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function dt_order_staff($mode)
    {
        $this->_get_dt_query_order_staff($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_by_staff($mode)
    {
        $this->_get_dt_query_order_staff($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_by_staff($mode)
    {
        $this->db->from('tb_eform');
        $this->db->where('status', $mode);
        return $this->db->count_all_results();
    }
    // end datatables

    // start datatables no staff
    var $column_order_no_staff = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $column_search_no_staff = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $order_no_staff = array('a.id' => 'DESC');

    private function _get_dt_query_order_no_staff($mode)
    {
        $hash = $this->session->hash;
        $id_kedai = $this->db->query("SELECT id_kedai FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;

        $this->db->select("	b.name AS nama_sales_kedai,
                            c.package AS name_package,
                            d.`value` AS name_type,
                        IF
                            ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                            DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                            a.* ");
        $this->db->from('tb_eform as a');
        $this->db->join('tb_user as b', 'a.name_kedai = b.id', 'left');
        $this->db->join('tb_package as c', 'a.package = c.id_pkg', 'left');
        $this->db->join('tb_type as d', 'a.type = d.id', 'left');
        $this->db->where('a.name_sales', $id_kedai);
        if ($mode == 'waiters-list') {
            $wl = array('port-full', 'upcoming-projects');
            $this->db->where_in('a.status', $wl);
        }else{
            $this->db->where('a.status', $mode);
        }
        $i = 0;
        foreach ($this->column_search_no_staff as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search_no_staff) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order_no_staff[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_no_staff)) {
            $order = $this->order_no_staff;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function dt_order_no_staff($mode)
    {
        $this->_get_dt_query_order_no_staff($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_no_staff($mode)
    {
        $this->_get_dt_query_order_no_staff($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_no_staff($mode)
    {
        $hash = $this->session->hash;
        $id_kedai = $this->db->query("SELECT id_kedai FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;
        $this->db->from('tb_eform');
        $this->db->where('name_sales', $id_kedai);
        $this->db->where('status', $mode);
        return $this->db->count_all_results();
    }
    // end datatables no staff

    // start datatables individu
    var $column_order_individu = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $column_search_individu = array('a.id', 'a.name_customer', 'a.order_number', 'b.name', 'c.package', 'a.segment', 'a.commision', 'a.status', 'a.remarks', 'a.input_date');
    var $order_individu = array('a.id' => 'DESC');

    private function _get_dt_query_order_individu($mode)
    {
        $hash = $this->session->hash;
        $id_kedai = $this->db->query("SELECT id_kedai FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;

        $this->db->select("	b.name AS nama_sales_kedai,
                            c.package AS name_package,
                            d.`value` AS name_type,
                        IF
                            ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                            DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                            a.* ");
        $this->db->from('tb_eform as a');
        $this->db->join('tb_user as b', 'a.name_kedai = b.id', 'left');
        $this->db->join('tb_package as c', 'a.package = c.id_pkg', 'left');
        $this->db->join('tb_type as d', 'a.type = d.id', 'left');
        $this->db->where('a.name_sales', $id_kedai);
        if ($mode == 'waiters-list') {
            $wl = array('port-full', 'upcoming-projects');
            $this->db->where_in('a.status', $wl);
        }else{
            $this->db->where('a.status', $mode);
        }
        $i = 0;
        foreach ($this->column_search_individu as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search_individu) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order_individu[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order_individu)) {
            $order = $this->order_individu;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function dt_order_individu($mode)
    {
        $this->_get_dt_query_order_individu($mode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered_individu($mode)
    {
        $this->_get_dt_query_order_individu($mode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all_individu($mode)
    {
        $hash = $this->session->hash;
        $id_kedai = $this->db->query("SELECT id_kedai FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;
        $this->db->from('tb_eform');
        $this->db->where('name_sales', $id_kedai);
        $this->db->where('status', $mode);
        return $this->db->count_all_results();
    }
    // end datatables no individu

    public function get_order_by_id($id)
    {
        $q = $this->db->query("SELECT
                                SHA1( a.id ) AS id_sha,
                                b.nama AS nama_sales_kedai,
                                c.package AS name_package,
                                d.`value` AS name_type,
                            IF
                                ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                                DATE_FORMAT( a.installation_date, '%d %b, %Y' ) AS new_install,
                                DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                                a.* 
                            FROM
                                tb_eform a
                                LEFT JOIN tb_kedai b ON a.name_kedai = b.id
                                LEFT JOIN tb_package c ON a.package = c.id_pkg
                                LEFT JOIN tb_type d ON a.type = d.id 
                            WHERE
                                a.id = '$id'
                            ");
        return $q->row();
    }

    public function get_order_by_idsha($id)
    {
        $q = $this->db->query("SELECT
                                SHA1( a.id ) AS id_sha,
                                b.id_kedai AS kode,
                                b.`level`,
                                IFNULL( b2.nama_perniagaan, c1.nama ) AS name_sales_person,
                                IFNULL( b2.email, c1.email ) AS to_email,
                                c.package AS name_package,
                                d.`value` AS name_type,
                            IF
                                ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                                DATE_FORMAT( a.installation_date, '%d %b, %Y' ) AS new_install,
                                DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                                a.* 
                            FROM
                                tb_eform a
                                LEFT JOIN tb_user b on a.name_sales = b.id_kedai
                                LEFT JOIN tb_kedai b2 ON a.name_sales = b2.kode_kedai
	                            LEFT JOIN tb_sales c1 ON a.name_sales = c1.kode_individu
                                LEFT JOIN tb_package c ON a.package = c.id_pkg
                                LEFT JOIN tb_type d ON a.type = d.id 
                            WHERE
                                sha1(a.id) = '$id'
                            ");
        return $q->row();
    }

    public function save_edit_order()
    {
        $id = $this->input->post('id', true);
        $order_number = $this->input->post('order_number', true);
        $number_ic = $this->input->post('number_ic', true);
        $segment = $this->input->post('segment', true);
        $name_customer = $this->input->post('name_customer', true);
        $contact1 = $this->input->post('contact1', true);
        $contact2 = $this->input->post('contact2', true);
        $email = $this->input->post('email', true);
        $install_address = $this->input->post('install_address', true);
        $billing_address = $this->input->post('billing_address', true);
        $package = $this->input->post('package', true);
        $type = $this->input->post('type', true);
        $comission = $this->input->post('comission', true);
        $remarks = $this->input->post('remarks', true);

        $this->db->where('id', $id);
        $this->db->update('tb_eform', array(
            'order_number' => $order_number,
            'number_ic' => $number_ic,
            'segment' => $segment,
            'name_customer' => $name_customer,
            'contact_1' => $contact1,
            'contact_2' => $contact2,
            'email' => $email,
            'address' => $install_address,
            'billing_address' => $billing_address,
            'package' => $package,
            'type' => $type,
            'commision' => $comission,
            'remarks' => $remarks
        ));

        $order_num = $this->db->query("SELECT order_number as no FROM tb_eform where id = '$id'")->row()->no;
        $this->db->insert('tb_history', array(
            'note_1' => 'Update data order',
            'note_2' => $remarks,
            'status' => 'update',
            'id' => $id,
            'order_number' => $order_num,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
    }

    function save_package()
    {
        $data['type'] = $this->input->post('type');
        $data['package'] = $this->input->post('package');
        $data['active'] = 'Y';
        $data['user_input'] = $this->session->hash;
        $data['date_input'] = date('Y-m-d H:i:s');
        $this->db->insert('tb_package', $data);
    }
}
