<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelUser extends CI_Model
{

    // start datatables sales
    var $column_order = array('a.id', 'a.level', 'a.id_kedai', 'a.username', 'a.verify', 'a.date_input', 'a.date_edit');
    var $column_search = array('a.level', 'a.id_kedai', 'a.username');
    var $order = array('a.id' => 'desc');

    private function _get_query()
    {
        $this->db->select("a.*, DATE_FORMAT(a.date_input,'%d %b, %Y %h:%i %p') AS `input`, DATE_FORMAT(a.date_edit,'%d %b, %Y %h:%i %p') AS last_update");
        $this->db->from('tb_user as a');
        $this->db->where('a.active', 'Y');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_user');
        return $this->db->count_all_results();
    }
    // end datatables sales

    public function get($username)
    {
        $this->db->where('username', $username);
        $this->db->where('active', 'Y');
        $q = $this->db->get('tb_user')->row();
        return $q;
    }

    public function get_level_by_id($id)
    {
        $q = $this->db->select('*')
            ->from('tb_user')
            ->where('id', $id)
            ->get()
            ->row();
        return $q;
    }

    public function get_level_by_kode($id_kode)
    {
        $q = $this->db->select('*')
            ->from('tb_user')
            ->where('id_kedai', $id_kode)
            ->get()
            ->row();
        return $q;
    }
}
