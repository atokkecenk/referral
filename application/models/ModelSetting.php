<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelSetting extends CI_Model
{
    public function get_setting()
    {
        $this->db->where('id', 1);
        $q = $this->db->get('tb_setting');
        return $q;
    }

    public function get_all_code()
    {
        $q = $this->db->select("a.*, DATE_FORMAT(a.edit_date,'%d %b, %Y %h:%i %p') AS new_edit_date")
            ->from('tb_id_code a')
            ->get()
            ->result();
        return $q;
    }

    public function get_all_package()
    {
        $q = $this->db->select("a.*, DATE_FORMAT(a.date_input,'%d %b, %Y %h:%i %p') AS new_date")
            ->from('tb_package a')
            ->where('a.active', 'Y')
            ->get()
            ->result();
        return $q;
    }

    public function cek_maintenance()
    {
        $q = $this->db->select('maintenance')
            ->from('tb_setting')
            ->get()
            ->row();
        return $q;
    }
}
