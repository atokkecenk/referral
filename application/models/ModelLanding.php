<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelLanding extends CI_Model
{

    public function get_setting()
    {
        $q = $this->db->get('tb_setting')->row();
        return $q;
    }

    public function search()
    {
        $filter = $this->input->post('filter');
        $search = $this->input->post('search');

        if ($search !== '') {
            $whr1 = "name_sales_person LIKE '%$search%'";
        } else {
            $whr1 = null;
        }

        if ($filter !== '') {
            $whr2 = "AND b.`level` = '$filter'";
        } else {
            $whr2 = null;
        }

        $q = $this->db->query("SELECT
                                COUNT( b.id_kedai ) AS `hitung`,
                                b.id_kedai AS kode,
                                b.`level`,
                                IFNULL(b2.no_pendaftaran_syarikat,c1.no_kad_pengenal) as nomor,
                                IFNULL(b2.hp,c1.hp) as no_hp,
                                IFNULL(b2.alamat,c1.alamat) as alamat,
                                IFNULL( b2.nama_perniagaan, c1.nama ) AS name_sales_person,
                                c.package AS name_package,
                                d.`value` AS name_type,
                            IF
                                ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                                DATE_FORMAT( a.installation_date, '%d %b, %Y' ) AS new_install,
                                DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                                IFNULL( b2.foto, c1.foto ) AS foto,
                                a.*
                            FROM
                                tb_eform a
                                LEFT JOIN tb_user b ON a.name_sales = b.id_kedai
                                LEFT JOIN tb_kedai b2 ON a.name_sales = b2.kode_kedai
                                LEFT JOIN tb_sales c1 ON a.name_sales = c1.kode_individu
                                LEFT JOIN tb_package c ON a.package = c.id_pkg
                                LEFT JOIN tb_type d ON a.type = d.id
                            GROUP BY
	                            b.id_kedai
                            HAVING
                                $whr1
                                $whr2
                            ");
        return $q;
    }
}
