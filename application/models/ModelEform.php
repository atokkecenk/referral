<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelEform extends CI_Model
{
    public function get_data_user($id)
    {
        $data = $this->db->select('*')
            ->from('tb_user')
            ->where('id_kedai', $id)
            ->get();
        return $data;
    }

    public function get_all_type()
    {
        $data = $this->db->select('*')
            ->from('tb_type')
            ->where('active', 'Y')
            ->get()
            ->result();
        return $data;
    }

    public function get_type_by_id($id)
    {
        $data = $this->db->select('*')
            ->from('tb_type')
            ->where('id', $id)
            ->get()
            ->row();
        return $data;
    }


    public function get_segment($type)
    {
        $data = $this->db->select('*')
            ->from('tb_package')
            ->where('type', $type)
            ->where('active', 'Y')
            ->get()
            ->result();
        return $data;
    }

    public function get_package_by_id($id)
    {
        $data = $this->db->select('*')
            ->from('tb_package')
            ->where('id_pkg', $id)
            ->get()
            ->row();
        return $data;
    }

    public function cek_duplicate_ic($number_ic)
    {
        $data = $this->db->select('*')
            ->from('tb_eform')
            // ->where('status', 'pending')
            ->where('number_ic', $number_ic)
            ->get();
        return $data;
    }

    public function save()
    {
        $kedai = $this->input->post('kedai');
        $name_sales = $this->input->post('name_sales');
        $name_customer = $this->input->post('name_customer');
        $number_ic = $this->input->post('number_ic');
        $segment = $this->input->post('segment');
        $contact1 = $this->input->post('contact1');
        $contact2 = $this->input->post('contact2');
        $email = $this->input->post('email');
        $install_address = $this->input->post('install_address');
        $billing_address = $this->input->post('billing_address');
        $package = $this->input->post('package');
        $type = $this->input->post('type');
        $del_number = $this->input->post('del_number');
        $date = $this->input->post('date');
        $extr = explode('/', $date);
        $new_date = $extr[2] . '-' . $extr[1] . '-' . $extr[0];

        $data = array(
            'name_kedai' => $kedai,
            'name_sales' => $name_sales,
            'name_customer' => $name_customer,
            'number_ic' => $number_ic,
            'contact_1' => $contact1,
            'contact_2' => $contact2,
            'segment' => $segment,
            'email' => $email,
            'address' => $install_address,
            'billing_address' => $billing_address,
            'package' => $package,
            'type' => $type,
            'del_number' => $del_number,
            'installation_date' => $new_date,
            'status' => 'pending',
            'input_date' => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_eform', $data);
    }
}
