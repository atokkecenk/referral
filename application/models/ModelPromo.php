<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelPromo extends CI_Model
{
    function get_data_promo()
    {
        return $this->db->query("SELECT
                                    a.id,
                                IF
                                    (
                                        a.kategori = 'agent',
                                        UPPER( 'KEDAI' ),
                                    UPPER( 'SALES' )) AS kategori,
                                    a.kode_user,
                                    UPPER(
                                    IFNULL( b.nama_perniagaan, c.nama )) AS nama,
                                    a.file,
                                IF
                                    ( a.`desc` IS NULL OR a.`desc` = '', '-', a.`desc` ) AS `desc`,
                                    -- a.`desc`,
                                    a.approval,
                                    a.reason,
                                    a.tgl_input,
                                    a.user_input 
                                FROM
                                    `tb_promo` a
                                    LEFT JOIN tb_kedai b ON a.kode_user = b.kode_kedai
                                    LEFT JOIN tb_sales c ON a.kode_user = c.kode_individu 
                                ORDER BY
                                    a.id DESC");
    }

    function get_promo_approved($whr = null)
    {
        if ($whr !== '') {
            $whr = " HAVING nama like '%$whr%'";
        }
        return $this->db->query("SELECT
                                    a.kategori,
                                    a.kode_user,
                                    IFNULL(
                                        UPPER( c.nama_perniagaan ),
                                    UPPER( d.nama )) AS nama,
                                    a.`desc`,
                                    a.wa_number,
                                    a.file,
                                    a.approval 
                                FROM
                                    `tb_promo` a
                                    LEFT JOIN tb_user b ON a.user_input = b.`hash`
                                    LEFT JOIN tb_kedai c ON a.kode_user = c.kode_kedai
                                    LEFT JOIN tb_sales d ON a.kode_user = d.kode_individu 
                                WHERE
                                    a.approval = 'Y' 
                                    $whr
                                ORDER BY
                                    a.tgl_input DESC
                                    ");
    }

    function get_promo_by_user($hash = null)
    {
        if (in_array($this->session->level, ['agent', 'individu'])) {
            $where = "WHERE a.user_input = '$hash'";
        }else{
            $where = null;
        }
        return $this->db->query("SELECT
                                    a.*,
                                    DATE_FORMAT( a.tgl_input, '%d %b %Y,<br>%H:%i %p' ) AS input,
                                IF
                                    ( a.date_approved IS NOT NULL, DATE_FORMAT( a.date_approved, '%d %b %Y,<br>%H:%i %p' ), '-' ) AS approve 
                                FROM
                                    `tb_promo` a 
                                $where
                                ORDER BY
                                    id DESC");
    }

    function get_slider()
    {
        return $this->db->query("SELECT
                                    * 
                                FROM
                                    `tb_slider` 
                                WHERE
                                    na = 'N' 
                                ORDER BY
                                    id DESC");
    }
}
