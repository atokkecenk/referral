<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelKedai extends CI_Model
{
    // start datatables kedai
    var $column_order = array('a.id', 'a.nama', 'a.no_kad_pengenal', 'a.nama_perniagaan', 'a.no_pendaftaran_syarikat', 'a.jantina', 'a.hp', 'a.alamat', 'a.email', 'a.foto', 'b.nama_bank', 'a.no_account_bank');
    var $column_search = array('a.nama', 'a.no_kad_pengenal', 'a.nama_perniagaan', 'a.hp', 'a.email', 'b.nama_bank', 'a.no_account_bank');
    var $order = array('a.id' => 'asc');

    private function _get_query_kedai()
    {
        $this->db->select("a.*, b.nama_bank");
        $this->db->from('tb_kedai as a');
        $this->db->join('tb_bank as b', 'a.bank = b.id', 'left');
        $this->db->where('a.active', 'Y');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_kedai()
    {
        $this->_get_query_kedai();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_query_kedai();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_kedai');
        $this->db->where('active', 'Y');
        return $this->db->count_all_results();
    }
    // end datatables kedai

    public function get_negeri()
    {
        $q = $this->db->select('*')
            ->from('tb_negeri')
            ->where('active', 'Y')
            ->get()
            ->result();
        return $q;
    }

    public function get_daerah()
    {
        $data = $this->db->select('*')
            ->from('tb_daerah')
            ->where('active', 'Y')
            ->get()
            ->result();
        return $data;
    }

    public function get_daerah_by_id($id)
    {
        $data = $this->db->select('*')
            ->from('tb_daerah')
            ->where('id_negeri', $id)
            ->where('active', 'Y')
            ->get()
            ->result();
        return $data;
    }

    public function get_daerah_by_whr($params)
    {
        $data = $this->db->select('*')
            ->from('tb_daerah')
            ->where('id_negeri', $params)
            ->where('active', 'Y')
            ->get()
            ->result();
        return $data;
    }

    public function get_data_kedai_by_sha($params)
    {
        $q = $this->db->select("b.id_negeri,
                                b.nama AS nama_negeri,
                                c.id_daerah,
                                c.nama AS nama_daerah,
                                a.*")
            ->from("tb_kedai a")
            ->join('tb_negeri b', 'a.negeri = b.id_negeri', 'left')
            ->join('tb_daerah c', 'a.daerah = c.id_daerah', 'left')
            ->where('sha1(a.id)', $params)
            ->get()->row();
        return $q;
    }
}
