<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelRegistration extends CI_Model
{

    public function get_all_bank()
    {
        $q = $this->db->from('tb_bank')
            ->where('active', 'Y')
            ->order_by('nama_bank', 'ASC')
            ->get();
        return $q;
    }

    function get_max_kode_kedai()
    {
        $this->db->select('max(RIGHT(kode_kedai,5)) AS `no`');
        $this->db->from('tb_kedai');
        $q = $this->db->get();
        return $q->row()->no;
    }

    function get_max_kode_individu()
    {
        $this->db->select('max(RIGHT(kode_individu,5)) AS `no`');
        $this->db->from('tb_sales');
        $q = $this->db->get();
        return $q->row()->no;
    }

    function get_max_hash_agent()
    {
        $this->db->select('max(RIGHT(hash,5)) AS `no`');
        $this->db->from('tb_user');
        $this->db->where('level', 'agent');
        $q = $this->db->get();
        return $q->row()->no;
    }

    function get_max_hash_individu()
    {
        $this->db->select('max(RIGHT(hash,5)) AS `no`');
        $this->db->from('tb_user');
        $this->db->where('level', 'individu');
        $q = $this->db->get();
        return $q->row()->no;
    }

    public function save_sales($qrcode, $verify)
    {
        $nama = $this->input->post('nama');
        $pengenal = $this->input->post('pengenal');
        $jantina = $this->input->post('jantina');
        $no_hp = $this->input->post('no_hp');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $negeri = $this->input->post('negeri');
        $daerah = $this->input->post('daerah');
        $bank = $this->input->post('bank');
        $no_account_bank = $this->input->post('no_account_bank');

        $nm_individu = str_replace(' ', '', $nama);
        $depan = $this->db->query("SELECT * FROM tb_id_code WHERE jenis = 'individu'")->row()->code_string;
        // $code = $this->db->query("SELECT MAX( RIGHT ( kode_individu, 5 ) ) AS `no` FROM tb_sales WHERE kode_individu LIKE '$depan%'")->row();
        $code = $this->db->query("SELECT MAX( RIGHT ( id_kedai, 5 ) ) AS `no` FROM tb_user WHERE id_kedai LIKE '$depan%'")->row();
        $code2 = '00000' . ($code->no + 1);
        $new_code = $depan . substr($code2, -5);

        $data = array(
            'kode_individu' => $new_code,
            'nama' => $nama,
            'no_kad_pengenal' => $pengenal,
            'jantina' => $jantina,
            'hp' => $no_hp,
            'alamat' => $alamat,
            'email' => $email,
            'bank' => $bank,
            'negeri' => $negeri,
            'daerah' => $daerah,
            'no_account_bank' => $no_account_bank,
            'qrcode' => $qrcode,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tb_sales', $data);

        $hash_individu = $this->get_max_hash_individu();
        $hash_individu2 = '00000' . ($hash_individu + 1);
        $new_hash_individu = 'individu' . substr($hash_individu2, -5);

        $data2 = array(
            'id_kedai' => $new_code,
            'username' => strtolower($nm_individu) . substr($code2, -5),
            'password' => 'bbf2dead374654cbb32a917afd236656',
            'name' => $nama,
            'hash' => $new_hash_individu,
            'level' => 'individu',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        );
        $this->db->insert('tb_user', $data2);

        $this->db->insert('tb_verify', array(
            'owner' =>  $new_code,
            'verify' => $verify,
            'used' => 'N',
            'date_input' => date('Y-m-d H:i:s')
        ));

        $this->db->insert('tb_history', array(
            'note_1' => 'Add new individu',
            'note_2' => 'sales',
            'status' => 'add-data',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
    }

    public function save_kedai($qrcode, $verify)
    {
        $nama = $this->input->post('nama');
        $pengenal = $this->input->post('pengenal');
        $jantina = $this->input->post('jantina');
        $no_hp = $this->input->post('no_hp');
        $jenis_perniagaan = $this->input->post('jenis_perniagaan');
        $nama_perniagaan = $this->input->post('nama_perniagaan');
        $no_syarikat = $this->input->post('no_syarikat');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $negeri = $this->input->post('negeri');
        $daerah = $this->input->post('daerah');
        $bank = $this->input->post('bank');
        $no_account_bank = $this->input->post('no_account_bank');

        $nm_kedai = str_replace(' ', '', $nama_perniagaan);
        $depan = $this->db->query("SELECT * FROM tb_id_code WHERE jenis = 'kedai'")->row()->code_string;
        // $code = $this->db->query("SELECT MAX( RIGHT ( kode_kedai, 5 ) ) AS `no` FROM tb_kedai WHERE kode_kedai LIKE '$depan%'")->row();
        $code = $this->db->query("SELECT MAX( RIGHT ( id_kedai, 5 ) ) AS `no` FROM tb_user WHERE id_kedai LIKE '$depan%'")->row();
        $code2 = '00000' . ($code->no + 1);
        $new_code = $depan . substr($code2, -5);

        $data = array(
            'kode_kedai' => $new_code,
            'nama' => $nama,
            'no_kad_pengenal' => $pengenal,
            'jantina' => $jantina,
            'jenis_perniagaan' => $jenis_perniagaan,
            'nama_perniagaan' => $nama_perniagaan,
            'no_pendaftaran_syarikat' => $no_syarikat,
            'hp' => $no_hp,
            'alamat' => $alamat,
            'email' => $email,
            'negeri' => $negeri,
            'daerah' => $daerah,
            'bank' => $bank,
            'no_account_bank' => $no_account_bank,
            'qrcode' => $qrcode,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_kedai', $data);

        $hash_agent = $this->get_max_hash_agent();
        $hash_agent2 = '00000' . ($hash_agent + 1);
        $new_hash_agent = 'agent' . substr($hash_agent2, -5);

        $data2 = array(
            'id_kedai' => $new_code,
            'username' => strtolower($nm_kedai) . substr($code2, -5),
            'password' => 'bbf2dead374654cbb32a917afd236656',
            'name' => $nama,
            'hash' => $new_hash_agent,
            'level' => 'agent',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_user', $data2);

        $this->db->insert('tb_verify', array(
            'owner' =>  $new_code,
            'verify' => $verify,
            'used' => 'N',
            'date_input' => date('Y-m-d H:i:s')
        ));

        $this->db->insert('tb_history', array(
            'note_1' => 'Add new kedai',
            'note_2' => 'kedai',
            'status' => 'add-data',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
    }

    public function save_edit_kedai()
    {
        $id = $this->input->post('id');
        $kode_kedai = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $pengenal = $this->input->post('pengenal');
        $jantina = $this->input->post('jantina');
        $no_hp = $this->input->post('no_hp');
        $jenis_perniagaan = $this->input->post('jenis_perniagaan');
        $nama_perniagaan = $this->input->post('nama_perniagaan');
        $no_syarikat = $this->input->post('no_syarikat');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $negeri = $this->input->post('negeri');
        $daerah = $this->input->post('daerah');
        $bank = $this->input->post('bank');
        $no_account_bank = $this->input->post('no_account_bank');

        $data = array(
            'nama' => $nama,
            'no_kad_pengenal' => $pengenal,
            'jantina' => $jantina,
            'jenis_perniagaan' => $jenis_perniagaan,
            'nama_perniagaan' => $nama_perniagaan,
            'no_pendaftaran_syarikat' => $no_syarikat,
            'hp' => $no_hp,
            'alamat' => $alamat,
            'email' => $email,
            'negeri' => $negeri,
            'daerah' => $daerah,
            'bank' => $bank,
            'no_account_bank' => $no_account_bank,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $this->db->update('tb_kedai', $data);

        $this->db->insert('tb_history', array(
            'note_1' => 'Edit data kedai',
            'note_2' => $kode_kedai,
            'id' => $id,
            'status' => 'edit-kedai',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
    }

    public function save_edit_sales()
    {
        $id = $this->input->post('id');
        $kode_individu = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $pengenal = $this->input->post('pengenal');
        $jantina = $this->input->post('jantina');
        $no_hp = $this->input->post('no_hp');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $negeri = $this->input->post('negeri');
        $daerah = $this->input->post('daerah');
        $bank = $this->input->post('bank');
        $no_account_bank = $this->input->post('no_account_bank');

        $data = array(
            'nama' => $nama,
            'no_kad_pengenal' => $pengenal,
            'jantina' => $jantina,
            'hp' => $no_hp,
            'alamat' => $alamat,
            'email' => $email,
            'bank' => $bank,
            'negeri' => $negeri,
            'daerah' => $daerah,
            'no_account_bank' => $no_account_bank,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $this->db->update('tb_sales', $data);

        $this->db->insert('tb_history', array(
            'note_1' => 'Edit data individu',
            'note_2' => $kode_individu,
            'id' => $id,
            'status' => 'edit-individu',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));
    }
}
