<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelDashboard extends CI_Model
{

    public function get_data_kedai($id_kedai)
    {
        $q = $this->db->select('a.*, b.nama_bank')
            ->from('tb_kedai a')
            ->join('tb_bank b', 'a.bank = b.id', 'left')
            ->where('a.kode_kedai', $id_kedai)
            ->get();
        return $q;
    }

    public function get_data_individu($id_individu)
    {
        $q = $this->db->select('a.*, b.nama_bank')
            ->from('tb_sales a')
            ->join('tb_bank b', 'a.bank = b.id', 'left')
            ->where('a.kode_individu', $id_individu)
            ->get();
        return $q;
    }

    public function get_data_user($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('tb_user');
        return $q;
    }

    public function get_order_by_status($status)
    {
        $q = $this->db->select('*')
            ->from('tb_eform')
            ->where('status', $status);
        return $q->count_all_results();
    }

    public function get_all_order_by_status()
    {
        $q = $this->db->select('*')
            ->from('tb_eform');
        return $q->count_all_results();
    }

    public function get_order_by_status_no_staff($status)
    {
        $hash = $this->session->hash;
        $id = $this->db->query("SELECT * FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;
        $q = $this->db->select('*')
            ->from('tb_eform')
            ->where('status', $status)
            ->where('name_sales', $id);
        return $q->count_all_results();
    }

    public function get_all_order_by_status_no_staff()
    {
        $hash = $this->session->hash;
        $id = $this->db->query("SELECT * FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;
        $q = $this->db->select('*')
            ->from('tb_eform')
            ->where('name_sales', $id);
        return $q->count_all_results();
    }

    public function get_complete_staff($status)
    {
        $q = $this->db->select('sum(a.commision) AS sum_commission')
            ->from('tb_eform a')
            ->where('a.status', $status)
            ->get();
        return $q;
    }

    public function get_complete_no_staff($status)
    {
        $q = $this->db->select('sum(a.commision) AS sum_commission')
            ->from('tb_eform a')
            ->where('a.name_sales', $this->session->id_kedai)
            ->where('a.status', $status)
            ->get();
        return $q;
    }

    public function count_kedai()
    {
        $q = $this->db->query("SELECT 
                                    a.nama_perniagaan as nama_kedai,
									COUNT( name_sales ) AS hitung
								FROM
								    `tb_kedai` a
								LEFT JOIN tb_eform b ON a.kode_kedai = b.name_sales 
								GROUP BY
									a.kode_kedai
								ORDER BY
									COUNT( name_sales ) DESC
									");
        return $q->result();
    }

    public function count_individu()
    {
        $q = $this->db->query("SELECT
                                    a.nama,
                                    COUNT( name_sales ) AS hitung 
                                FROM
                                    `tb_sales` a
                                    LEFT JOIN tb_eform b ON a.kode_individu = b.name_sales 
                                GROUP BY
                                    a.kode_individu 
                                ORDER BY
                                    COUNT( name_sales ) DESC
                                    ");
        return $q->result();
    }

    public function get_all_announcement_active()
    {
        $q = $this->db->select("a.*, DATE_FORMAT(a.start_date, '%d/%m/%Y') AS new_start, DATE_FORMAT(a.end_date, '%d/%m/%Y') AS new_end, DATEDIFF( CURRENT_DATE, a.end_date ) AS hitung, CAST(DATEDIFF(CURRENT_DATE, DATE(a.date_input)) AS UNSIGNED) as new")
            ->from('tb_announcement a')
            ->where('a.`status`', 'Y')
            ->where('a.`publish`', 'publish')
            // ->having('hitung <= ', 0)
            ->order_by('a.id', 'desc')
            ->get();
        return $q;
    }

    public function cek_verify($owner)
    {
        $q = $this->db->query("SELECT
                                * 
                            FROM
                                tb_verify 
                            WHERE
                                id IN ( SELECT MAX( id ) FROM `tb_verify` WHERE `owner` = '$owner' AND used = 'N' )");
        return $q;
    }
}
