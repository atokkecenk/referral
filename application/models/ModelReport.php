<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelReport extends CI_Model
{
    public function search_all()
    {
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $q = $this->db->select("b.id_kedai AS kode,
                                b.`level`,
                                IFNULL(b2.no_pendaftaran_syarikat,c1.no_kad_pengenal) as nomor,
                                IFNULL(b2.hp,c1.hp) as no_hp,
                                IFNULL(b2.alamat,c1.alamat) as alamat,
                                IFNULL( b2.nama_perniagaan, c1.nama ) AS name_sales_person,
                                c.package AS name_package,
                                d.`value` AS name_type,
                            IF
                                ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                                DATE_FORMAT( a.installation_date, '%d %b, %Y' ) AS new_install,
                                DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                                IFNULL( b2.foto, c1.foto ) AS foto,
                                a.*,
                                DATE_FORMAT( e.date_input, '%d %b %Y' ) AS date_paid,
                                MONTH ( a.input_date ) AS filt_month,
                                YEAR ( a.input_date ) AS filt_year")
            ->from('tb_eform a')
            ->join('tb_user b', 'a.name_sales = b.id_kedai', 'left')
            ->join('tb_kedai b2', 'a.name_sales = b2.kode_kedai', 'left')
            ->join('tb_sales c1', 'a.name_sales = c1.kode_individu', 'left')
            ->join('tb_package c', 'a.package = c.id_pkg', 'left')
            ->join('tb_type d', ' a.type = d.id', 'left')
            ->join("( SELECT * FROM tb_history WHERE id_history IN ( SELECT MAX( id_history ) FROM tb_history WHERE `status` = 'paid' GROUP BY id ) ) e", "a.id = e.id", "left")
            ->having('filt_month', $month)
            ->having('filt_year', $year)
            ->order_by('a.id', 'DESC')
            ->get();
        return $q;
    }

    public function search_by($mode)
    {
        $filter = $this->input->post('filter');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $q = $this->db->select("b.id_kedai AS kode,
                                b.`level`,
                                IFNULL(b2.no_pendaftaran_syarikat,c1.no_kad_pengenal) as nomor,
                                IFNULL(b2.hp,c1.hp) as no_hp,
                                IFNULL(b2.alamat,c1.alamat) as alamat,
                                IFNULL( b2.nama_perniagaan, c1.nama ) AS name_sales_person,
                                c.package AS name_package,
                                d.`value` AS name_type,
                            IF
                                ( a.segment = 'S', 'SME', 'CONSUMER' ) AS name_segment,
                                DATE_FORMAT( a.installation_date, '%d %b, %Y' ) AS new_install,
                                DATE_FORMAT( a.input_date, '%d %b, %Y %h:%i %p' ) AS new_input,
                                IFNULL( b2.foto, c1.foto ) AS foto,
                                a.*,
                                DATE_FORMAT( e.date_input, '%d %b %Y' ) AS date_paid,
                                MONTH ( a.input_date ) AS filt_month,
                                YEAR ( a.input_date ) AS filt_year")
            ->from('tb_eform a')
            ->join('tb_user b', 'a.name_sales = b.id_kedai', 'left')
            ->join('tb_kedai b2', 'a.name_sales = b2.kode_kedai', 'left')
            ->join('tb_sales c1', 'a.name_sales = c1.kode_individu', 'left')
            ->join('tb_package c', 'a.package = c.id_pkg', 'left')
            ->join('tb_type d', ' a.type = d.id', 'left')
            ->join("( SELECT * FROM tb_history WHERE id_history IN ( SELECT MAX( id_history ) FROM tb_history WHERE `status` = 'paid' GROUP BY id ) ) e", "a.id = e.id", "left")
            ->where('b.`level`', $mode)
            ->where('a.name_sales', $filter)
            ->group_by('a.id')
            ->having('filt_month', $month)
            ->having('filt_year', $year)
            ->get();
        return $q;
    }

    public function get_all_user()
    {
        $q = $this->db->query("SELECT
                                a.`level`,
                                a.id_kedai AS `code`,
                                a.username,
                                a.`name`,
                                IFNULL( b.nama_perniagaan, c.nama ) AS name_sales_person 
                            FROM
                                tb_user a
                                LEFT JOIN (SELECT * FROM tb_kedai WHERE active = 'Y') b ON a.id_kedai = b.kode_kedai
                                LEFT JOIN (SELECT * FROM tb_sales WHERE active = 'Y') c ON a.id_kedai = c.kode_individu 
                            WHERE
                                a.`level` != 'staff'
                            ORDER BY
                                a.`level` ASC,
                                IFNULL( b.nama_perniagaan, c.nama ) DESC
                                ");
        return $q;
    }

    public function monthly_report($status = null, $user = null, $month = null, $year = null)
    {
        $whr = array('a.name_sales' => $user, 'a.`status`' => $status, 'MONTH ( a.input_date ) = ' => $month, 'YEAR ( a.input_date ) = ' => $year);
        $q = $this->db->select("a.`status`,
                                    COUNT( a.name_sales ) AS total_order,
                                    SUM( a.commision ) AS comms,
                                    a.name_sales,
                                    IFNULL( b1.nama_perniagaan, b2.nama ) AS name_sales_person,
                                    MONTH ( a.input_date ) AS bulan,
                                    DATE_FORMAT(a.input_date,'%M') AS new_bulan,
                                    YEAR ( a.input_date ) AS tahun")
            ->from("tb_eform a")
            ->join("tb_kedai b1", "a.name_sales = b1.kode_kedai", "left")
            ->join("tb_sales b2", "a.name_sales = b2.kode_individu", "left")
            ->where($whr)
            ->group_by(array("a.name_sales", "a.`status`", "MONTH ( a.input_date )", "YEAR ( a.input_date )"))
            ->get();

        return $q;
    }

    public function total_monthly_report($code = null, $month = null, $year = null)
    {
        // $whr = array('a.name_sales' => $code, 'a.`status`' => 'paid', 'MONTH ( a.input_date ) = ' => $month, 'YEAR ( a.input_date ) = ' => $year);
        $whr = array('a.name_sales' => $code, 'MONTH ( a.input_date ) = ' => $month, 'YEAR ( a.input_date ) = ' => $year);
        $q = $this->db->select("a.`status`,
                                COUNT( a.name_sales ) AS total_order,
                                SUM( a.commision ) AS comms,
                                a.name_sales,
                                IFNULL( b1.nama_perniagaan, b2.nama ) AS name_sales_person,
                                MONTH ( a.input_date ) AS bulan,
                                DATE_FORMAT(a.input_date,'%M') AS new_bulan,
                                YEAR ( a.input_date ) AS tahun")
            ->from("tb_eform a")
            ->join("tb_kedai b1", "a.name_sales = b1.kode_kedai", "left")
            ->join("tb_sales b2", "a.name_sales = b2.kode_individu", "left")
            ->where($whr)
            ->group_by(array("a.name_sales", "MONTH ( a.input_date )", "YEAR ( a.input_date )"))
            ->get();
        return $q;
    }
}
