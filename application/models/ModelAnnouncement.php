<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelAnnouncement extends CI_Model
{
    // start datatables admin
    var $column_order = array('a.id', 'a.subject', 'a.text', 'a.status', 'a.masa', 'a.start_date');
    var $column_search = array('a.subject', 'a.text', 'a.start_date', 'a.end_date');
    var $order = array('a.id' => 'asc');

    private function _get_datatables_query()
    {
        $this->db->select("a.*, DATE_FORMAT(a.start_date, '%d/%m/%Y') AS new_start, DATE_FORMAT(a.end_date, '%d/%m/%Y') AS new_end");
        $this->db->from('tb_announcement as a');
        $this->db->where('a.status', 'Y');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_announcement');
        return $this->db->count_all_results();
    }
    // end datatables

    public function save($image)
    {
        $tipe = $this->input->post('tipe');
        $subject = $this->input->post('subject');
        $publish = $this->input->post('publish');
        $masa = $this->input->post('masa');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $text = $this->input->post('text');

        $ndate1 = explode('/', $start_date);
        $new_st_date = $ndate1[2] . '-' . $ndate1[1] . '-' . $ndate1[0];
        $ndate2 = explode('/', $end_date);
        $new_en_date = $ndate2[2] . '-' . $ndate2[1] . '-' . $ndate2[0];

        switch ($masa) {
            case 'U':
                $start = null;
                $end = null;
                break;

            case 'D':
                $start = $new_st_date;
                $end = $new_en_date;
                break;
        }

        $data = array(
            'tipe' => $tipe,
            'subject' => $subject,
            'text' => $text,
            'foto' => $image,
            'publish' => $publish,
            'masa' => $masa,
            'start_date' => $start,
            'end_date' => $end,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        );

        $this->db->insert('tb_announcement', $data);
    }
}
