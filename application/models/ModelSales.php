<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelSales extends CI_Model
{
    // start datatables sales
    var $column_order = array('a.id', 'a.nama', 'a.no_kad_pengenal', 'a.jantina', 'a.hp', 'a.alamat', 'a.email', 'a.foto', 'b.nama_bank', 'a.no_account_bank');
    var $column_search = array('a.nama', 'a.no_kad_pengenal', 'a.hp', 'a.email', 'b.nama_bank', 'a.no_account_bank');
    var $order = array('a.id' => 'asc');

    private function _get_query_sales()
    {
        $this->db->select("a.*, b.nama_bank");
        $this->db->from('tb_sales as a');
        $this->db->join('tb_bank as b', 'a.bank = b.id', 'left');
        $this->db->where('a.active', 'Y');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables_sales()
    {
        $this->_get_query_sales();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_query_sales();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('tb_sales');
        $this->db->where('active', 'Y');
        return $this->db->count_all_results();
    }
    // end datatables sales

    public function get_data_individu_by_sha($params)
    {
        $q = $this->db->select("b.id_negeri,
                                b.nama AS nama_negeri,
                                c.id_daerah,
                                c.nama AS nama_daerah,
                                a.*")
        ->from("tb_sales a")
        ->join('tb_negeri b', 'a.negeri = b.id_negeri', 'left')
        ->join('tb_daerah c', 'a.daerah = c.id_daerah', 'left')
        ->where('sha1(a.id)', $params)
        ->get()->row();
        return $q;
    }
}
