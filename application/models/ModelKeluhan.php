<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ModelKeluhan extends CI_Model
{
    public function get_keluhan_all()
    {
        return $this->db->select("b.`level`, LOWER(b.`name`) as nama, 	DATE_FORMAT( a.date_input, '%d %b, %Y %h:%i %p' ) AS new_input, a.* ")
            ->from('tb_keluhan a')
            ->join('tb_user b', 'a.user_input = b.`hash`', 'left')
            ->order_by('id', 'desc')->get()->result();
    }

    public function get_keluhan_by_user()
    {
        return $this->db->select("b.`level`, LOWER(b.`name`) as nama, 	DATE_FORMAT( a.date_input, '%d %b, %Y %h:%i %p' ) AS new_input, a.* ")
            ->from('tb_keluhan a')
            ->join('tb_user b', 'a.user_input = b.`hash`', 'left')
            ->where('a.user_input', $this->session->hash)
            ->order_by('id', 'desc')->get()->result();
    }
}
