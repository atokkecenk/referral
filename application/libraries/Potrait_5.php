<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
class Potrait_5 extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }

    public function Header()
    {
        // $mysqli = new mysqli("localhost", "root", "", "ereferral");
        $mysqli = new mysqli("localhost", "sistembl_referral", "f@rid012", "sistembl_referral");
        if ($mysqli->connect_errno) {
            echo "Failed to connect to MySQL: " . $mysqli->connect_error;
            exit();
        }
        $result = $mysqli->query("SELECT * FROM tb_flyer WHERE id = 5");
        $row = $result->fetch_array(MYSQLI_ASSOC);
       
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $this->Image('assets/images/setting/' . $row['file'], 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}
