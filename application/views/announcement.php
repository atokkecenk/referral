<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12 col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>ANNOUNCEMENT</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        if ($announcement->num_rows() < 1) {
                                            if ($this->session->level == 'staff') {
                                                echo '<font style="color: #000;"><i class="feather icon-info text-c-red m-r-10"></i>No Announcement Here.. <br>please <a href="' . base_url('add-announcement') . '" title="Add Announcement">Add Announcement</a></font>';
                                            } else {
                                                echo '<div class="alert alert-danger"><i class="feather icon-info text-c-red m-r-10"></i>No Announcement Here !!</div>';
                                            }
                                        } else {
                                            foreach ($announcement->result() as $annc) {
                                                if ($annc->new < 8) {
                                                    $new = '<span class="badge badge-success m-l-5">NEW</span>';
                                                } else {
                                                    $new = null;
                                                }

                                                if ($annc->masa == 'D') {
                                                    $date = '<i class="feather icon-calendar m-r-5"></i><b>' . $annc->new_start . '</b> until <b>' . $annc->new_end . '</b>';
                                                } else {
                                                    $date = '<i class="feather icon-calendar m-r-5"></i>Unlimited';
                                                }

                                                if ($annc->foto == '') {
                                                    $image = null;
                                                } else {
                                                    if ($annc->tipe == 'video') {
                                                        $image = "<video class='video' controls>
                                                                <source src='" . base_url('assets/images/announcement/' . $annc->foto) . "' type='video/mp4'>
                                                                Your browser does not support the video tag.
                                                                </video>
                                                                ";
                                                    } else {
                                                        $image = '<p><img src="' . base_url('assets/images/announcement/' . $annc->foto) . '" style="border: 1px solid #994442; width: 100%;"></p>';
                                                    }
                                                }
                                        ?>
                                                <div class="alert alert-danger">
                                                    <h4 style="font-weight: bold; color: #721c24;"><i class="feather icon-bell m-r-10"></i><?= $annc->subject . ' ' . $new ?></h4>
                                                    <?= $image ?>
                                                    <?= '<p>' . $annc->text . '</p>' ?>
                                                    <?= $date ?>
                                                </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->

<!-- Modal Edit Password -->
<div class="modal fade" id="modalEditPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save_edit_password', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" class="form-control" name="hash" value="<?= $this->session->userdata('hash') ?>">
                <div class="form-group">
                    <label>Default Password</label>
                    <input type="text" class="form-control" value="ABC123" readonly>
                </div>
                <div class="form-group">
                    <label>New Password</label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Set New Password" id="password" required oninvalid="this.setCustomValidity('Input new password')" oninput="setCustomValidity('')">
                        <div class="input-group-append">
                            <span class="input-group-text" id="passIcon"></span>
                        </div>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="showPassword" onclick="showPass();">
                    <label class="custom-control-label" for="showPassword">Show Password</label>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Update & Restart</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>