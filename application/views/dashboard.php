<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <?php
                            if ($level == 'staff' || $level == 'user') {
                            ?>
                                <div class="col-md-6 col-xl-6">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/pending') ?>'" style="cursor: pointer;" title="View Order Pending"><i class="feather icon-alert-circle f-20 m-r-20" style="color: #04a9f5;"></i>Pending<?= '<b class="m-l-5">' . $pending . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/port-full') ?>'" style="cursor: pointer;" title="View Order Port Full"><i class="feather icon-alert-triangle f-20 m-r-20" style="color: #04a9f5;"></i>Port Full<?= '<b class="m-l-5">' . $portfull . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/processing') ?>'" style="cursor: pointer;" title="View Order Processing"><i class="feather icon-refresh-cw f-20 m-r-20" style="color: #04a9f5;"></i>Processing<?= '<b class="m-l-5">' . $processing . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/paid') ?>'" style="cursor: pointer;" title="View Order Paid"><i class="feather icon-check-circle f-20 m-r-20" style="color: #04a9f5;"></i>Paid<?= '<b class="m-l-5">' . $paid . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/cancel') ?>'" style="cursor: pointer;" title="View Order Cancel"><i class="feather icon-x-circle f-20 m-r-20" style="color: #04a9f5;"></i>Cancel<?= '<b class="m-l-5">' . $cancel . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/upcoming-projects') ?>'" style="cursor: pointer;" title="View Order Upcoming Projects"><i class="feather icon-external-link f-20 m-r-20" style="color: #04a9f5;"></i>Upcoming Projects<?= '<b class="m-l-5">' . $upprojects . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/rejected') ?>'" style="cursor: pointer;" title="View Order Rejected"><i class="feather icon-corner-up-left f-20 m-r-20" style="color: #04a9f5;"></i>Rejected<?= '<b class="m-l-5">' . $rejected . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order/completed') ?>'" style="cursor: pointer;" title="View Order Completed"><i class="feather icon-award f-20 m-r-20" style="color: #04a9f5;"></i>Completed<?= '<b class="m-l-5">' . $completed . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $all_order . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-6">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <!-- <h6 class="mb-4">AMOUNT WILL BE PAID</h6> -->
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <!-- <h1><b>RM120.45</b></h1> -->
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <div class="form-group">
                                                                <label>AMOUNT WILL BE PAID</label>
                                                                <h1><b>RM<?php
                                                                            $complete = ($comm_complete == '') ? '0.00' : $comm_complete;
                                                                            echo $complete;
                                                                            ?></b></h1>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <div class="form-group">
                                                                <label>COMPLETE</label>
                                                                <h4><b>RM<?php
                                                                            echo $complete;
                                                                            ?></b></b></h4>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <div class="form-group">
                                                                <label>PAID</label>
                                                                <h4><b>RM<?php
                                                                            $paid = ($comm_paid == '') ? '0.00' : $comm_paid;
                                                                            echo $paid;
                                                                            ?></b></b></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>NAMA KEDAI/WARUNG/KIOS</h5>
                                                </div>
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table class="table" id="tbl-dashboard-kedai">
                                                            <thead>
                                                                <tr>
                                                                    <th>No.</th>
                                                                    <th>Nama Kedai/Warung/Kios</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $no = 1;
                                                                foreach ($count_kedai as $kd) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?= $no . '.' ?></td>
                                                                        <td><?= ucwords(strtolower($kd->nama_kedai)) ?></td>
                                                                        <td><?= $kd->hitung ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $no++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>NAMA INDIVIDU</h5>
                                                </div>
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table class="table" id="tbl-dashboard-individu">
                                                            <thead>
                                                                <tr>
                                                                    <th>No.</th>
                                                                    <th>Nama Individu</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $no = 1;
                                                                foreach ($count_individu as $id) {
                                                                ?>
                                                                    <tr>
                                                                        <td><?= $no . '.' ?></td>
                                                                        <td><?= ucwords(strtolower($id->nama)) ?></td>
                                                                        <td><?= $id->hitung ?></td>
                                                                    </tr>
                                                                <?php
                                                                    $no++;
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } elseif ($level == 'agent') {
                            ?>
                                <div class=" col-lg-12">
                                    <div class="row">
                                        <?php
                                        if ($data_user->password == 'bbf2dead374654cbb32a917afd236656') {
                                        ?>
                                            <div class="col-md-6">
                                                <div class="alert alert-danger">
                                                    <p>
                                                    <h5 style="color: #721c24;">Step 1 &rarr; Update Password</h5>
                                                    [ + ] Update your <b>password</b>
                                                    <br>
                                                    [ ? ] Default your password <b>ABC123</b>
                                                    <br>
                                                    <button type="button" class="btn btn-dark mt-2" data-toggle="modal" data-target="#modalEditPass">Click Here</button>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        if ($data->foto == '') {
                                        ?>
                                            <div class="col-md-6">
                                                <div class="alert alert-danger">
                                                    <p>
                                                    <h5 style="color: #721c24;">Step 2 &rarr; Update Your Profile</h5>
                                                    [ + ] Upload your <b>Images Perniagaan/Gambar Selfie</b>
                                                    <br>
                                                    <button type="button" data-toggle="modal" data-target="#modalUploadImage" class="btn btn-dark mt-2">Click Here</button>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-6 mt-2">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/pending') ?>'" style="cursor: pointer;" title="View Order Pending"><i class="feather icon-alert-circle f-20 m-r-20" style="color: #04a9f5;"></i>Pending<?= '<b class="m-l-5">' . $ns_pending . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/port-full') ?>'" style="cursor: pointer;" title="View Order Port Full"><i class="feather icon-alert-triangle f-20 m-r-20" style="color: #04a9f5;"></i>Port Full<?= '<b class="m-l-5">' . $ns_portfull . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/processing') ?>'" style="cursor: pointer;" title="View Order Processing"><i class="feather icon-refresh-cw f-20 m-r-20" style="color: #04a9f5;"></i>Processing<?= '<b class="m-l-5">' . $ns_processing . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/paid') ?>'" style="cursor: pointer;" title="View Order Paid"><i class="feather icon-check-circle f-20 m-r-20" style="color: #04a9f5;"></i>Paid<?= '<b class="m-l-5">' . $ns_paid . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/cancel') ?>'" style="cursor: pointer;" title="View Order Cancel"><i class="feather icon-x-circle f-20 m-r-20" style="color: #04a9f5;"></i>Cancel<?= '<b class="m-l-5">' . $ns_cancel . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/upcoming-projects') ?>'" style="cursor: pointer;" title="View Order Upcoming Projects"><i class="feather icon-external-link f-20 m-r-20" style="color: #04a9f5;"></i>Upcoming Projects<?= '<b class="m-l-5">' . $ns_upprojects . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/rejected') ?>'" style="cursor: pointer;" title="View Order Rejected"><i class="feather icon-corner-up-left f-20 m-r-20" style="color: #04a9f5;"></i>Rejected<?= '<b class="m-l-5">' . $ns_rejected . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-agent/completed') ?>'" style="cursor: pointer;" title="View Order Completed"><i class="feather icon-award f-20 m-r-20" style="color: #04a9f5;"></i>Completed<?= '<b class="m-l-5">' . $ns_completed . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $ns_all_order . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-6 mt-2">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <!-- <h6 class="mb-4">AMOUNT WILL BE PAID</h6> -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>COMPLETE</label>
                                                        <h4><b>RM<?php
                                                                    $compl = ($ns_comm_complete == '') ? '0.00' : $ns_comm_complete;
                                                                    echo $compl;
                                                                    ?></b></b></h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>PAID</label>
                                                        <h4><b>RM<?php
                                                                    $paid = ($ns_comm_paid == '') ? '0.00' : $ns_comm_paid;
                                                                    echo $paid;
                                                                    ?></b></b></h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="text-center">
                                                        Share your link & earn more!
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="source" id="source" value="<?= base_url('scan/' . $data_user->id_kedai) ?>" placeholder="Source URL">
                                                            <div class="input-group-append">
                                                                <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Copy Link" id="btnCopyURL">Copy</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://facebook.com" target="_blank" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://web.whatsapp.com" target="_blank" class="btn btn-whatsapp btn-block"><i class="fab fa-whatsapp" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://web.telegram.org" target="_blank" class="btn btn-telegram btn-block"><i class="fab fa-telegram-plane" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://instagram.com" target="_blank" class="btn btn-instagram btn-block"><i class="fab fa-instagram" style="margin: 0 !important;"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-center mt-2">
                                                (Your code: <?= $data->kode_kedai ?>)
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <!-- <div class="card-header">
                                            <h5>Kedai Registration Form &rarr; Kedai/Warung/Kios</h5>
                                        </div> -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="col-sm-12" style="text-align: center;">
                                                        <img class="qr-code" src="<?= base_url('assets/images/qr-code/' . $data->qrcode) ?>" width="200px">
                                                    </div>
                                                    <div class="col-sm-12 mt-3" style="text-align: center;">
                                                        <a href="<?= base_url('download/' . $data->qrcode) ?>" class="btn btn-primary" target="_blank"><i class="fas fa-download"></i>Download this file</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="4" class="text-center"><i class="feather icon-award" style="color: #04a9f5;"></i> INFORMASI KEDAI/KIOS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td width="25%"><b>Kode Kedai</b></td>
                                                                    <td width="25%"><?= $data->kode_kedai ?></td>
                                                                    <td width="25%"><b>Username</b></td>
                                                                    <td width="25%"><?= $data_user->username ?>&nbsp;<button type="button" class="btn btn-info btn-margin" data-toggle="modal" data-target="#modalEditUsername"><i class="feather icon-edit r-no-margin"></i> Edit</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Nama Kedai/Kios</b></td>
                                                                    <td><?= strtoupper($data->nama_perniagaan) ?></td>
                                                                    <td><b>Bank</b></td>
                                                                    <td><?= $data->nama_bank ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>No Pendaftaaran Syarikat</b></td>
                                                                    <td><?= $data->no_pendaftaran_syarikat ?></td>
                                                                    <td><b>No Akaun Bank</b></td>
                                                                    <td><?= $data->no_account_bank ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>No KAD Pengenal</b></td>
                                                                    <td><?= $data->no_kad_pengenal ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Nama</b></td>
                                                                    <td><?= $data->nama ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Jantina</b></td>
                                                                    <td><?= ucwords($data->jantina) ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>No Handphone</b></td>
                                                                    <td><?= $data->hp ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Alamat</b></td>
                                                                    <td><?= $data->alamat ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Email</b></td>
                                                                    <td><?= $data->email ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            } elseif ($level == 'individu') {
                            ?>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <?php
                                        if ($data_user->password == 'bbf2dead374654cbb32a917afd236656') {
                                        ?>
                                            <div class="col-md-6">
                                                <div class="alert alert-danger">
                                                    <p>
                                                    <h5 style="color: #721c24;">Step 1 &rarr; Update Password</h5>
                                                    [ + ] Update your <b>password</b>
                                                    <br>
                                                    [ ? ] Default your password <b>ABC123</b>
                                                    <br>
                                                    <button type="button" class="btn btn-dark mt-2" data-toggle="modal" data-target="#modalEditPass">Click Here</button>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        if ($data2->foto == '') {
                                        ?>
                                            <div class="col-md-6">
                                                <div class="alert alert-danger">
                                                    <p>
                                                    <h5 style="color: #721c24;">Step 2 &rarr; Update Your Profile</h5>
                                                    [ + ] Upload your <b>Images Perniagaan/Gambar Selfie</b>
                                                    <br>
                                                    <button type="button" data-toggle="modal" data-target="#modalUploadImageIndividu" class="btn btn-dark mt-2">Click Here</button>
                                                    </p>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-6 mt-2">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <h6 class="mb-4">TOTAL ORDER</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/pending') ?>'" style="cursor: pointer;" title="View Order Pending"><i class="feather icon-alert-circle f-20 m-r-20" style="color: #04a9f5;"></i>Pending<?= '<b class="m-l-5">' . $ns_pending . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/port-full') ?>'" style="cursor: pointer;" title="View Order Port Full"><i class="feather icon-alert-triangle f-20 m-r-20" style="color: #04a9f5;"></i>Port Full<?= '<b class="m-l-5">' . $ns_portfull . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/processing') ?>'" style="cursor: pointer;" title="View Order Processing"><i class="feather icon-refresh-cw f-20 m-r-20" style="color: #04a9f5;"></i>Processing<?= '<b class="m-l-5">' . $ns_processing . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/paid') ?>'" style="cursor: pointer;" title="View Order Paid"><i class="feather icon-check-circle f-20 m-r-20" style="color: #04a9f5;"></i>Paid<?= '<b class="m-l-5">' . $ns_paid . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/cancel') ?>'" style="cursor: pointer;" title="View Order Cancel"><i class="feather icon-x-circle f-20 m-r-20" style="color: #04a9f5;"></i>Cancel<?= '<b class="m-l-5">' . $ns_cancel . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/upcoming-projects') ?>'" style="cursor: pointer;" title="View Order Upcoming Projects"><i class="feather icon-external-link f-20 m-r-20" style="color: #04a9f5;"></i>Upcoming Projects<?= '<b class="m-l-5">' . $ns_upprojects . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/completed') ?>'" style="cursor: pointer;" title="View Order Rejected"><i class="feather icon-corner-up-left f-20 m-r-20" style="color: #04a9f5;"></i>Rejected<?= '<b class="m-l-5">' . $ns_rejected . '</b>' ?></h4>
                                                        </div>
                                                    </div>

                                                    <div class="row d-flex align-items-center mb-3">
                                                        <div class="col-9">
                                                            <h4 class="f-w-300 d-flex align-items-center m-b-0 hover" onclick="javascript:location.href = '<?= base_url('view-order-individu/completed') ?>'" style="cursor: pointer;" title="View Order Completed"><i class="feather icon-award f-20 m-r-20" style="color: #04a9f5;"></i>Completed<?= '<b class="m-l-5">' . $ns_completed . '</b>' ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <i class="feather icon-info m-r-10"></i><?= 'Total <b>' . $ns_all_order . '</b>' ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xl-6 mt-2">
                                    <div class="card daily-sales">
                                        <div class="card-block">
                                            <!-- <h6 class="mb-4">AMOUNT WILL BE PAID</h6> -->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>COMPLETE</label>
                                                        <h4><b>RM<?php
                                                                    $compl = ($ns_comm_complete == '') ? '0.00' : $ns_comm_complete;
                                                                    echo $compl;
                                                                    ?></b></b></h4>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>PAID</label>
                                                        <h4><b>RM<?php
                                                                    $paid = ($ns_comm_paid == '') ? '0.00' : $ns_comm_paid;
                                                                    echo $paid;
                                                                    ?></b></b></h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="progress m-b-10" style="height: 7px;">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="text-center">
                                                        Share your link & earn more!
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" name="source" id="source" value="<?= base_url('scan/' . $data_user->id_kedai) ?>" placeholder="Source URL">
                                                            <div class="input-group-append">
                                                                <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Copy Link" id="btnCopyURL">Copy</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://facebook.com" target="_blank" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://web.whatsapp.com" target="_blank" class="btn btn-whatsapp btn-block"><i class="fab fa-whatsapp" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://web.telegram.org" target="_blank" class="btn btn-telegram btn-block"><i class="fab fa-telegram-plane" style="margin: 0 !important;"></i></a>
                                                </div>
                                                <div class="col-sm-3 mb-2">
                                                    <a href="https://instagram.com" target="_blank" class="btn btn-instagram btn-block"><i class="fab fa-instagram" style="margin: 0 !important;"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-center mt-2">
                                                (Your code: <?= $data2->kode_individu ?>)
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <!-- <div class="card-header">
                                            <h5>Kedai Registration Form &rarr; Kedai/Warung/Kios</h5>
                                        </div> -->
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="col-sm-12" style="text-align: center;">
                                                        <img class="qr-code" src="<?= base_url('assets/images/qr-code/' . $data2->qrcode) ?>" width="200px">
                                                    </div>
                                                    <div class="col-sm-12 mt-3" style="text-align: center;">
                                                        <a href="<?= base_url('download/' . $data2->qrcode) ?>" class="btn btn-primary" target="_blank"><i class="fas fa-download"></i>Download this file</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th colspan="4" class="text-center"><i class="feather icon-award" style="color: #04a9f5;"></i> INFORMASI INDIVIDU</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td width="25%"><b>Kode Referral Individu</b></td>
                                                                    <td width="25%"><?= $data2->kode_individu ?></td>
                                                                    <td width="25%"><b>Username</b></td>
                                                                    <td width="25%"><?= $data_user->username ?> &nbsp;<button type="button" class="btn btn-info btn-margin" data-toggle="modal" data-target="#modalEditUsername"><i class="feather icon-edit r-no-margin"></i> Edit</button></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Nama</b></td>
                                                                    <td><?= $data2->nama ?></td>
                                                                    <td><b>Bank</b></td>
                                                                    <td><?= $data2->nama_bank ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>No KAD Pengenal</b></td>
                                                                    <td><?= $data2->no_kad_pengenal ?></td>
                                                                    <td><b>No Akaun Bank</b></td>
                                                                    <td><?= $data2->no_account_bank ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Jantina</b></td>
                                                                    <td>
                                                                        <?= ucwords($data2->jantina) ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>No Handphone</b></td>
                                                                    <td><?= $data2->hp ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Alamat</b></td>
                                                                    <td><?= $data2->alamat ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><b>Email</b></td>
                                                                    <td><?= $data2->email ?></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <!-- [ Main Content ] end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ Main Content ] end -->

<!-- Modal Edit Password -->
<div class="modal fade" id="modalEditPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save-edit-password', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" name="id" value="<?= $this->session->id ?>">
                <div class="form-group">
                    <label>Default Password</label>
                    <input type="text" class="form-control" value="ABC123" readonly>
                </div>
                <div class="form-group">
                    <label>New Password <span style="color: red;">*</span></label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Set New Password" id="password" required oninvalid="this.setCustomValidity('Input new password')" oninput="setCustomValidity('')">
                        <div class="input-group-append">
                            <span class="input-group-text" id="passIcon"></span>
                        </div>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="showPassword" onclick="showPass();">
                    <label class="custom-control-label" for="showPassword">Show Password</label>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Update & Restart</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Upload Image Kedai -->
<div class="modal fade" id="modalUploadImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('upload-image-kedai', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" name="kd_kedai" value="<?= $this->session->id_kedai ?>">
                <div class="form-group">
                    <label>Upload Image Perniagaan/Gambar Selfie <span style="color: red;">*</span></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input imageKedai" name="image" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                        <label class="custom-file-label">Choose file..</label>
                    </div>
                    <small class="text-c-red">Max file size 5 mb. Extension jpg, jpeg, png</small>
                </div>
                <div class="text-center">
                    <img src="#" class="img-responsive" id="viewImageKedai">
                    <div class="m-t-5" id="messageImageKedai"></div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-upload"></i>Upload</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Upload Image Individu -->
<div class="modal fade" id="modalUploadImageIndividu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open_multipart('upload-image-individu', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" name="kd_kedai" value="<?= $this->session->id_kedai ?>">
                <div class="form-group">
                    <label>Upload Image Perniagaan/Gambar Selfie <span style="color: red;">*</span></label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input imageIndividu" name="image" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                        <label class="custom-file-label">Choose file..</label>
                    </div>
                    <small class="text-c-red">Max file size 5 mb. Extension jpg, jpeg, png</small>
                </div>
                <div class="text-center">
                    <img src="#" class="img-responsive" id="viewImageIndividu">
                    <div class="m-t-5" id="messageImageIndividu"></div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-upload"></i>Upload</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Edit Username -->
<div class="modal fade" id="modalEditUsername" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Username</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('save-edit-username', 'method="post"') ?>
            <div class="modal-body">
                <?php
                $hash = $this->session->hash;
                $username = $this->db->query("SELECT * FROM tb_user WHERE `hash` = '$hash'")->row()->username;
                ?>
                <input type="hidden" name="id" value="<?= $this->session->id ?>">
                <div class="form-group">
                    <label>Username <span style="color: red;">*</span></label>
                    <input type="text" class="form-control" name="username" value="<?= $username ?>" required oninvalid="this.setCustomValidity('Please input username')" oninput="this.setCustomValidity('')">
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Update & Restart</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>