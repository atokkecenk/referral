﻿<?php
$sett = $this->db->get_where('tb_setting', array('id' => '1'))->row();
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>eReferral - Digital Eform</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/<?= $sett->favicon ?>">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/animate/animate.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/css-hamburgers/hamburgers.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/animsition/css/animsition.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/select2/select2.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css">

	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/eform/css/main.css">

</head>

<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<?php
				if ($this->session->flashdata('error')) {
					echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						' . $this->session->flashdata('error') . '
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: -0.5rem; padding: .75rem .2rem;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>';
				} elseif ($this->session->flashdata('success')) {
					echo '<div class="col-sm-12 mt-3">
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						' . $this->session->flashdata('success') . '
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="right: -0.5rem; padding: .75rem .2rem;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>';
				}
				?>
				<div class="title-form">
					UNIFI APPLICATION FORM (DIGITAL-FORM)
				</div>
				<?= form_open('save-eform', 'class="login100-form validate-form" method="post" target="_blank"') ?>
				<?php
				if (!isset($data->id_kedai)) {
					echo '<div class="col-sm-12">
							<div class="alert alert-danger" role="alert">
								Data Not Found, cek your URL !!
							</div>
						</div>';
				} else {
				?>
					<input type="hidden" name="kedai" value="<?= $data->id ?>">
					<input type="hidden" name="name_sales" value="<?= $data->id_kedai ?>">
					<input type="hidden" class="nsp" name="name_sales_string" value="<?= $data->name ?>">
					<?php
					$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$data->id_kedai'")->row();
					if ($cek->level == 'agent') {
						$get = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$data->id_kedai'")->row();
						$nsp = $get->nama_perniagaan;
					} elseif ($cek->level = 'individu') {
						$nsp = $data->name;
					}
					?>
					<div class="wrap-input100 m-b-26">
						<label>E-Reff Agent Name <span style="color: red;">*</span></label>
						<input class="input100" type="text" value="<?= $nsp ?>" readonly>
					</div>
					<div class="form-group" style="margin-bottom: 0 !important;">
						<label>Segment <span style="color: red;">*</span></label>
						<div class="radio mt-1 m-b-26">
							<div class="custom-control custom-radio">
								<label>
									<input type="radio" class="rdSegment" name="segment" value="S">
									Business (SME)</label>
							</div>
							<div class="custom-control custom-radio">
								<label>
									<input type="radio" class="rdSegment" name="segment" value="C">
									Residential (CONSUMER)</label>
							</div>
						</div>
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Customer/Business Name  <span style="color: red;">*</span></label>
						<input class="input100 cn" type="text" name="name_customer" placeholder="Abu Wafa" required oninvalid="this.setCustomValidity('Input name customer')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Number IC <span style="color: red;">*</span></label>
						<input class="input100 numberIc" type="text" name="number_ic" placeholder="123456-00-1122" maxlength="14" required oninvalid="this.setCustomValidity('Input number IC')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Contact Number <span style="color: red;">*</span></label>
						<input class="input100 ctn" type="number" name="contact1" placeholder="0123456789" required oninvalid="this.setCustomValidity('Input contact number 1')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>2nd Contact Number <span style="color: red;">*</span></label>
						<input class="input100 ncn" type="number" name="contact2" placeholder="If no 2nd contact put 0" required oninvalid="this.setCustomValidity('Input contact number 2nd')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Email Address <span style="color: red;">*</span></label>
						<input class="input100 ea" type="email" name="email" placeholder="farid@gmail.com" required oninvalid="this.setCustomValidity('Input email address')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Installation Address <span style="color: red;">*</span></label>
						<input class="input100 ia" type="text" name="install_address" placeholder="Installation Address" required oninvalid="this.setCustomValidity('Input installation address')" oninput="this.setCustomValidity('')">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Billing Address</label>
						<input class="input100 ba" type="text" name="billing_address" placeholder="if same put (-)">
					</div>
					<div class="form-group mb-4">
						<label class="mb-2">Package to be subscribed <span style="color: red;">*</span></label>
						<div id="package"></div>

					</div>
					<div id="delNumber"></div>
					<div class="form-group" style="margin-bottom: 0 !important;">
						<label>Installation Type <span style="color: red;">*</span></label>
						<div class="radio mt-1 m-b-26">
							<?php
							$i = 1;
							foreach ($type as $typ) {
							?>
								<div class="custom-control custom-radio">
									<label>
										<input type="radio" class="type installType" data-type="<?= $typ->id ?>" id="<?= 'radio' . $i ?>" name="type" value="<?= $typ->id ?>">
										<?= $typ->value ?></label>
								</div>
							<?php
								$i++;
							}
							?>
						</div>
						<input type="hidden" class="typeText">
					</div>
					<div class="wrap-input100 m-b-26 delNumber" style="display: none;">
						<label>Del Number <span style="color: red;">*</span></label>
						<input class="input100" type="text" name="del_number" placeholder="Del Number">
					</div>
					<div class="wrap-input100 m-b-26">
						<label>Prefered Installation Date <span style="color: red;">*</span></label>
						<input class="input100 datepicker id" type="text" name="date" placeholder="dd/mm/yyyy">
					</div>
					<div style="margin-bottom: 18px;">
						<p style="color: #000;">
							<b>Term & Condition</b>
							<br>
						<div class="checkbox">
							<label><input type="checkbox" class="tc1" required oninvalid="this.setCustomValidity('Checklist this item')" oninput="this.setCustomValidity('')" value="I hereby consent to subscribe the service with subscription contract of 24 month."> I hereby consent to subscribe the service with subscription contract of 24 month.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="tc2" required oninvalid="this.setCustomValidity('Checklist this item')" oninput="this.setCustomValidity('')" value="I have read, understand and agree to be bound by the Term and Condition of service."> I have read, understand and agree to be bound by the Term & Condition of service.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="tc3" required oninvalid="this.setCustomValidity('Checklist this item')" oninput="this.setCustomValidity('')" value="I agree to pay advance payment within 10 days after Installation complete."> I agree to pay advance payment within 10 days after Installation complete.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="tc4" required oninvalid="this.setCustomValidity('Checklist this item')" oninput="this.setCustomValidity('')" value="I hereby consent TM representative to proceed and process my order. Kindly notify me if there is any issues pertaining to my request."> I hereby consent TM representative to proceed and process my order. Kindly notify me if there is any issues pertaining to my request.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="tc5" required oninvalid="this.setCustomValidity('Checklist this item')" oninput="this.setCustomValidity('')" value="For unifi lite package automatic upgrading to unifi fixed upon infra readiness."> For unifi lite package automatic upgrading to unifi fixed upon infra readiness.</label>
						</div>
						</p>
					</div>
					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn submit" style="width: 100% !important;">
							Submit My Application
						</button>
					</div>
				<?php } ?>
				<?= form_close() ?>
				<div class="footer">
					eReferral &copy; <?= date('Y') ?>
				</div>
			</div>
		</div>
	</div>

	<script src="<?= base_url() ?>assets/eform/vendor/jquery/jquery-3.2.1.min.js"></script>

	<script src="<?= base_url() ?>assets/eform/vendor/animsition/js/animsition.min.js"></script>

	<script src="<?= base_url() ?>assets/eform/vendor/bootstrap/js/popper.js"></script>
	<script src="<?= base_url() ?>assets/eform/vendor/bootstrap/js/bootstrap.min.js"></script>

	<script src="<?= base_url() ?>assets/eform/vendor/select2/select2.min.js"></script>

	<script src="<?= base_url() ?>assets/eform/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?= base_url() ?>assets/eform/vendor/daterangepicker/daterangepicker.js"></script>
	<script src="<?= base_url() ?>assets/eform/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

	<script src="<?= base_url() ?>assets/eform/vendor/countdowntime/countdowntime.js"></script>

	<script src="<?= base_url() ?>assets/eform/js/main.js"></script>

	<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
		$(document).ready(function() {
			$('.datepicker').datepicker({
				format: "dd/mm/yyyy",
				autoclose: true,
				todayHighlight: true,
				todayBtn: "linked"
			});

			$(document).on('keyup', '.numberIc', function() {
				var nilai = $(this).val();
				if (nilai.length == 6 || nilai.length == 9) {
					this.value += '-';
				}
			});

			$('.installType').change(function() {
				var id = $(this).data("type");
				if (id == 2) {
					$('.delNumber').show();
				} else {
					$('.delNumber').hide();
				}
			});

			$('.submit').click(function() {
				var typ = $('.installType:checked').val();
				var delnum = $('[name="del_number"]').val();
				// console.log('->' + delnum);
				if (typ == '2' && delnum == '') {
					alert("Please insert del number !!");
				}
			});

			$('.rdSegment').change(function() {
				var id = $(this).val();
				$.ajax({
					url: "<?= base_url('get-package') ?>",
					method: "POST",
					data: {
						id: id
					},
					async: true,
					dataType: 'json',
					success: function(data) {

						var html = '';
						var i;
						html += '<select class="form-control" name="package" id="listPackage" onchange="test();">';
						html += '<option value = "" > Nothing Selected </option>';
						for (i = 0; i < data.length; i++) {
							html += '<option value=' + data[i].id_pkg + '>' + data[i].package + '</option>';
						}
						html += '</select>';
						$('#package').html(html);

					}
				});
				return false;
			});
			$('#package').html('<select class="form-control" name="package"><option value = "" > Nothing Selected </option></select>');

		});

		function test() {
			var id = $('#listPackage').val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('get-package-by-id/') ?>" + id,
				dataType: 'json',
				success: function(data) {
					$('.packageText').val(data.package);
				}
			});
			return false;
		}

		$('.type').change(function() {
			var id = $(this).val();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('get-type-by-id/') ?>" + id,
				dataType: 'json',
				success: function(data) {
					$('.typeText').val(data.value);
				}
			});
			return false;
		});

		// $('.submit').click(wa);

		function wa() {
			var sg;
			var url_wa = 'https://web.whatsapp.com/send';
			if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
				url_wa = 'whatsapp://send/';
			}
			// Get Value
			var via_url = location.href;
			var nsp = $('.nsp').val();
			var segment = $('.rdSegment').val();
			if (segment == 'S') {
				sg = 'SME';
			} else {
				sg = 'CONSUMER';
			}
			var cn = $('.cn').val();
			var ctn = $('.ctn').val();
			var ncn = $('.ncn').val();
			var ea = $('.ea').val();
			var ia = $('.ia').val();
			var ba = $('.ba').val();
			var pk = $('.packageText').val();
			var tp = $('.typeText').val();
			var id = $('.id').val();
			var tc1 = $('.tc1').val();
			var tc2 = $('.tc2').val();
			var tc3 = $('.tc3').val();
			var tc4 = $('.tc4').val();
			var tc5 = $('.tc5').val();
			var w = 960,
				h = 540,
				left = Number((screen.width / 2) - (w / 2)),
				tops = Number((screen.height / 2) - (h / 2)),
				popupWindow = window.open(url_wa + '?phone=62895608266103&text=Name Sales Person: ' + nsp + ' %0ASegment: ' + sg + ' %0ACustomer Name: ' + cn + '%0AContact Number: ' + ctn + '%0A 2nd Contact Number: ' + ncn + '%0AEmail Address: ' + ea + '%0AInstallation Address: ' + ia + '%0ABilling Address: ' + ba + '%0APackage to be subscribed: ' + pk + '%0AInstallation Type: ' + tp + '%0APreffered Installation Date: ' + id + '%0A%0A Term and Condition :%0A ☑ ' + tc1 + '%0A ☑ ' + tc2 + '%0A ☑ ' + tc3 + '%0A ☑ ' + tc4 + '%0A ☑ ' + tc5 + '%0A%0Avia ' + via_url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=1, copyhistory=no, width=' + w + ', height=' + h + ', top=' + tops + ', left=' + left);
			popupWindow.focus();
			return false;
		}

		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());

		gtag('config', 'UA-23581568-13');
	</script>
</body>

</html>