<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Ruang Keluhan Response</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Response</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('save-keluhan-response', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Subject</label>
                                                            <p style="background-color: #f4f7fa; padding: 10px; color: #000; text-align: justify;"><?= strtoupper($keluhan->subject) ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Keluhan Text</label>
                                                            <p style="background-color: #f4f7fa; padding: 10px; color: #000; text-align: justify;"><?= $keluhan->text ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="<?= base_url('keluhan') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                                        <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-sm-12">
                                                    <input type="hidden" name="id_keluhan" value="<?= $keluhan->id ?>">
                                                    <div class="form-group">
                                                        <label>Response Text <span style="color: red;">*</span></label>
                                                        <textarea class="form-control" name="response" rows="10" placeholder="Response here.." required oninvalid="this.setCustomValidity('Input Text')" oninput="setCustomValidity('')"><?= $keluhan->respon ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>