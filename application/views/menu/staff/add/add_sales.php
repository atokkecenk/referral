<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Sales Agent</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                $depan = $this->db->query("SELECT * FROM tb_id_code WHERE jenis = 'individu'")->row()->code_string;
                // $code = $this->db->query("SELECT MAX( RIGHT ( kode_individu, 5 ) ) AS `no` FROM tb_sales WHERE kode_individu LIKE '$depan%'")->row();
                $code = $this->db->query("SELECT MAX( RIGHT ( id_kedai, 5 ) ) AS `no` FROM tb_user WHERE id_kedai LIKE '$depan%'")->row();
                $code2 = '00000' . ($code->no + 1);
                $new_code = $depan . substr($code2, -5);
                print_r($new_code);
                if (validation_errors()) {
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ' . validation_errors() . '
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>';
                } elseif ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                if (isset($email_sess)) {
                    echo '
                    <input type="hidden" id="sessMail" value="modalMail">
                    <div class="modal fade" id="modalMail" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterTitle">Send Email Verification <i class="fas fa-paper-plane" style="color: #ffa426;"></i></h5>
                                </div>
                                <div class="modal-body text-center">
                                    ' . form_open('Sales/send_email', 'method="post"') . '
                                    <input type="hidden" name="nama" value="' . $email_sess['nama'] . '">
                                    <input type="hidden" name="user" value="' . $email_sess['username'] . '">
                                    <input type="hidden" name="email" value="' . $email_sess['email'] . '">
                                    <input type="hidden" name="verify" value="' . $email_sess['verify'] . '">
                                    
                                    Send email verify to <b>' . $email_sess['email'] . '</b> ?
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-primary">Send</button>
                                    ' . form_close() . '
                                </div>
                            </div>
                        </div>
                    </div>
                    ';
                    // <button type="button" class="btn btn-danger btn-exit-md-email my-2" data-dismiss="modal">Exit</button>
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Sales Agent Registration Form &rarr; Individu</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('save-sales', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NAMA <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control" name="nama" placeholder="Nama" required oninvalid="this.setCustomValidity('Input Nama')" oninput="setCustomValidity('')">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NO KAD PENGENALAN <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control kadPengenal" name="pengenal" placeholder="0123456789" maxlength="14" required oninvalid="this.setCustomValidity('Input No KAD Pengenal')" oninput="setCustomValidity('')">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label>JANTINA <span style="color: red;">*</span></label>
                                                        <div class="form-group">
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="lelaki" name="jantina" value="lelaki" class="custom-control-input">
                                                                <label class="custom-control-label" for="lelaki">Lelaki</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="perempuan" name="jantina" value="perempuan" class="custom-control-input">
                                                                <label class="custom-control-label" for="perempuan">Perempuan</label>
                                                            </div>
                                                            <!-- <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" id="other" name="jantina" value="other" class="custom-control-input">
                                                                <label class="custom-control-label" for="other">Other</label>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NO HANDPHONE <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control" name="no_hp" placeholder="No Handphone" required oninvalid="this.setCustomValidity('Input No Handphone')" oninput="setCustomValidity('')">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>ALAMAT <span style="color: red;">*</span></label>
                                                            <textarea class="form-control" rows="4" name="alamat" placeholder="Alamat" required oninvalid="this.setCustomValidity('Input Alamat')" oninput="setCustomValidity('')"></textarea>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>EMAIL</label>
                                                            <input type="email" class="form-control" name="email" placeholder="abu@gmail.com" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NEGERI <span style="color: red;">*</span></label>
                                                            <select class="form-control negeri" name="negeri">
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                foreach ($negeri as $ngi) {
                                                                ?>
                                                                    <option value="<?= $ngi->id_negeri ?>"><?= $ngi->nama ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>DAERAH <span style="color: red;">*</span></label>
                                                            <div class="daerah"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NAMA BANK <span style="color: red;">*</span></label>
                                                            <select class="form-control js-example-basic-single" name="bank" required oninvalid="this.setCustomValidity('Please Select One')" oninput="this.setCustomValidity('')">
                                                                <option value="">-- Nothing Selected --</option>
                                                                <?php
                                                                foreach ($bank as $bnk) {
                                                                ?>
                                                                    <option value="<?= $bnk->id ?>"><?= $bnk->nama_bank ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>NO AKAUN BANK <span style="color: red;">*</span></label>
                                                            <input type="text" class="form-control" name="no_account_bank" placeholder="No Akaun Bank" required oninvalid="this.setCustomValidity('Input No Akaun Bank')" oninput="this.setCustomValidity('')">
                                                            <small class="text-c-red">(NOTA KECIL : BAGI TUJUAN PEMBAYARAN KOMISYEN SAHAJA)</small>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>UPLOAD IMAGE GAMBAR SELFIE <span style="color: red;">*</span></label>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input imageSales" name="image" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                                                                <label class="custom-file-label">Choose file..</label>
                                                            </div>
                                                            <small class="text-c-red">Max file size 5 mb. Extension jpg, jpeg, png</small>
                                                        </div>
                                                        <img src="#" class="img-responsive" id="viewImageSales">
                                                        <div class="m-t-5" id="messageImageSales"></div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="text-right">
                                            <a href="<?= base_url('view-sales') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>