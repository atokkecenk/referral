<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Ruang Keluhan</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?= form_open('save-keluhan', 'method="post"') ?>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>Subject <span style="color: red;">*</span></label>
                                                            <select class="form-control" name="subject" required oninvalid="this.setCustomValidity('Select One')" oninput="setCustomValidity('')">
                                                                <option value="">Nothing Selected</option>
                                                                <option value="enquiry">ENQUIRY</option>
                                                                <option value="complaint">COMPLAINT</option>
                                                                <option value="suggestion">SUGGESTION</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Keluhan Text <span style="color: red;">*</span></label>
                                                            <textarea class="form-control" name="text" rows="6" placeholder="Text here.." required oninvalid="this.setCustomValidity('Input Text')" oninput="setCustomValidity('')"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="<?= base_url('keluhan') ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                                        <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save</button>
                                                    </div>
                                                </div>
                                                <?= form_close() ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>