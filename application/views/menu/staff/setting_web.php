<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Setting Website</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- Setting Text -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting Text</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open('set-text', 'method="post"') ?>
                                            <h6 style="font-weight: bold;" class="mb-4"><i class="feather icon-layers m-r-5"></i>Used For Websites</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Title Tab</label>
                                                        <input type="text" class="form-control" name="title" value="<?= $sett->title ?>" placeholder="Website Title" required oninvalid="this.setCustomValidity('Input Website Title')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Website Name</label>
                                                        <input type="text" class="form-control" name="name" value="<?= $sett->name ?>" placeholder="Website Name" required oninvalid="this.setCustomValidity('Input Website Name')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Tag Meta Description</label>
                                                        <textarea class="form-control" name="meta_description" rows="3" placeholder="Meta Description" required oninvalid="this.setCustomValidity('Input Meta Description')" oninput="setCustomValidity('')"><?= $sett->meta_description ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Tag Meta Keywords</label>
                                                        <textarea class="form-control" name="meta_keywords" rows="3" placeholder="Meta Keywords" required oninvalid="this.setCustomValidity('Input Meta Keywords')" oninput="setCustomValidity('')"><?= $sett->meta_keyword ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>No Handphone</label>
                                                        <input type="text" class="form-control" name="hp" value="<?= $sett->no_hp ?>" placeholder="No HP" required oninvalid="this.setCustomValidity('Input No HP')" oninput="setCustomValidity('')">
                                                        <small class="text-c-red">This number use for button whatsapp</small>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- ## -->
                                            <hr>
                                            <h6 style="font-weight: bold;" class="mb-4"><i class="feather icon-layers m-r-5"></i>Used For Websites, Reports</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <textarea class="form-control" name="alamat" id="alamat" rows="3" placeholder="Alamat" required oninvalid="this.setCustomValidity('Input Alamat')" oninput="setCustomValidity('')"><?= $sett->alamat ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input type="email" class="form-control" name="email" value="<?= $sett->email ?>" placeholder="Email Address" required oninvalid="this.setCustomValidity('Input Email Address')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Telephone</label>
                                                        <input type="text" class="form-control" name="tlp" value="<?= $sett->telephone ?>" placeholder="Website Title" required oninvalid="this.setCustomValidity('Input Telephone Number')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Faksimile</label>
                                                        <input type="text" class="form-control" name="faks" value="<?= $sett->faks ?>" placeholder="Website Name" required oninvalid="this.setCustomValidity('Input Faksimile')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Website</label>
                                                        <input type="text" class="form-control" name="website" value="<?= $sett->website ?>" placeholder="Website Address" required oninvalid="this.setCustomValidity('Input Website Address')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Setting Favicon -->
                            <div class="col-sm-5">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting Favicon</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open_multipart('set-favicon', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group card-upload">
                                                        <label>Favicon</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input favicon" name="favicon" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                                                            <label class="custom-file-label">Choose file..</label>
                                                        </div>
                                                        <small style="color: red; font-size: 11px;">Max size file 500 kilobyte. File <strong>jpeg, jpg, png, ico</strong></small>
                                                        <div class="text-center">
                                                            <img src="<?= base_url() . 'assets/images/setting/' . $sett->favicon ?>" class="img-responsive" id="favicon" alt="favicon" width="100px" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Setting Logo -->
                            <div class="col-sm-5">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting Logo Sidebar</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open_multipart('set-logo', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class=" form-group card-upload">
                                                        <label>Logo</label>
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input logo" name="logo" required oninvalid="this.setCustomValidity('Please Select File')" oninput="setCustomValidity('')">
                                                            <label class="custom-file-label">Choose file..</label>
                                                        </div>
                                                        <small style="color: red; font-size: 11px;">Max size file 1 megabyte. File <strong>jpeg, jpg, png</strong></small>
                                                        <div class="text-center">
                                                            <img src="<?= base_url() . 'assets/images/setting/' . $sett->logo ?>" class="img-responsive" id="logo" alt="logo" width="100px" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>