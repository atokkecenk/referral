<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Edit Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        Edit Data Order
                                    </div>
                                    <div class="card-body">
                                        <!-- <div class="loader">
                                            <div style="position: absolute; z-index: 2 !important;" id="loader"></div>
                                            <img class="loader-gif" src="../assets/template/loader/ripple.gif"> Loading..
                                        </div> -->
                                        <?= form_open('save-edit-order', 'method="post"') ?>
                                        <input type="hidden" name="id" value="<?= $order->id ?>">
                                        <input type="hidden" class="emailNotifOrder" value="<?= $order->to_email ?>">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Order Number</label>
                                                    <input type="text" class="form-control" value="<?= $order->order_number ?>" name="order_number" placeholder="Order Number">
                                                </div>
                                            </div>
                                            <?php
                                            if ($order->level == 'agent') {
                                                $kedai = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$order->kode'")->row();
                                            ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Name Sales Person</label>
                                                        <input type="text" class="form-control" value="<?= ucwords(strtolower($kedai->nama)) ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Nama Perniagaan</label>
                                                        <input type="text" class="form-control" value="<?= ucwords(strtolower($kedai->nama_perniagaan)) ?>" disabled>
                                                    </div>
                                                </div>
                                            <?php
                                            } elseif ($order->level == 'individu') {
                                                $individu = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$order->kode'")->row();
                                            ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Name Sales Person</label>
                                                        <input type="text" class="form-control" value="<?= $individu->nama ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Nama Perniagaan</label>
                                                        <input type="text" class="form-control" value="-" disabled>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Number IC</label>
                                                    <input type="text" class="form-control numberIc" value="<?= $order->number_ic ?>" name="number_ic" maxlength="14" placeholder="Number IC" required oninvalid="this.setCustomValidity('Input number IC')" oninput="this.setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Segment</label>
                                                <div class="form-group">
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="rdSegment" name="segment" class="custom-control-input" value="S" <?php if ($order->segment == 'S') {
                                                                                                                                                        echo 'checked';
                                                                                                                                                    } ?>>
                                                        <label class="custom-control-label" for="rdSegment">SME</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="rdSegment2" name="segment" class="custom-control-input" value="C" <?php if ($order->segment == 'C') {
                                                                                                                                                        echo 'checked';
                                                                                                                                                    } ?>>
                                                        <label class="custom-control-label" for="rdSegment2">CONSUMER</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" class="form-control" value="<?= $order->name_customer ?>" name="name_customer" placeholder="Name Customer">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Contact Number</label>
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="<?= $order->contact_1 ?>" name="contact1" placeholder="0123456789">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2"><i class="feather icon-smartphone"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>2nd Contact Number</label>
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="<?= $order->contact_2 ?>" name="contact2" placeholder="If no 2nd contact put 0">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2"><i class="feather icon-smartphone"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Email</label>
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <input type="email" class="form-control" value="<?= $order->email ?>" name="email" placeholder="farid@gmail.com" required oninvalid="this.setCustomValidity('Input email address')" oninput="this.setCustomValidity('')">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2"><i class="feather icon-at-sign"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Installation Address</label>
                                                    <input type="text" class="form-control" value="<?= $order->address ?>" name="install_address" placeholder="Installation Address">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Billing Address</label>
                                                    <input type="text" class="form-control" value="<?= $order->billing_address ?>" name="billing_address" placeholder="if same put (-)">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Packpage to be subscribed</label>
                                                    <select class="form-control" name="package">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        $get_pkg = $this->db->query("SELECT * FROM tb_package 
                                                                                    WHERE active = 'Y' 
                                                                                    ORDER BY 
                                                                                        CASE 
                                                                                        WHEN type = 'C' THEN 0 
	                                                                                    WHEN type = 'S' THEN 1 
                                                                                        END
                                                                                    ")->result();
                                                        foreach ($get_pkg as $pkg) {
                                                            switch ($pkg->type) {
                                                                case 'S':
                                                                    $pk = 'SME';
                                                                    break;

                                                                case 'C':
                                                                    $pk = 'CONSUMER';
                                                                    break;
                                                            }
                                                        ?>
                                                            <option value="<?= $pkg->id_pkg ?>" <?php if ($pkg->id_pkg == $order->package) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?= $pk . ' &rarr; ' . $pkg->package ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Installation Type</label>
                                                <div class="form-group">
                                                    <?php
                                                    $no = 1;
                                                    foreach ($type as $tp) {
                                                    ?>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="radio<?= $no ?>" name="type" class="custom-control-input" value="<?= $tp->id ?>" <?php if ($tp->id == $order->type) {
                                                                                                                                                                            echo 'checked';
                                                                                                                                                                        } ?>>
                                                            <label class="custom-control-label" for="radio<?= $no ?>"><?= $tp->value ?></label>
                                                        </div>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Commission</label>
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">RM</span>
                                                        </div>
                                                        <input type="text" class="form-control" name="comission" value="<?= $order->commision ?>" placeholder="0.00">
                                                    </div>
                                                    <small class="form-text text-c-red">Use point (.)</small>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Referral Person Staff</label>
                                                    <select class="form-control" name="refferral_staff">
                                                        <option value="">Nothing Selected</option>
                                                        <?php
                                                        $get_user = $this->db->query("SELECT * FROM tb_user 
                                                                        WHERE active = 'Y' AND (level = 'agent' or level = 'user') 
                                                                    ")->result();
                                                        foreach ($get_user as $staff) { ?>
                                                            <option value="<?= $staff->id ?>" 
                                                                <?php 
                                                                    if ($staff->id == $order->refferral_staff) {
                                                                        echo 'selected';
                                                                    } 
                                                                ?>
                                                            >
                                                            <?= $staff->name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-4">

                                            </div>
                                        </div>
                                        <!-- <hr> -->
                                        <div class="row">
                                            <div class="col-md-12 text-center">
                                                <input type="hidden" name="addStatusOrder" id="addStatusOrder" value="<?= $order->status ?>">
                                                <button type="button" class="btn btn-warning btn-status-order mb-2" data-mode="pending" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>" title="Pending"><i class="feather icon-clock"></i>PENDING</button>
                                                <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#modalWaiters" title="Waiters List"><i class="feather icon-list"></i>WAITERS LIST</button>
                                                <button type="button" class="btn btn-secondary btn-status-order mb-2" data-mode="processing" data-id="<?= $order->id ?>" data-ordernum="<?= $order->order_number ?>" data-toggle="modal" data-target="#modalProsesOrder" title="Processing"><i class="feather icon-rotate-cw"></i>PROCESSING</button>
                                                <button type="button" class="btn btn-success btn-status-order mb-2" data-mode="paid" data-id="<?= $order->id ?>" data-ordernum="<?= $order->order_number ?>" data-code="<?= $order->name_sales ?>" data-comms="<?= $order->commision ?>" data-toggle="modal" data-target="#modalPaidOrder" title="Paid"><i class="feather icon-check-circle"></i>PAID</button>
                                                <button type="button" class="btn btn-danger btn-status-order mb-2" data-mode="cancel" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>" title="Cancel"><i class="feather icon-x-circle"></i>CANCEL</button>
                                                <button type="button" class="btn btn-dark btn-status-order mb-2" data-mode="rejected" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>" title="Rejected"><i class="feather icon-corner-up-left"></i>REJECTED</button>
                                                <button type="button" class="btn btn-info btn-status-order mb-2" data-mode="completed" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>" title="Completed"><i class="feather icon-file-text"></i>COMPLETED</button>
                                            </div>
                                            <div class="col-md-12 mt-2">
                                                <div class="alert alert-warning"><i class="feather icon-info mr-2"></i>Status This Order <b><?= strtoupper($order->status) ?></b></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Remarks</label>
                                                <div class="form-group">
                                                    <textarea class="form-control" rows="4" name="remarks" id="inputRemarks" placeholder="Input Remarks"><?= $order->remarks ?></textarea>
                                                    <?php
                                                    if ($order->status == "processing" && $order->remarks == '') {
                                                        echo "<small class='text-c-red' style='font-weight: bold; font-size: 13px;'>Input this remarks !!</small>";
                                                    } elseif ($order->status == "cancel" || $order->status == "rejected") {
                                                        echo "<small class='text-c-red' style='font-weight: bold; font-size: 13px;'>Input this remarks !!</small>";
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <a href="<?= base_url('view-order/' . $order->status) ?>" class="btn btn-secondary"><i class="feather icon-arrow-left"></i>Back</a>
                                            <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Save | Send Update Email</button>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Proses Order -->
<div class="modal fade" id="modalProsesOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Processing</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3"><i class="feather icon-copy blue mr-2"></i>PROCESSING</th>
                            </tr>
                        </thead>
                    </table>
                    <?= form_open('update-order-number', 'method="post"') ?>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Order Number</label>
                            <input type="hidden" name="id" class="idDetailOrder">
                            <input type="text" class="form-control" name="order_num" id="ordernumDetailOrder" placeholder="Order Number">
                            <!-- <div class="input-group">
                                <input type="text" class="form-control" name="order_num" id="ordernumDetailOrder" placeholder="Order Number">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" title="Save or Update Order Number"><i class="feather icon-save"></i>Save</button>
                                </div>
                            </div> -->
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Paid Order -->
<div class="modal fade" id="modalPaidOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order Paid</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center"><i class="feather icon-copy blue mr-2"></i>RESIT & COMISSION</th>
                            </tr>
                        </thead>
                    </table>
                    <?= form_open_multipart('save-order/paid', 'method="post"') ?>
                    <input type="hidden" name="id" class="idDetailOrder">
                    <input type="hidden" name="code" class="codeDetailOrder">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Upload Resit</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input uploadReceiptPaid" name="receipt">
                                <label class="custom-file-label">Choose file..</label>
                            </div>
                            <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                            <div class="mt-1" id="uploadReceiptTeks"></div>
                        </div>
                        <div class="form-group">
                            <label>Amount Comission</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">RM</span>
                                </div>
                                <input type="text" name="comission" class="form-control commsDetailOrder" placeholder="98.00">
                            </div>
                            <small class="form-text text-c-red">Use point (.)</small>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-12 mb-1">
                        <div class="row">
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="feather icon-x"></i>Close</button>
                            </div>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalWaiters" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <!-- Click One
                <br> -->
                <!-- </div>
          <div class="modal-footer"> -->
            <!-- 
                PF = Port Full
                UP = Upcoming Projects 
            -->
                <button type="button" class="btn btn-primary my-2 btn-status-order" data-mode="port-full" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>">PORT FULL</button>
                <button type="button" class="btn btn-primary my-2 btn-status-order" data-mode="upcoming-projects" data-id="<?= $order->id ?>" data-code="<?= $order->name_sales ?>">UPCOMING PROJECTS</button>
                <div id="cobacoba"></div>
            </div>
        </div>
    </div>
</div>