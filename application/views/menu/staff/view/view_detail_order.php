<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Detail Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Detail Order</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" colspan="6"><i class="feather icon-file-text m-r-5" style="color: #04a9f5;"></i>DATA DETAIL ORDER</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>ORDER NUMBER</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%">
                                                            <?php
                                                            $on = ($data->order_number == '') ? '<i>Please update</i>' : $data->order_number;
                                                            echo $on;
                                                            ?>
                                                        </td>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>INSTALLATION DATE</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= $data->new_install ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>NAME SALES PERSON</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->name_sales_person) ?></td>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SUBMITTED DATE</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= $data->new_input ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>CUSTOMER NAME</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->name_customer) ?></td>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>COMMISSION</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= 'RM ' . number_format($data->commision, 2) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>SEGMENT</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->name_segment) ?></td>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>STATUS</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?php
                                                                        switch ($data->status) {
                                                                            case "pending":
                                                                                $color = "#ffeb00fa";
                                                                                break;

                                                                            case "port-full":
                                                                                $color = "#007bff";
                                                                                break;

                                                                            case "upcoming-projects":
                                                                                $color = "#007bff";
                                                                                break;

                                                                            case "processing":
                                                                                $color = "#748892";
                                                                                break;

                                                                            case "paid":
                                                                                $color = "#1de9b6";
                                                                                break;

                                                                            case "cancel":
                                                                                $color = "#f44236";
                                                                                break;

                                                                            case "rejected":
                                                                                $color = "#37474f";
                                                                                break;

                                                                            case "completed":
                                                                                $color = "#3ebfea";
                                                                                break;
                                                                        }
                                                                        echo "<i class='fas fa-circle f-10 m-r-10' style='color:" . $color . ";'></i>" . strtoupper($data->status);
                                                                        ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>CONTACT NUMBER</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->contact_1) ?></td>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>REMARKS</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?php
                                                                        $query = $this->db->query("SELECT
                                                                                                    a.*,
                                                                                                    DATE_FORMAT(a.date_input, '%d %b, %Y %h:%i %p') as new_date
                                                                                                FROM
                                                                                                    tb_history a 
                                                                                                WHERE
                                                                                                    a.id = '$data->id' 
                                                                                                    AND a.note_2 IS NOT NULL 
                                                                                                    AND a.`status` != 'paid' 
                                                                                                GROUP BY
                                                                                                    a.id_history
                                                                                                ORDER BY
	                                                                                                a.id_history DESC")->result();
                                                                        $no = 1;
                                                                        foreach ($query as $rmk) {
                                                                            echo '<p style="font-size: 12px;"><b class="btn btn-danger btn-margin">' . $no . '.</b> ' . $rmk->new_date . ' | <i class="text-c-red">"' . $rmk->note_2 . '"</i></p>';
                                                                            $no++;
                                                                        }
                                                                        ?>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>2ND CONTACT NUMBER</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->contact_2) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>EMAIL</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= $data->email ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>INSTALLATION ADDRESS</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->address) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>BILLING ADDRESS</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->billing_address) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>PACKAGE</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->name_package) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>INSTALLATION TYPE</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?= strtoupper($data->name_type) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" style="font-weight: bold;"><i class="feather icon-chevron-right m-r-5"></i>DELL NUMBER</td>
                                                        <td width="2%">:</td>
                                                        <td width="28%"><?php
                                                                        $del = ($data->del_number == '') ? '-' : $data->del_number;
                                                                        echo $del;
                                                                        ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h5><i class="feather icon-paperclip m-r-10"></i>File Receipt</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php
                                        if ($data->receipt == '') {
                                            echo '<div class="alert alert-danger"><i class="feather icon-alert-circle m-r-10"></i>No File Receipt Upload.</div>';
                                        } else {
                                            echo '<iframe src="' . base_url() . 'assets/images/order/receipt/' . $data->receipt . '" class="img-responsive file-upload" alt="file-receipt" height="550px"></iframe>';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>