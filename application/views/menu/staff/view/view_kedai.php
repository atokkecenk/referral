<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Kedai</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Kedai</h5>
                                        <a href="<?= base_url('add-kedai') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-kedai">
                                                <thead>
                                                    <tr>
                                                        <th>NO.</th>
                                                        <th>NAMA</th>
                                                        <th>NO KAD PENGENAL</th>
                                                        <th>NAMA PERNIAGAAN</th>
                                                        <th>NO PENDAFTARAN SYARIKAT</th>
                                                        <th>JANTINA</th>
                                                        <th>NO HP</th>
                                                        <th>ALAMAT</th>
                                                        <th>EMAIL</th>
                                                        <th>BARCODE</th>
                                                        <th>FOTO</th>
                                                        <th>BANK</th>
                                                        <th>AKAUN BANK</th>
                                                        <th><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete', 'method="post"') ?>
                <input type="hidden" name="mode" class="mode">
                <input type="hidden" name="key" class="key">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail Foto -->
<div class="modal fade" id="detailFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <!-- <div class="modal-content"> -->
        <div style="background-color: #fff; padding: .8em;">
            <div class="detailFotoKedai"></div>
        </div>
        <!-- </div> -->
    </div>
</div>

<!-- Modal Change Foto -->
<div class="modal fade" id="modalChangeFotoKedai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chage Foto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart('change-foto', 'method="post"') ?>
                <input type="hidden" name="id" class="idChageFoto">
                <input type="hidden" name="mode" value="kedai">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Upload Foto</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input changeFoto" name="image">
                            <label class="custom-file-label">Choose file..</label>
                        </div>
                        <small style="color: red; font-size: 11px;">Max size file 5 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                        <center>
                            <div class="mt-3 changeFotoView"></div>
                        </center>
                    </div>
                </div>
                <hr>
                <div class="col-sm-12 mb-1">
                    <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>