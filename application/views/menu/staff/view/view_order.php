<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Order</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>List Data Order</h5>
                                    </div>
                                    <div class="card-body">
                                        <input type="hidden" id="modeViewOrder" value="<?= $mode ?>">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-order">
                                                <thead>
                                                    <tr>
                                                        <th>NO.</th>
                                                        <th width="14%">ORDER NUMBER</th>
                                                        <th>CUSTOMER NAME</th>
                                                        <th>SALES AGENT</th>
                                                        <th>PACKAGE</th>
                                                        <th>SEGMENT</th>
                                                        <th>COMMISSION</th>
                                                        <th width="11%">STATUS</th>
                                                        <th width="15%">REMARKS</th>
                                                        <th>DATE INPUT</th>
                                                        <th><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete', 'method="post"') ?>
                <?php 
                if($this->uri->segment(1) == 'view-order'){
                    echo '
                    <input type="hidden" name="rdr" value="'. $this->uri->segment(2).'">';
                    
                }
                ?>
                <input type="hidden" name="mode" class="mode">
                <input type="hidden" name="key" class="key">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detail -->
<div class="modal fade" id="modalDetailOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3">DETAIL ORDER</th>
                            </tr>
                            <tr>
                                <?= form_open('update-order-number', 'method="post"') ?>
                                <th width="20%">Order Number</th>
                                <th width="2%">:</th>
                                <th width="78%" style="font-weight: normal;">
                                    <input type="hidden" name="id" class="idDetailOrder">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="order_num" id="ordernumDetailOrder" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save & Update Order Number"><i class="feather icon-save"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <?= form_open('update-comission', 'method="post"') ?>
                                <th width="20%">Commission</th>
                                <th width="2%">:</th>
                                <th width="78%" style="font-weight: normal;">
                                    <input type="hidden" name="id" class="idDetailOrder">
                                    <input type="hidden" name="mode" class="modeviewDetailOrder">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">RM</span>
                                        </div>
                                        <input type="text" class="form-control" name="comission" id="comissionDetailOrder" placeholder="0.00" required oninvalid="this.setCustomValidity('Input Comission')" oninput="setCustomValidity('')">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save & Update Comission"><i class="feather icon-save"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                            <tr>
                                <?= form_open('update-referral-staff', 'method="post"') ?>
                                <th width="20%">Referral Person</th>
                                <th width="2%">:</th>
                                <th width="78%" style="font-weight: normal;">
                                    <input type="hidden" name="id" class="idDetailOrder">
                                    <input type="hidden" name="mode" class="modeviewDetailOrder">
                                    <div class="input-group">
                                        <select class="form-control" name="refferral_staff">
                                            <option value="">Nothing Selected</option>
                                            <?php
                                            $get_user = $this->db->query("SELECT * FROM tb_user 
                                                                        WHERE active = 'Y' AND (level = 'agent' or level = 'user')
                                                                        ")->result();
                                            foreach ($get_user as $staff) { ?>
                                                <option value="<?= $staff->id ?>">
                                                    <?= $staff->name ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary" title="Save & Update Refferral Staff Comission"><i class="feather icon-save"></i>Save</button>
                                        </div>
                                    </div>
                                </th>
                                <?= form_close() ?>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <div id="extDetailOrder"></div>
                </div>
            </div>
        </div>
    </div>
</div>