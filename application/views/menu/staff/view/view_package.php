<!-- [ Main Content ] start -->

<div class="pcoded-main-container">

    <div class="pcoded-wrapper">

        <div class="pcoded-content">

            <div class="pcoded-inner-content">

                <!-- [ breadcrumb ] start -->

                <div class="page-header">

                    <div class="page-block">

                        <div class="row align-items-center">

                            <div class="col-md-12">

                                <div class="page-header-title">

                                    <h5 class="m-b-10">View Package</h5>

                                </div>

                                <ul class="breadcrumb">

                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>

                                    <li class="breadcrumb-item">Active</li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

                <!-- [ breadcrumb ] end -->

                <?php

                if ($this->session->flashdata('success')) {

                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {

                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }

                ?>

                <div class="main-body">

                    <div class="page-wrapper">

                        <!-- [ Main Content ] start -->

                        <div class="row">

                            <div class="col-sm-12">

                                <div class="card">

                                    <div class="card-header">

                                        <h5>Data Package</h5>

                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddPackage"><i class="feather icon-plus"></i>Add New</a>

                                    </div>

                                    <div class="card-body">

                                        <div class="table-responsive">

                                            <table class="table table-hover" id="tbl-view-package">

                                                <thead>

                                                    <tr>

                                                        <th class="text-center">NO.</th>

                                                        <th>TYPE</th>

                                                        <th>PACKAGE</th>

                                                        <th>SUBMITTED DATE</th>

                                                        <th class="text-center"><i class="feather icon-settings"></i></th>

                                                    </tr>

                                                </thead>

                                                <tbody>

                                                    <?php

                                                    $no = 1;

                                                    foreach ($all_package as $pkg) {

                                                        $val_pkg = ($pkg->type == 'S') ? 'SME' : 'CONSUMER';

                                                    ?>

                                                        <tr>

                                                            <td align="center"><?= $no . '.' ?></td>

                                                            <td><?= $val_pkg ?></td>

                                                            <td><?= $pkg->package ?></td>

                                                            <td><?= $pkg->new_date ?></td>

                                                            <td align="center">

                                                                <a href="#" data-id="<?= $pkg->id_pkg ?>" data-type="<?= $pkg->type ?>" data-package="<?= $pkg->package ?>" data-toggle="modal" data-target="#modalEditPackage" class="btn btn-primary modalEditPackage" title="Edit"><i class="feather icon-edit r-no-margin"></i></a>

                                                                <button type="button" class="btn btn-danger modalDelete" data-mode="package" data-key="<?= $pkg->id_pkg ?>"><i class="feather icon-trash r-no-margin"></i></button>

                                                            </td>

                                                        </tr>

                                                    <?php

                                                        $no++;
                                                    }

                                                    ?>

                                                </tbody>

                                            </table>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>



<!-- Modal Hapus -->

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>

                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">

            <span aria-hidden="true">&times;</span>

          </button> -->

            </div>

            <div class="modal-body text-center">

                <?= form_open('delete', 'method="post"') ?>

                <input type="hidden" name="mode" class="mode">

                <input type="hidden" name="key" class="key">

                Are you sure delete this data ?

                <br>

                <!-- </div>

          <div class="modal-footer"> -->

                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>

                <button type="submit" class="btn btn-primary">Yes</button>

                <?= form_close() ?>

            </div>

        </div>

    </div>

</div>



<!-- Modal Add -->

<div class="modal fade" id="modalAddPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalLabel">Add New Package</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                </button>

            </div>

            <div class="modal-body">

                <div class="table-responsive">

                    <?= form_open('save-package', 'method="post"') ?>

                    <div class="col-sm-12">

                        <div class="form-group selectType">

                            <label>Type</label>

                            <select class="form-control" name="type" required oninvalid="this.setCustomValidity('Select one')" oninput="this.setCustomValidity('')">

                                <option value="">Nothing Selected</option>

                                <option value="S">SME</option>

                                <option value="C">CONSUMER</option>

                            </select>

                        </div>

                        <div class="form-group">

                            <label>Package</label>

                            <input type="text" class="form-control" name="package" placeholder="Package Name" required oninvalid="this.setCustomValidity('Please input package name')" oninput="this.setCustomValidity('')">

                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save</button>

                        </div>

                    </div>

                    <?= form_close() ?>

                </div>

            </div>

        </div>

    </div>

</div>



<!-- Modal Edit -->

<div class="modal fade" id="modalEditPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

            <div class="modal-header">

                <h5 class="modal-title" id="exampleModalLabel">Edit Package</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                    <span aria-hidden="true">&times;</span>

                </button>

            </div>

            <div class="modal-body">

                <div class="table-responsive">

                    <?= form_open('update-package', 'method="post"') ?>

                    <input type="hidden" name="id" id="typeId">

                    <div class="col-sm-12">

                        <div class="form-group">

                            <label>Type <span style="color: red;">*</span></label>

                            <select class="form-control" name="type" id="selectType" required oninvalid="this.setCustomValidity('Select one')" oninput="this.setCustomValidity('')">

                                <option value="">Nothing Selected</option>

                                <option value="S">SME</option>

                                <option value="C">CONSUMER</option>

                            </select>

                        </div>

                        <div class="form-group">

                            <label>Package <span style="color: red;">*</span></label>

                            <input type="text" class="form-control editPackage" name="package" placeholder="Package Name" required oninvalid="this.setCustomValidity('Please input package name')" oninput="this.setCustomValidity('')">

                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Update</button>

                        </div>

                    </div>

                    <?= form_close() ?>

                </div>

            </div>

        </div>

    </div>

</div>