<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View User</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data User</h5>
                                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddUser"><i class="feather icon-plus"></i>Add User</a>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-user">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">NO.</th>
                                                        <th>CREDENTIAL</th>
                                                        <th>CODE</th>
                                                        <th>USERNAME</th>
                                                        <th>VERIFIED</th>
                                                        <th width="15%">SUBMITTED DATE</th>
                                                        <th width="14%">LAST UPDATE</th>
                                                        <th class="text-center" width="20%"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete-user', 'method="post"') ?>
                <input type="hidden" name="id" class="idUser">
                <input type="hidden" name="code" class="idCode">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Reset -->
<div class="modal fade" id="modalResetPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('reset-password', 'method="post"') ?>
                <input type="hidden" name="id" class="idUser">
                Are you sure reset password for this username ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit Password -->
<div class="modal fade" id="modalEditPass" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Username & Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('edit-usernamepassword', 'method="post"') ?>
            <div class="modal-body">
                <input type="hidden" name="id" id="editPassword">
                <div class="form-group">
                    <label>Username <span style="color: red;">*</span></label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username" required oninvalid="this.setCustomValidity('Input username')" oninput="this.setCustomValidity('')">
                </div>
                <div class="form-group">
                    <label>New Password <span style="color: red;">*</span></label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" id="password1" placeholder="Set New Password" required oninvalid="this.setCustomValidity('Input new password')" oninput="setCustomValidity('')">
                        <div class="input-group-append">
                            <span class="input-group-text" id="passIcon"></span>
                        </div>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input showPassword" id="showPassword">
                    <label class="custom-control-label" for="showPassword"><span class="textShow"></span></label>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Update & Restart</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Add New User -->
<div class="modal fade" id="modalAddUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?= form_open('add-new-user', 'method="post"') ?>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama <span style="color: red;">*</span></label>
                    <input type="text" class="form-control" name="nama" placeholder="Nama Penuh" required oninvalid="this.setCustomValidity('Input nama penuh')" oninput="this.setCustomValidity('')">
                </div>
                <div class="form-group">
                    <label>Username <span style="color: red;">*</span></label>
                    <input type="text" class="form-control" name="username" placeholder="Username" required oninvalid="this.setCustomValidity('Input username')" oninput="this.setCustomValidity('')">
                </div>
                <div class="form-group">
                    <label>New Password <span style="color: red;">*</span></label>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="password" placeholder="Set Password" id="password2" required oninvalid="this.setCustomValidity('Input new password')" oninput="setCustomValidity('')">
                        <div class="input-group-append">
                            <span class="input-group-text" id="passIcon2"></span>
                        </div>
                    </div>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input showPassword" id="showPassword2">
                    <label class="custom-control-label" for="showPassword2"><span class="textShow"></span></label>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-outline-danger" data-dismiss="modal"><i class="feather icon-x-circle"></i>Cancel</button> -->
                <button type="submit" class="btn btn-primary"><i class="feather icon-check-circle"></i>Save</button>
            </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<!-- Modal Verify -->
<div class="modal fade" id="modalVerifyMsg" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Information <i class="fas fa-info-circle" style="color: #ffa426;"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p class="mt-2 mb-4" style="font-size: 16px;"><b>Please,</b> verify this username first !!</p>
                <!-- <button type="button" class="btn btn-danger my-2 mt-4" data-dismiss="modal">Close</button> -->
            </div>
        </div>
    </div>
</div>