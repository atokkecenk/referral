<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Code</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Code</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-code">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">NO.</th>
                                                        <th>CREDENTIAL</th>
                                                        <th>CODE</th>
                                                        <th>VIEW</th>
                                                        <th>LAST UPDATE</th>
                                                        <th class="text-center"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no = 1;
                                                    foreach ($all_code as $code) {
                                                    ?>
                                                        <tr>
                                                            <td align="center"><?= $no . '.' ?></td>
                                                            <td><?= strtoupper($code->jenis) ?></td>
                                                            <td><?= $code->code_string ?></td>
                                                            <td><?= 'Example Code : ' . $code->code_string . '000XX' ?></td>
                                                            <td><?= $code->new_edit_date ?></td>
                                                            <td align="center"><button type="button" class="btn btn-primary btn-edit-code" data-id="<?= $code->id_code ?>" data-code="<?= $code->code_string ?>" data-toggle="modal" data-target="#modalEditCode" title="Edit"><i class="feather icon-edit r-no-margin"></i></button></td>
                                                        </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalEditCode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="3"><i class="feather icon-copy blue mr-2"></i>DETAIL</th>
                            </tr>
                        </thead>
                    </table>
                    <?= form_open('update-code', 'method="post"') ?>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Code</label>
                            <input type="hidden" name="id" id="idCodeEdit">
                            <input type="text" class="form-control" name="code" id="stringCodeEdit" placeholder="MT" required oninvalid="this.setCustomValidity('Input code')" oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>