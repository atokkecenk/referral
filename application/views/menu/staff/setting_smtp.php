<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Setting SMTP Email</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <div class="row">
                            <!-- Setting Landing Page -->
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting SMTP</h5>
                                    </div>
                                    <div class="card-body">
                                        <?php foreach ($setting as $sett) { ?>
                                            <?= form_open('set-smtp-save', 'method="post"') ?>
                                            <input type="hidden" name="id" value="<?= $sett->id ?>">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Host</label>
                                                        <input class="form-control" name="host" value="<?= $sett->host ?>" placeholder="Host Mail" required oninvalid="this.setCustomValidity('Input Host Mail')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Port</label>
                                                        <input type="number" min="0" max="9999" class="form-control" name="port" value="<?= $sett->port ?>" placeholder="Port" required oninvalid="this.setCustomValidity('Input Port')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Username Email</label>
                                                        <input class="form-control" name="user" value="<?= $sett->username ?>" placeholder="Username Email" required oninvalid="this.setCustomValidity('Input Username Email')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Password Email</label>
                                                        <input type="text" class="form-control" name="pass" value="<?= $sett->password ?>" placeholder="Password Email" required oninvalid="this.setCustomValidity('Input Password Email')" oninput="setCustomValidity('')">
                                                    </div>
                                                    <!-- <label>Email</label>
                                                    <div class="form-group">
                                                        <div class="input-group mb-3">
                                                            <input type="password" class="form-control" value="<?= $sett->password ?>" name="email" placeholder="farid@gmail.com" required oninvalid="this.setCustomValidity('Input Password Email')" oninput="this.setCustomValidity('')">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon2"><i class="feather icon-at-sign"></i></span>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>From Email</label>
                                                        <input class="form-control" name="from" value="<?= $sett->from ?>" placeholder="Address From Email" required oninvalid="this.setCustomValidity('Input Address From Email')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Reply To Email</label>
                                                        <input type="text" class="form-control" name="reply" value="<?= $sett->reply_to ?>" placeholder="Address Reply Email" required oninvalid="this.setCustomValidity('Input Address Reply Email')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Sender Name</label>
                                                        <input class="form-control" name="sender" value="<?= $sett->sender_name ?>" placeholder="Sender Name Email" required oninvalid="this.setCustomValidity('Input Sender Name Email')" oninput="this.setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Subject</label>
                                                        <input type="text" class="form-control" name="subject" value="<?= $sett->subject ?>" placeholder="Subject Email" required oninvalid="this.setCustomValidity('Input Subject Email')" oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit"><i class="feather icon-check-circle"></i>Update</button>
                                            </div>
                                            <?= form_close() ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>