<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Setting</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Setting</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td colspan="3" align="center"><b>Notification Email</b></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="8%" align="center">1.</td>
                                                                <td width="70%">Notification Status Order</td>
                                                                <td width="22%" align="center"><label class="switch">
                                                                        <input type="checkbox" class="switchNotif" data-code="<?= $code ?>" <?= $notif_order ?>>
                                                                        <span class="slider round"></span>
                                                                    </label></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Detail -->
    <div class="modal fade" id="modalDetailOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Status Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center" colspan="3">DETAIL ORDER</th>
                                </tr>
                                <tr>
                                    <?= form_open('update-order-number', 'method="post"') ?>
                                    <th width="20%">Order Number</th>
                                    <th width="2%">:</th>
                                    <th width="78%" style="font-weight: normal;">
                                        <input type="hidden" name="id" class="idDetailOrder">
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="order_num" id="ordernumDetailOrder" placeholder="Order Number" required oninvalid="this.setCustomValidity('Input Order Number')" oninput="setCustomValidity('')">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-primary" title="Save & Update Order Number"><i class="feather icon-save"></i>Save</button>
                                            </div>
                                        </div>
                                    </th>
                                    <?= form_close() ?>
                                </tr>
                                <tr>
                                    <?= form_open('update-comission', 'method="post"') ?>
                                    <th width="20%">Comission</th>
                                    <th width="2%">:</th>
                                    <th width="78%" style="font-weight: normal;">
                                        <input type="hidden" name="id" class="idDetailOrder">
                                        <input type="hidden" name="mode" class="modeviewDetailOrder">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">RM</span>
                                            </div>
                                            <input type="text" class="form-control" name="comission" id="comissionDetailOrder" placeholder="0.00" required oninvalid="this.setCustomValidity('Input Comission')" oninput="setCustomValidity('')">
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-primary" title="Save & Update Comission"><i class="feather icon-save"></i>Save</button>
                                            </div>
                                        </div>
                                    </th>
                                    <?= form_close() ?>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div id="extDetailOrder"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>