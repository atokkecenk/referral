<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Page Promo</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Promo</h5>
                                        <!-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalAddPackage"><i class="feather icon-plus"></i>Add New</a> -->
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover tb-vpromo" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="80">NO.</th>
                                                        <th width="600">DETAIL</th>
                                                        <th width="80">FILE</th>
                                                        <th width="150">STATUS</th>
                                                        <th>SET</th>
                                                        <th class="text-center"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody style="color: #505050;">
                                                    <?php
                                                    $no = 1;
                                                    foreach ($data as $dt) {
                                                        // $color = ($no % 2 == 0) ? 'background-color: #fff;' : 'background-color: #feffea;';
                                                        if ($dt->approval == '') {
                                                            $color = 'background-color: #fff;';
                                                            $sts =  '-- no status --';
                                                            $rows = 3;
                                                        } elseif ($dt->approval == 'N') {
                                                            $color = 'background-color: #ff00001c;';
                                                            $sts = '<span class="merah-appv"><i class="feather icon-x"></i> Not Approve</span>';
                                                            $rows = 4;
                                                        } else {
                                                            $color = 'background-color: #00ff0014;';
                                                            $sts = '<span class="hijau-appv"><i class="feather icon-check"></i> Approve</span>';
                                                            $rows = 3;
                                                        }
                                                    ?>
                                                        <tr style="<?= $color ?>" class="my-td">
                                                            <td rowspan="<?= $rows ?>" align="center"><?= $no . '.' ?></td>
                                                            <td><b>Category :</b> <?= $dt->kategori ?></td>
                                                            <td rowspan="<?= $rows ?>" align="center"><img class="filePromo" src="<?= base_url('assets/images/setting/files.png') ?>" style="cursor: pointer;" width="40px" data-file="<?= $dt->file ?>" title="Click to view file"></td>
                                                            <td rowspan="<?= $rows ?>"><?= $sts ?></td>
                                                            <td rowspan="<?= $rows ?>">
                                                                <?php
                                                                if ($dt->approval == null) {
                                                                    echo '<button type="button" class="btn btn-success btnApprove" data-id="' . $dt->id . '" data-file="' . $dt->file . '"><i class="feather icon-thumbs-up"></i>Approve</button></td>';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td rowspan="<?= $rows ?>" align="center">
                                                                <button class="btn btn-danger delPromo" data-id="<?= $dt->id ?>" data-img="<?= $dt->file ?>"><i class="feather icon-trash r-no-margin"></i></button>
                                                            </td>
                                                        </tr>
                                                        <tr style="<?= $color ?>" class="my-td">
                                                            <td><b>Nama Perniagaan :</b> <?= $dt->nama ?></td>
                                                        </tr>
                                                        <tr style="<?= $color ?>" class="my-td">
                                                            <td><b>Description :</b> <?= $dt->desc ?></td>
                                                        </tr>
                                                    <?php
                                                        if ($dt->approval == 'N') {
                                                            echo '<tr style="' . $color . '" class="my-td">
                                                                <td class="merah-appv"><b>Reason : ' . $dt->reason . '</b></td>
                                                            </tr>';
                                                        }
                                                        $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete', 'method="post"') ?>
                <input type="hidden" name="mode" class="mode">
                <input type="hidden" name="key" class="key">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Approve -->
<div class="modal fade" id="modalApprove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Approve Promo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <?= form_open('save-approve', 'method="post" id="frmAppv"') ?>
                    <div class="row">
                        <input type="hidden" name="id" id="idAppv">
                        <div class="col-sm-12 to-center">
                            <span class="showImgPromo"></span>
                        </div>
                        <div class="col-sm-12 mt-10">
                            <p>
                                <i class="feather icon-thumbs-up text-success"></i> : If approve, this design <span class="text-success">show</span> in page promo.<br>
                                <i class="feather icon-thumbs-down text-danger"></i> : If not approve, this design <span class="text-danger">not show</span> in page promo.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success btn-block"><i class="feather icon-thumbs-up"></i>Approve</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-danger btn-block not-appv"><i class="feather icon-thumbs-down"></i>Not Approve</button>
                        </div>
                        <div class="col-sm-12 mt-10 validasiAppv">
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal View File -->
<div class="modal fade" id="modalViewFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">File Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body detailFilePromo">
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalEditPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Package</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <?= form_open('update-package', 'method="post"') ?>
                    <input type="hidden" name="id" id="typeId">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Type <span style="color: red;">*</span></label>
                            <select class="form-control" name="type" id="selectType" required oninvalid="this.setCustomValidity('Select one')" oninput="this.setCustomValidity('')">
                                <option value="">Nothing Selected</option>
                                <option value="S">SME</option>
                                <option value="C">CONSUMER</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Package <span style="color: red;">*</span></label>
                            <input type="text" class="form-control editPackage" name="package" placeholder="Package Name" required oninvalid="this.setCustomValidity('Please input package name')" oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Update</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>