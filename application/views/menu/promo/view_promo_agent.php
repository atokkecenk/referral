<?php
error_reporting(E_ALL);
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">View Page Promo</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Data Promo</h5>
                                        <a href="<?= base_url('add-promo') ?>" class="btn btn-primary"><i class="feather icon-plus"></i>Add New</a>
                                    </div>
                                    <div class="card-body">
                                        <button type="button" class="hidden" id="openModal" data-toggle="modal" data-target="#modalZoom">Cek</button>
                                        <div class="table-responsive">
                                            <table class="table table-hover tb-vpromo" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" width="50">NO.</th>
                                                        <th width="50">FILE</th>
                                                        <th width="200">DESCRIPTION</th>
                                                        <th width="50">WA NUMBER</th>
                                                        <th width="200">STATUS</th>
                                                        <th width="200">REASON</th>
                                                        <th width="200">SUBMIT DATE</th>
                                                        <th width="200">APPROVE DATE</th>
                                                        <th class="text-center"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no = 1;
                                                    foreach ($data as $val) {
                                                        if ($val->approval == '') {
                                                            $sts =  '<span class="kuning-appv"><i class="feather icon-alert-circle"></i> Pending</span>';
                                                            $clr = 'style="background-color: #ffeeee;"';
                                                        } elseif ($val->approval == 'N') {
                                                            $sts = '<span class="merah-appv"><i class="feather icon-x"></i> Not Approve</span>';
                                                            $clr = '';
                                                        } else {
                                                            $sts = '<span class="hijau-appv"><i class="feather icon-check"></i> Approve</span>';
                                                            $clr = '';
                                                        }
                                                    ?>
                                                        <tr <?= $clr ?>>
                                                            <td align="center"><?= $no ?>.</td>
                                                            <td><img src="<?= base_url('assets/landing-promo/' . $val->file) ?>" style="cursor: pointer;" onclick="showmodal(this);" class="img-promo" title="Click to view detail"></td>
                                                            <td><?= $val->desc ?></td>
                                                            <td><?= $val->wa_number ?></td>
                                                            <td><?= $sts ?></td>
                                                            <td><?= $val->reason ?></td>
                                                            <td><?= $val->input ?></td>
                                                            <td><?= $val->approve ?></td>
                                                            <td align="center">
                                                                <button class="btn btn-danger delPromo" data-id="<?= $val->id ?>" data-img="<?= $val->file ?>"><i class="feather icon-trash r-no-margin"></i></button>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                        $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete', 'method="post"') ?>
                <input type="hidden" name="mode" class="mode">
                <input type="hidden" name="key" class="key">
                Are you sure delete this data ?
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Zoom -->
<div class="modal fade" id="modalZoom" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog text-center" role="document">
        <!-- <div class="modal-content"> -->
        <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
        <!-- <div class="modal-body text-center"> -->
        <div id="show-imgpromo"></div>
        <!-- </div> -->
        <!-- </div> -->
    </div>
</div>

<!-- Modal Approve -->
<div class="modal fade" id="modalApprove" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Approve Promo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <?= form_open('save-approve', 'method="post" id="frmAppv"') ?>
                    <div class="row">
                        <input type="hidden" name="id" id="idAppv">
                        <div class="col-sm-12 to-center">
                            <span class="showImgPromo"></span>
                        </div>
                        <div class="col-sm-12 mt-10">
                            <p>
                                <i class="feather icon-thumbs-up text-success"></i> : If approve, this design <span class="text-success">show</span> in page promo.<br>
                                <i class="feather icon-thumbs-down text-danger"></i> : If not approve, this design <span class="text-danger">not show</span> in page promo.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success btn-block"><i class="feather icon-thumbs-up"></i>Approve</button>
                        </div>
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-danger btn-block not-appv"><i class="feather icon-thumbs-down"></i>Not Approve</button>
                        </div>
                        <div class="col-sm-12 mt-10 validasiAppv">
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal View File -->
<div class="modal fade" id="modalViewFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">File Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> -->
            <div class="modal-body detailFilePromo">
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalEditPackage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Package</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <?= form_open('update-package', 'method="post"') ?>
                    <input type="hidden" name="id" id="typeId">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Type <span style="color: red;">*</span></label>
                            <select class="form-control" name="type" id="selectType" required oninvalid="this.setCustomValidity('Select one')" oninput="this.setCustomValidity('')">
                                <option value="">Nothing Selected</option>
                                <option value="S">SME</option>
                                <option value="C">CONSUMER</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Package <span style="color: red;">*</span></label>
                            <input type="text" class="form-control editPackage" name="package" placeholder="Package Name" required oninvalid="this.setCustomValidity('Please input package name')" oninput="this.setCustomValidity('')">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Update</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showmodal(x) {
        // console.log(x.getAttribute('src'));
        document.getElementById("openModal").click();
        document.getElementById('show-imgpromo').innerHTML = '<img src="' + x.getAttribute('src') + '" height="550px">';
    }
</script>