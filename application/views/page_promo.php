<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/images/setting/72x72.png') ?>">
  <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/<?= $setting->favicon ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Promo Page | My Unifi
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <meta name="description" content="My Unifi Application"/>
  <meta name="keywords" content="unifi, website, web, application">
  <meta name="author" content="Firdaus Zulkarnain">

  <!--     Fonts and icons     -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" /> -->
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link href="<?= base_url() ?>assets/landing-page/fa-icon/css/all.css" rel="stylesheet" />
  <!-- CSS Files -->

  <link href="<?= base_url() ?>assets/landing-page/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />

  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?= base_url() ?>assets/landing-page/demo/demo.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/bootstrap-4/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" rel="stylesheet">

  <!-- Splide JS -->
  <link href="<?= base_url() ?>assets/plugin/splide-3.1.3/dist/css/splide.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/plugin/splide-3.1.3/dist/css/splide-core.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/plugin/splide-3.1.3/dist/css/themes/splide-default.min.css" rel="stylesheet" />

</head>
<style>
  .btn-login {
    font-size: 17px;
    font-weight: bold;
    background-color: #4e73df;
    padding: 1.8em;
    border-radius: 16px;
    margin: 0 12px 0 12px;
  }

  .btn-login:hover,
  .btn-login:active {
    background-color: #3a62d5;
  }

  .btn-search {
    font-size: 16px;
    background-color: #4e73df;
    border-radius: 25px;
    padding: 13px 19px 13px 19px;
  }

  .btn-search:hover,
  .btn-search:active {
    background-color: #3a62d5;
  }

  .form-control-user {
    font-size: 15px;
  }

  .img-logo {
    width: 350px;
    margin-bottom: 15px;
  }

  .brand {
    margin-top: -150px;
  }

  .control-new {
    height: 50px !important;
    font-size: 14px !important;
    background-color: #fff;
  }

  .p-tb-2 {
    padding-top: 4px;
    padding-bottom: 4px;
  }

  .f-15 {
    font-size: 15px !important;
  }

  .fa-whatsapp {
    color: #25D366;
  }

  .f-black {
    color: #000;
  }

  .border {
    border: 1px solid #000 !important;
  }

  .card-body .text-promo {
    font-size: 14px;
    font-weight: bold;
    text-align: center;
    margin-top: 5px;
  }

  .card-body .img {
    text-align: center;
  }

  .card-body .img img {
    min-width: 210px;
    max-width: 210px;
  }

  .garis {
    margin: 0 0.3px 0 0.3px;
    border-top: 1px solid red;
  }

  .page-header {
    min-height: 86vh !important;
    max-height: none;
  }

  .page-header,
  .page-header:before {
    background-color: white !important;
  }

  .card-promo {
    min-height: 250px !important;
    max-height: 250px !important;
    /* background-color: #25D366 !important; */
  }

  .img-promo {
    /* min-width: 130px !important; */
    /* max-width: 130px !important; */
    height: 140px !important;
    width: auto !important;
  }


  /* slider */
  .mySlides {
    display: none
  }

  img {
    vertical-align: middle;
  }

  /* Slideshow container */
  .slideshow-container {
    max-width: 1000px;
    position: relative;
    margin: auto;
  }

  /* Next & previous buttons */
  .prev1,
  .next1 {
    cursor: pointer;
    position: absolute;
    top: 50%;
    width: auto;
    padding: 16px;
    margin-top: -22px;
    color: white;
    font-weight: bold;
    font-size: 18px;
    transition: 0.6s ease;
    border-radius: 0 3px 3px 0;
    user-select: none;
  }

  /* Position the "next button" to the right */
  .next1 {
    right: 0;
    border-radius: 3px 0 0 3px;
  }

  /* On hover, add a black background color with a little bit see-through */
  .prev1:hover,
  .next1:hover {
    background-color: rgba(0, 0, 0, 0.8);
  }

  /* Caption text */
  .text {
    color: #f2f2f2;
    font-size: 15px;
    padding: 8px 12px;
    position: absolute;
    bottom: 8px;
    width: 100%;
    text-align: center;
  }

  /* Number text (1/3 etc) */
  .numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
  }

  /* The dots/bullets/indicators */
  .dot1 {
    cursor: pointer;
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
  }

  .active,
  .dot1:hover {
    background-color: #717171;
  }

  /* Fading animation */
  .fade {
    -webkit-animation-name: fade;
    -webkit-animation-duration: 1.5s;
    animation-name: fade;
    animation-duration: 1.5s;
  }

  @-webkit-keyframes fade {
    from {
      opacity: .4
    }

    to {
      opacity: 1
    }
  }

  @keyframes fade {
    from {
      opacity: .4
    }

    to {
      opacity: 1
    }
  }

  /* On smaller screens, decrease text size */
  @media only screen and (max-width: 300px) {

    .prev1,
    .next1,
    .text {
      font-size: 11px
    }
  }
  /* slider */

  /* Horizontal Scroll */
  div.scrollmenu {
    background-color: #fff;
    /* overflow: auto; */
    overflow-x: auto;
    overflow-y: hidden;
    white-space: nowrap;
    height: 285px;
  }

  div.scrollmenu a {
    display: inline-block;
    width: 270px;
    height: 263px;
    color: white;
    text-align: center;
    /* padding: 7px; */
    /* margin: 5px; */
    border-radius: 5px;
    text-decoration: none;
  }

  .scrollmenu>a {
    margin-right: 7px;
    margin-bottom: 5px;
  }

  .card-body>.image {
    border-radius: 5px;
  }

  .card-body>.nama-toko {
    margin-top: 10px;
    overflow: hidden;
    color: #1e1e1e;
    font-size: 12px;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  .scrollmenu>a>.card:hover {
    background-color: #f0f0f0;
  }

  .image>img {
    width: 100%;
    height: 100%;
    overflow: hidden;
    object-fit: cover;
    /* cursor: pointer; */
  }

  /* Horizontal Scroll */

  .border {
    border: 1px solid #000;
  }
</style>

<body class="offline-doc">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-absolute" style="background-color: #4e73df;">
    <div class="container">
      <div class="navbar-wrapper">
        <div class="navbar-toggle">
          <button type="button" class="navbar-toggler">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <!-- <a class="navbar-brand" href="<?= base_url('signin-staff') ?>" title="Click to sign in Staff"><i class="fas fa-sign-in-alt" style="color: #fff;"></i> Staff Login</a> -->
        <a class="navbar-brand" href="<?= base_url('welcome') ?>"><?= $setting->lp_text ?></a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?= base_url('login') ?>" target="_blank">
              <i class="fas fa-sign-in-alt mr-2"></i>Portal Login
            </a>
          </li> -->
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->

  <div class="page-header" style="color: #000 !important;">
    <!-- <div class="page-header-image"></div> -->
    <div class="container">
      <!-- Search -->
      <?= form_open('promo/search', 'method="post"') ?>
      <div style="margin-top: 70px;">
        <!-- <div style="margin-top: 180px;"> -->
        <div class="col-lg-12">
          <div class="row">
            <div class="col-9 py-2">
              <input type="text" class="form-control" name="search" placeholder="Search product.." required>
            </div>
            <div class="col-3">
              <?php
              $src = $this->uri->segment(2);
              if (isset($src)) {
                echo '
                    <button type="submit" name="cari" class="btn btn-dark"><i class="fas fa-search"></i> Search</button>
                    <a href="' . base_url('promo') . '" class="btn btn-danger"><i class="fas fa-undo"></i> Reset Filter</a>
                ';
              } else {
                echo '
                    <button type="submit" name="cari" class="btn btn-dark btn-block"><i class="fas fa-search"></i> Search</button>
                ';
              }

              ?>
            </div>
          </div>
        </div>
      </div>
      <?= form_close() ?>

      <div class="col-lg-12">
        <div class="row">
          <div class="col-lg-12">
            <div class="splide" data-splide='{"autoHeight": "TRUE", "type": "loop","autoplay": "TRUE"}'>
              <div class="splide__track">
                <ul class="splide__list">
                  <?php
                  foreach ($slider as $val) {
                  ?>
                    <li class="splide__slide">
                      <img src="<?= base_url('assets/landing-promo/slider/' . $val->file) ?>" alt="img-promo">
                    </li>
                  <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-12 mt-4">
        <?php 
        echo '
          <div class="mb-2">
            <i class="fas fa-bars mr-1" style="color: #f44;"></i> All result <b>'.$jumlah.' record</b>
          </div>
        ';
        ?>
        <div class="row">
          <?php
          foreach ($data as $dt) {
          ?>
            <div class="col-md-3">
              <div class="card imgPromo" style="cursor: pointer;" data-file="<?= $dt->file ?>" data-nama="<?= $dt->nama ?>" data-desc="<?= $dt->desc ?>" data-wa="<?= $dt->wa_number ?>">
                <div class="card-body">
                  <div class="image">
                    <img src="<?= base_url('assets/landing-promo/' . $dt->file) ?>">
                  </div>
                  <div class="nama-toko" title="<?= $dt->nama ?>">
                    <?= $dt->nama ?>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>

    </div>
  </div>

  <footer class="footer" style="padding: 7px 0; background-color: #4e73df;">
    <div class="container-fluid">
      <!-- <nav>
        <ul>
          <li>
            <a href="https://www.creative-tim.com">
              Creative Tim
            </a>
          </li>
          <li>
            <a href="http://presentation.creative-tim.com">
              About Us
            </a>
          </li>
          <li>
            <a href="http://blog.creative-tim.com">
              Blog
            </a>
          </li>
        </ul>
      </nav> -->
      <div class="copyright" id="copyright">
        <a href="https://web.whatsapp.com/send?phone=<?= $setting->no_hp ?>&text=Hello...." target="_blank" style="font-size: 22px; text-decoration: none; color: #fff;" title="Contact Us"><i class="fab fa-whatsapp mr-2"></i>Whatsapp</a>
      </div>
    </div>
  </footer>

  <!-- Modal -->
  <div class="modal fade" id="modalDetailPromo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mdLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-7">
              <div class="mdImgPromo"></div>
            </div>
            <div class="col-5">
              <table class="table">
                <tbody>
                  <tr>
                    <td width="125" style="vertical-align: top;"><b>Description</b></td>
                    <td class="mdDescPromo" style="vertical-align: top; text-align: justify;"></td>
                  </tr>
                  <tr>
                    <td style="vertical-align: top;"><b>Whatsapp</b></td>
                    <td class="mdWaPromo" style="vertical-align: top;"></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="modal-footer" style="justify-content: flex-start;">
          <button type="button" class="btn btn-secondary" data-dismiss="modal"> Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="<?= base_url() ?>assets/landing-page/js/core/jquery.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/popper.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/bootstrap.min.js"></script>
  <!-- <script src="<?= base_url() ?>assets/landing-page/js/plugins/perfect-scrollbar.jquery.min.js"></script> -->
  <script src="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.js"></script>

  <!-- Chart JS -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?= base_url() ?>assets/landing-page/demo/demo.js"></script>

  <!-- Splide JS -->
  <script src="<?= base_url() ?>assets/plugin/splide-3.1.3/dist/js/splide.js"></script>
  <script src="<?= base_url() ?>assets/plugin/splide-3.1.3/dist/js/splide-renderer.min.js"></script>

  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var splide = new Splide('.splide');

      splide.mount();
    });
  </script>
  <script>
    const flashdata = $(".flash-data").data("flashdata");
    const flashtipe = $(".flash-data").data("flashtipe");

    if (flashdata && flashtipe == "success") {
      sweetAlert({
        title: "Success",
        text: flashdata,
        type: "success",
        showConfirmButton: false,
        timer: 2600,
      });
    } else if (flashdata && flashtipe == "error") {
      sweetAlert({
        title: "Error",
        text: flashdata,
        type: "error",
        showConfirmButton: false,
        timer: 2600,
      });
    }

    $(document).ready(function() {
      $(document).on('click', '.imgPromo', function() {
        var file = $(this).data('file');
        var nama = $(this).data('nama');
        var desc = $(this).data('desc');
        var wa = $(this).data('wa');
        $('#modalDetailPromo').modal('show');
        $('.mdImgPromo').html('<img src="<?= base_url('assets/landing-promo/') ?>' + file + '">');
        $('#mdLabel').html('NAME : <b>' + nama + '</b>');
        $('.mdDescPromo').html(desc);
        if (wa !== '') {
          $('.mdWaPromo').html(wa + '<br><a href="https://web.whatsapp.com/send?phone=' + wa + '&text=Hello '+nama+'...." target="_blank" style="text-decoration: none;" title="Chat to whatsapp number"><img src="<?= base_url('assets/images/wa.png') ?>" width="25"> Chat me..</a>');
        } else {
          $('.mdWaPromo').html('-');
        }
      });
    });

    (function($) {
      "use strict";
      // Auto-scroll
      // $('#myCarousel').carousel({
      //   interval: 4000
      // });

      // Control buttons
      $('.next').click(function() {
        $('.carousel').carousel('next');
        return false;
      });
      $('.prev').click(function() {
        $('.carousel').carousel('prev');
        return false;
      });

      // On carousel scroll
      $("#myCarousel").on("slide.bs.carousel", function(e) {
        var $e = $(e.relatedTarget);
        var idx = $e.index();
        var itemsPerSlide = 3;
        var totalItems = $(".carousel-item").length;
        if (idx >= totalItems - (itemsPerSlide - 1)) {
          var it = itemsPerSlide -
            (totalItems - idx);
          for (var i = 0; i < it; i++) {
            // append slides to end 
            if (e.direction == "left") {
              $(
                ".carousel-item").eq(i).appendTo(".carousel-inner");
            } else {
              $(".carousel-item").eq(0).appendTo(".carousel-inner");
            }
          }
        }
      });
    })
    (jQuery);
  </script>
</body>

</html>