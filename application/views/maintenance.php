<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SYSTEM UNDER MAINTENANCE</title>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/48x48.png">
</head>
<style>
    body {
        font-family: 'Courier New', Courier, monospace !important;
        background-color: #efefef;
    }

    .container {
        width: 700px;
        /* border: 1px solid red; */
        border-radius: 10px;
        padding: 3em 3em .6em 3em;
        text-align: center;
        font-size: 28px;
        color: red;
        margin: 0 auto;
        margin-top: 55px;
    }

    .img-maintenance {
        width: 250px;
        margin-bottom: 50px;
    }

    .logo {
        margin-top: 50px;
        width: 100px;
    }

    .footer {
        font-size: 16px;
        color: #000;
    }

    .text {
        padding: .7em 0 .7em 0;
        border-bottom: 4px solid red;
    }

    .border {
        border: 1px solid #000;
    }
</style>

<body>
    <div class="container border">
        <img class="img-maintenance" src="<?= base_url('assets/images/setting/maintenance.png') ?>"><br>
        <div class="text">
            <b>SORRY,</b>
            <font style="color: #000;">THIS WEBSITE IS MAINTENANCE !!</font>
        </div>
        <center>
            <?php
            $web = $this->db->query("SELECT website FROM tb_setting WHERE id = 1")->row()->website;
            echo '<p style="font-size: 15px; color: #000; margin-top: 6.6em;"><a href="' . base_url('on') . '" style="text-decoration: none;">' . $web . '</a> &copy; ' . date('Y') . '</p>';
            ?>
        </center>
    </div>
</body>

</html>