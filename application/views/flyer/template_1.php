<?php
$pdf = new Pdf_template_1('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('Template 1');
$pdf->SetHeaderMargin(0);
$pdf->SetTopMargin(0);
$pdf->setFooterMargin(0);
$pdf->SetAutoPageBreak(false);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

$pdf->AddPage();
// $pdf->setJPEGQuality(75);
// Print a text
$html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
$pdf->SetXY(15, 260);
$pdf->SetFont('helvetica', 'B', 24);
$pdf->SetTextColor(255, 255, 255);
$pdf->Cell(0, 0, 'FIRDAUS ZULKARNAIN', 0, 1, 'L', 0, '', 0);
$pdf->Image('assets/images/setting/qr.png', 144.5, 227, 54, 54, 'PNG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 0, false, false, false);
// $pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Template-Right.pdf', 'I');
exit();
