<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Design Flyer</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- Setting Text -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Flyer A4</h5>
                                        <!-- <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalUpload"><i class="feather icon-plus"></i>Add New Template</a> -->
                                    </div>
                                    <div class="card-body">
                                        <div class="testtt"></div>
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="tbl-view-flyer">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">NO.</th>
                                                        <th>DESIGN</th>
                                                        <th>DETAIL</th>
                                                        <!-- <th>NOTE</th> -->
                                                        <!-- <th>LAYOUT</th> -->
                                                        <!-- <?php
                                                                if (in_array($level, ['staff', 'user'])) {
                                                                    echo '<th>STATUS</th>';
                                                                }
                                                                ?> -->
                                                        <th class="text-center"><i class="feather icon-settings"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $no = 1;
                                                    foreach ($flyer as $fl) {
                                                        $sts = ($fl->active == 'Y') ? '<span class="badge badge-success f-13">Showing on agent</span>' : '<span class="badge badge-danger f-13">Not showing</span>';
                                                        $ubah_sts = ($fl->active == 'Y') ? 'Non-active' : 'Active';
                                                        $ket = ($fl->keterangan !== null && $fl->keterangan !== '') ? $fl->keterangan : '-';
                                                        $rowspan = (in_array($level, ['staff', 'user'])) ? 4 : 3;
                                                    ?>
                                                        <tr>
                                                            <td rowspan="<?= $rowspan ?>" align="center"><?= $no . '.' ?></td>
                                                            <td rowspan="<?= $rowspan ?>" width="200"><img src="<?= base_url('assets/images/setting/' . $fl->file) ?>" width="160"></td>
                                                            <td>
                                                                <label class="bold mr-3"><i class="feather icon-chevron-right"></i> Template Name : </label><?= $fl->name ?>
                                                            </td>
                                                            <td rowspan="<?= $rowspan ?>" align="center" width="280">
                                                                <a href="<?= base_url('generate-pdf/p-' . $no . '/I/' . $hash) ?>" class="btn btn-warning" target="_blank" title="Preview Template"><i class="feather icon-eye r-no-margin"></i></a>
                                                                <a href="<?= base_url('generate-pdf/p-' . $no . '/D/' . $hash) ?>" class="btn btn-dark" title="Download Template"><i class="feather icon-download r-no-margin"></i></a>
                                                                <?php
                                                                if (in_array($level, ['staff', 'user'])) {
                                                                ?>
                                                                    <button type="button" class="btn btn-info modalChangeTmp" data-toggle="modal" data-target="#modalUpload" title="Change design" data-id="<?= $fl->id ?>" data-name="<?= $fl->name ?>" data-file="<?= $fl->file ?>" data-note="<?= $fl->keterangan ?>"><i class="feather icon-edit r-no-margin"></i></button>
                                                                    <button type="button" class="btn btn-danger modalDelete" data-mode="flyer" data-key="<?= $fl->id ?>" data-sts="<?= $ubah_sts ?>" title="Set Status Template"><i class="feather icon-x r-no-margin"></i></button>
                                                                <?php
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label class="bold mr-3"><i class="feather icon-chevron-right"></i> Note : </label><?= $ket ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <label class="bold mr-3"><i class="feather icon-chevron-right"></i> Layout : </label><?= ($fl->layout == 'P') ? 'Potrait' : 'Landscape' ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if (in_array($level, ['staff', 'user'])) {
                                                        ?>
                                                            <tr>
                                                                <td>
                                                                    <label class="bold mr-3"><i class="feather icon-chevron-right"></i> Status : </label><?= $sts ?>
                                                                </td>
                                                            </tr>
                                                    <?php
                                                        }
                                                        $no++;
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Hapus -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
            </div>
            <div class="modal-body text-center">
                <?= form_open('delete', 'method="post"') ?>
                <input type="hidden" name="mode" class="mode">
                <input type="hidden" name="key" class="key">
                <input type="hidden" name="status" class="status">
                Are you sure <span class="sts-template"></span> this template ?
                <br>
                <span class="sts-template-2"></span>
                <br>
                <!-- </div>
          <div class="modal-footer"> -->
                <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>

<!-- Modal Change Foto -->
<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Design</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <!-- <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center"><i class="feather icon-copy blue mr-2"></i>UPLOAD TEMPLATE</th>
                            </tr>
                        </thead>
                    </table> -->
                    <?= form_open_multipart('save-template', 'method="post"') ?>
                    <input type="hidden" name="id_tmp">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Template Name</label>
                            <input type="text" class="form-control" name="tmp_name" placeholder="Template Name" required>
                        </div>
                        <div class="form-group">
                            <label>Note</label>
                            <input type="text" class="form-control" name="note" placeholder="Note">
                        </div>
                        <div class="form-group">
                            <label>Upload File</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input uploadFileTemplate" name="file" accept="image/jpeg, image/jpg, image/png">
                                <label class="custom-file-label">Choose file..</label>
                            </div>
                            <small style="color: red; font-size: 11px;">Max size file 25 megabyte. File <strong>jpeg, jpg, png</strong></small>
                            <img src="" id="imgTmp" style="margin-top: 4px;">
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-12 mb-1">
                        <div class="row">
                            <!-- <div class="col-sm-6">
                                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="feather icon-x"></i>Close</button>
                            </div> -->
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Upload -->
<div class="modal fade" id="modalUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Template</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <!-- <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center"><i class="feather icon-copy blue mr-2"></i>UPLOAD TEMPLATE</th>
                            </tr>
                        </thead>
                    </table> -->
                    <?= form_open_multipart('save-template', 'method="post"') ?>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Template Name</label>
                            <input type="text" class="form-control" name="tmp_name" placeholder="Template Name" required>
                        </div>
                        <div class="form-group">
                            <!-- <select name="layout" class="form-control" required>
                                <option value="">Nothing Selected</option>
                                <option value="P">Potrait</option>
                                <option value="L">Landscape</option>
                            </select> -->
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="potrait" name="layout" value="P" class="custom-control-input">
                                <label class="custom-control-label" for="potrait">Potrait</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="landscape" name="layout" value="L" class="custom-control-input">
                                <label class="custom-control-label" for="landscape">Landscape</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Upload File</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input uploadFileTemplate" name="receipt" required>
                                <label class="custom-file-label">Choose file..</label>
                            </div>
                            <small style="color: red; font-size: 11px;">Max size file 20 megabyte. File <strong>jpeg, jpg, png, pdf</strong></small>
                            <div class="mt-1" id="uploadFileTemplateView"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-sm-12 mb-1">
                        <div class="row">
                            <!-- <div class="col-sm-6">
                                <button type="button" class="btn btn-danger btn-block" data-dismiss="modal"><i class="feather icon-x"></i>Close</button>
                            </div> -->
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-block"><i class="feather icon-save"></i>Save & Update</button>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>