    <!-- Required Js -->
    <script src="<?= base_url() ?>assets/template/js/vendor-all.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/template/js/pcoded.min.js"></script>
    <!-- <script src="<?= base_url() ?>assets/jquery/jquery-3.5.1.js"></script> -->
    <script src="<?= base_url() ?>assets/template/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>

    <script src="<?= base_url() ?>assets/template/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?= base_url() ?>assets/myscript/myscript.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/select2/select2.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/ckeditor/ckeditor.js"></script>

    <script>
        // document.addEventListener("keydown", function(e){
        //     if (e.ctrlKey || e.keyCode == 123) {
        //         e.stopPropagation();
        //         e.preventDefault();
        //     }
        // });

        $(document).ready(function() {

            // CKEDITOR.replace('textAnnoucement', {
            //     height: 300
            // });

            $(document).on("click", ".btn-edit-code", function () {
                var id = $(this).data("id");
                var code = $(this).data("code");
                $("#idCodeEdit").val(id);
                $("#stringCodeEdit").val(code);
            });

            // $('#tbl-view-package').DataTable();
            $("#tbl-view-code, #tbl-dashboard-kedai, #tbl-dashboard-individu, #tbl-view-package").DataTable();

            $(document).on("change", ".showPassword", function () {
              var pass1 = document.getElementById("password1");
              var pass2 = document.getElementById("password2");

              if (pass1.type === "password" || pass2.type === "password") {
                pass1.type = "text";
                pass2.type = "text";

                $('.textShow').text("Hide Password");
                $("#passIcon, #passIcon2").html('<i class="feather icon-eye"></i>');
              } else {
                pass1.type = "password";
                pass2.type = "password";

                $('.textShow').text("Show Password");
                $("#passIcon, #passIcon2").html('<i class="feather icon-eye-off"></i>');
              }
            });

            $('.textShow').text("Show Password");
            $("#passIcon, #passIcon2").html('<i class="feather icon-eye-off"></i>');

            $(document).on("click", ".detailFoto", function () {
              var img = $(this).data("img");
              var ket = $(this).data("ket");
              if (ket == "kedai") {
                $(".detailFotoKedai").html(
                  '<img src="assets/images/kedai/' +
                    img +
                    '" class="img-responsive" style="width: 500px; height: auto;"/>'
                );
              } else if (ket) {
                $(".detailFotoIndividu").html(
                  '<img src="assets/images/sales/' +
                    img +
                    '" class="img-responsive" style="width: 500px; height: auto;"/>'
                );
              }
            });

            $(".credentialMonthly").change(function () {
              var params = $(this).val();
              $.ajax({
                url: "Report/get_data_user",

                method: "POST",

                data: {
                  params: params,
                },

                async: true,

                dataType: "json",

                success: function (data) {
                  var html = "";

                  var i;

                  html += '<select class="form-control" name="user">';

                  html += '<option value = "" > Nothing Selected </option>';

                  for (i = 0; i < data.length; i++) {
                    html +=
                      "<option value=" +
                      data[i].id_kedai +
                      ">" +
                      data[i].nsp.toUpperCase() +
                      "</option>";
                  }

                  html += "</select>";

                  $(".nspMonthly").html(html);
                },
              });

              return false;
            });

            $(".nspMonthly").html(
              '<select class="form-control"><option value = "" > Nothing Selected </option></select>'
            );

        });
    </script>

    </body>

    </html>