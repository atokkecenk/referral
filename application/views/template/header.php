<?php
// Firdaus Zulkarnain ID
$id_sess = $this->session->id;
$sett = $this->db->query("SELECT * FROM tb_setting WHERE id = 1")->row();
if ($level == 'staff') {
    $jml_keluhan = $this->db->get_where('tb_keluhan', ['status' => 'N'])->num_rows();
} else {
    $jml_keluhan = $this->db->get_where('tb_keluhan', ['status' => 'N', 'user_input' => $this->session->hash])->num_rows();
}
$cek_annc = $this->db->query("SELECT
                                a.*,
                                DATEDIFF( CURRENT_DATE, a.date_input ) AS hitung 
                            FROM
                                `tb_history` a 
                            WHERE
                                a.note_1 LIKE 'Add Announcement%' 
                                AND a.id = '$id_sess' 
                                AND a.`status` = 'N' 
                            HAVING
                                hitung < 8
                                ");
$annc_not_read = count($cek_annc->result());
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $sett->title ?></title>
    <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 11]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?= $sett->meta_description ?>" />
    <meta name="keywords" content="<?= $sett->meta_keyword ?>" />
    <meta name="author" content="Firdaus Zulkarnain" />

    <!-- Favicon icon -->
    <link rel="icon" href="<?= base_url() ?>assets/images/setting/<?= $sett->favicon ?>" type="image/x-icon">
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/fonts/fontawesome/css/fontawesome-all.min.css">
    <!-- animation css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/animation/css/animate.min.css">
    <!-- vendor css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/css/style.css">
    <!-- datepicker -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/myscript/mystyle.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/template/plugins/select2/select2.min.css">
    <!-- Chart Js -->
    <script src="<?= base_url() ?>assets/template/plugins/chart-js/dist/Chart.js"></script>
    <style>
        .hover:hover {
            color: #04a9f5;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <!-- [ Pre-loader ] start -->
    <div class="loader-bg">
        <div class="loader-track">
            <div class="loader-fill"></div>
        </div>
    </div>
    <!-- [ Pre-loader ] End -->
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="<?= base_url('dashboard') ?>" class="b-brand">
                    <!-- <div class="b-bg"> -->
                    <!-- <i class="feather icon-trending-up"></i> -->
                    <div class="b-bg" style="background-image: url('<?= base_url() ?>assets/images/setting/<?= $sett->logo ?>') !important; background-repeat: no-repeat; background-size: 35px 35px">
                    </div>
                    <span class="b-title"><?= $sett->name ?></span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="javascript:"><span></span></a>
            </div>
            <div class="navbar-content scroll-div">
                <ul class="nav pcoded-inner-navbar">
                    <li class="nav-item pcoded-menu-caption">
                        <label>Main Menu</label>
                    </li>
                    <li <?php if ($uri1 == 'announcement') {
                            echo 'class="nav-item active"';
                        } else {
                            echo 'class="nav-item"';
                        }

                        if ($annc_not_read !== 0) {
                            $badge =  '<span class="badge badge-success ml-1">' . $annc_not_read . ' NEW</span>';
                        } else {
                            $badge = '';
                        }
                        ?>>
                        <!-- onclick="insAnnc();"  RUN WITH CLICK -->
                        <a href="<?= base_url('announcement') ?>" class="nav-link" onclick="insAnnc();"><span class="pcoded-micon"><i class="feather icon-bell"></i></span><span class="pcoded-mtext">Announcement <?= $badge ?></span></a>
                    </li>
                    <li <?php if ($uri1 == 'dashboard') {
                            echo 'class="nav-item active"';
                        } else {
                            echo 'class="nav-item"';
                        } ?>>
                        <a href="<?= base_url('dashboard') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                    </li>
                    <?php
                    if ($level == 'staff' || $level == 'user') {
                        $method = ['port-full', 'upcoming-projects', 'pending', 'processing', 'paid', 'cancel', 'rejected', 'completed', 'set-code', 'set-website', 'set-landing', 'set-package', 'view-user'];
                    ?>
                        <li <?php if (in_array($uri2, $method) || $uri1 == 'detail-order') {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } elseif ($uri1 == 'edit-order') {
                                echo 'class="nav-item pcoded-hasmenu active"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">Order</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri2 == 'pending') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/pending') ?>" class="">Pending</a></li>
                                <li <?php if ($uri2 == 'port-full') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/port-full') ?>" class="">Port Full</a></li>
                                <li <?php if ($uri2 == 'upcoming-projects') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/upcoming-projects') ?>" class="">Upcoming Projects</a></li>
                                <li <?php if ($uri2 == 'processing') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/processing') ?>" class="">Processing</a></li>
                                <li <?php if ($uri2 == 'paid') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/paid') ?>" class="">Paid</a></li>
                                <li <?php if ($uri2 == 'cancel') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/cancel') ?>" class="">Cancel</a></li>
                                <li <?php if ($uri2 == 'rejected') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/rejected') ?>" class="">Rejected</a></li>
                                <li <?php if ($uri2 == 'completed') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order/completed') ?>" class="">Completed</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uri1 == 'add-sales' || $uri1 == 'view-sales' || $uri1 == 'save-sales' || $uri1 == 'edit-individu') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('view-sales') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-user"></i></span><span class="pcoded-mtext">Sales Agent</span></a>
                        </li>
                        <!--<li <?php if ($uri1 == 'add-staff' || $uri1 == 'view-staff' || $uri1 == 'save-staff' || $uri1 == 'edit-staff') {
                                    echo 'class="nav-item active"';
                                } else {
                                    echo 'class="nav-item"';
                                } ?>>
                            <a href="<?= base_url('view-staff') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-user"></i></span><span class="pcoded-mtext">Staff Referrer Incharge</span></a>
                        </li>-->
                        <li <?php if ($uri1 == 'add-kedai' || $uri1 == 'view-kedai' || $uri1 == 'save-kedai' || $uri1 == 'edit-kedai') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('view-kedai') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Kedai</span></a>
                        </li>
                        <li <?php if ($uri1 == 'add-keluhan' || $uri1 == 'keluhan') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('keluhan') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-success ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a>
                            <!-- <a href="#" onclick="alert('Menu On Progress')" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-danger ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a> -->
                        </li>
                        <li <?php if (in_array($uri1, ['add-promo', 'promo', 'view-promo'])) {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('view-promo') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span><span class="pcoded-mtext">Promo</span></a>
                        </li>
                        <?php
                        // Menu Setting Announcement by User
                        if ($level == 'user') { ?>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Setting Menu</label>
                            </li>
                            <li <?php if ($uri1 == 'add-announcement' || $uri1 == 'view-announcement') {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Setting</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri1 == 'add-announcement' || $uri1 == 'view-announcement') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-announcement') ?>" class="">Annoucement</a></li>
                                </ul>
                            </li>
                            <li <?php if ($uri1 == 'flyer') {
                                    echo 'class="nav-item active"';
                                } else {
                                    echo 'class="nav-item"';
                                } ?>>
                                <a href="<?= base_url('flyer') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Flyer</a>
                            </li>
                        <?php
                        } // Menu Setting Announcement by User

                        if ($level == 'staff') {
                        ?>
                            <li class="nav-item pcoded-menu-caption">
                                <label>Setting Menu</label>
                            </li>
                            <li <?php if (in_array($uri1, $method)) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Setting</span></a>
                                <ul class="pcoded-submenu">
                                    <!-- <li <?php if ($uri1 == 'add-announcement' || $uri1 == 'view-announcement') {
                                                    echo 'class="active"';
                                                } ?>><a href="<?= base_url('view-announcement') ?>" class="">Annoucement</a></li> -->
                                    <li <?php if ($uri1 == 'view-user') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-user') ?>" class="">User</a></li>
                                    <li <?php if ($uri1 == 'set-code') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('set-code') ?>" class="">Code</a></li>
                                    <li <?php if ($uri1 == 'set-package') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('set-package') ?>" class="">Package</a></li>
                                    <li <?php if ($uri1 == 'set-website') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('set-website') ?>" class="">Website</a></li>
                                    <li <?php if ($uri1 == 'set-landing') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('set-landing') ?>" class="">Landing Page</a></li>
                                    <li <?php if ($uri1 == 'set-smtp') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('set-smtp') ?>" class="">SMTP Email</a></li>
                                </ul>
                            </li>
                            <li <?php if (in_array($uri1, ['add-announcement', 'add-announcement-video', 'view-announcement'])) {
                                    echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                } else {
                                    echo 'class="nav-item pcoded-hasmenu"';
                                } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-bell"></i></span><span class="pcoded-mtext">Annoucement</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri1 == 'view-announcement') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('view-announcement') ?>" class="">List Annoucement</a></li>
                                    <li <?php if ($uri1 == 'add-announcement') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('add-announcement') ?>" class="">Image</a></li>
                                    <li <?php if ($uri1 == 'add-announcement-video') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('add-announcement-video') ?>" class="">Video</a></li>
                                </ul>
                            </li>
                            <li <?php if ($uri1 == 'flyer') {
                                    echo 'class="nav-item active"';
                                } else {
                                    echo 'class="nav-item"';
                                } ?>>
                                <a href="<?= base_url('flyer') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Flyer</a>
                            </li>
                            <!-- <li <?php if ($uri1 == 'flyer') {
                                            echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                                        } else {
                                            echo 'class="nav-item pcoded-hasmenu"';
                                        } ?>>
                                <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Flyer</span></a>
                                <ul class="pcoded-submenu">
                                    <li <?php if ($uri1 == 'flyer') {
                                            echo 'class="active"';
                                        } ?>><a href="<?= base_url('flyer') ?>" class="">Template</a></li>
                                </ul>
                            </li> -->
                        <?php } ?>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Report</label>
                        </li>
                        <li <?php if ($uri1 == 'monthly-statement') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('monthly-statement') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-navigation"></i></span><span class="pcoded-mtext">Monthly Statement</span></a>
                        </li>
                        <li <?php if ($uri1 == 'all-sale' || $uri1 == 'sale-by-kedai' || $uri1 == 'sale-by-individu') {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-printer"></i></span><span class="pcoded-mtext">Report Pdf</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri1 == 'all-sale') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('all-sale') ?>" class="">All Sale</a></li>
                                <li <?php if ($uri1 == 'sale-by-kedai') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('sale-by-kedai') ?>" class="">Sale by Kedai</a></li>
                                <li <?php if ($uri1 == 'sale-by-individu') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('sale-by-individu') ?>" class="">Sale by Individu</a></li>
                            </ul>
                        </li>
                    <?php
                    } elseif ($level == 'agent') {
                        $method = ['pending', 'port-full', 'upcoming-projects', 'processing', 'paid', 'cancel', 'rejected', 'completed'];
                    ?>
                        <li <?php if (in_array($uri2, $method)) {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">Order</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri2 == 'pending') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/pending') ?>" class="">Pending</a></li>
                                <li <?php if ($uri2 == 'port-full') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/port-full') ?>" class="">Port Full</a></li>
                                <li <?php if ($uri2 == 'upcoming-projects') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/upcoming-projects') ?>" class="">Upcoming Projects</a></li>
                                <li <?php if ($uri2 == 'processing') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/processing') ?>" class="">Processing</a></li>
                                <li <?php if ($uri2 == 'paid') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/paid') ?>" class="">Paid</a></li>
                                <li <?php if ($uri2 == 'cancel') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/cancel') ?>" class="">Cancel</a></li>
                                <li <?php if ($uri2 == 'rejected') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/rejected') ?>" class="">Rejected</a></li>
                                <li <?php if ($uri2 == 'completed') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-agent/completed') ?>" class="">Completed</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uri1 == 'add-keluhan' || $uri1 == 'keluhan') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('keluhan') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-success ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a>
                            <!-- <a href="#" onclick="alert('Menu On Progress')" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-danger ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a> -->
                        </li>
                        <li <?php if (in_array($uri1, ['view-promo-agent', 'add-promo'])) {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('view-promo-agent') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span><span class="pcoded-mtext">Promo</span></a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Setting Menu</label>
                        </li>
                        <li <?php if ($uri1 == 'notification') {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Setting</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri1 == 'notification') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('notification') ?>" class="">Notification</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uri1 == 'flyer') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('flyer') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Flyer</a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Report</label>
                        </li>
                        <li <?php if ($uri1 == 'sale-by-kedai') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('sale-by-kedai') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-printer"></i></span><span class="pcoded-mtext">Report Pdf</span></a>
                        </li>
                    <?php
                    } elseif ($level == 'individu') {
                        $method = ['pending', 'port-full', 'upcoming-projects', 'processing', 'paid', 'cancel', 'rejected', 'completed'];
                    ?>
                        <li <?php if (in_array($uri2, $method)) {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-package"></i></span><span class="pcoded-mtext">Order</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri2 == 'pending') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/pending') ?>" class="">Pending</a></li>
                                <li <?php if ($uri2 == 'port-full') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/port-full') ?>" class="">Port Full</a></li>
                                <li <?php if ($uri2 == 'upcoming-projects') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/upcoming-projects') ?>" class="">Upcoming Projects</a></li>
                                <li <?php if ($uri2 == 'processing') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/processing') ?>" class="">Processing</a></li>
                                <li <?php if ($uri2 == 'paid') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/paid') ?>" class="">Paid</a></li>
                                <li <?php if ($uri2 == 'cancel') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/cancel') ?>" class="">Cancel</a></li>
                                <li <?php if ($uri2 == 'rejected') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/rejected') ?>" class="">Rejected</a></li>
                                <li <?php if ($uri2 == 'completed') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('view-order-individu/completed') ?>" class="">Completed</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uri1 == 'add-keluhan' || $uri1 == 'keluhan') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('keluhan') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-success ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a>
                            <!-- <a href="#" onclick="alert('Menu On Progress')" class="nav-link"><span class="pcoded-micon"><i class="feather icon-message-square"></i></span><span class="pcoded-mtext">Keluhan <span class="badge badge-danger ml-1" style="font-size: 12px;"><?= $jml_keluhan ?></span></span></a> -->
                        </li>
                        <li <?php if (in_array($uri1, ['view-promo-agent', 'add-promo'])) {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('view-promo-agent') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span><span class="pcoded-mtext">Promo</span></a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Setting Menu</label>
                        </li>
                        <li <?php if ($uri1 == 'notification') {
                                echo 'class="nav-item pcoded-hasmenu active pcoded-trigger"';
                            } else {
                                echo 'class="nav-item pcoded-hasmenu"';
                            } ?>>
                            <a href="javascript:" class="nav-link "><span class="pcoded-micon"><i class="feather icon-settings"></i></span><span class="pcoded-mtext">Setting</span></a>
                            <ul class="pcoded-submenu">
                                <li <?php if ($uri1 == 'notification') {
                                        echo 'class="active"';
                                    } ?>><a href="<?= base_url('notification') ?>" class="">Notification</a></li>
                            </ul>
                        </li>
                        <li <?php if ($uri1 == 'flyer') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('flyer') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-file-text"></i></span><span class="pcoded-mtext">Flyer</a>
                        </li>
                        <li class="nav-item pcoded-menu-caption">
                            <label>Report</label>
                        </li>
                        <li <?php if ($uri1 == 'sale-by-individu') {
                                echo 'class="nav-item active"';
                            } else {
                                echo 'class="nav-item"';
                            } ?>>
                            <a href="<?= base_url('sale-by-individu') ?>" class="nav-link"><span class="pcoded-micon"><i class="feather icon-printer"></i></span><span class="pcoded-mtext">Report Pdf</span></a>
                        </li>
                    <?php
                    }
                    ?>
                    <li class="nav-item pcoded-menu-caption">
                        <label style="color: #c0ff00;">Version 1.6</label>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->

    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="javascript:"><span></span></a>
            <a href="<?= base_url('dashboard') ?>" class="b-brand">
                <!-- <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div> -->
                <div>
                    <img src="<?= base_url() ?>assets/images/setting/fav.png" alt="img-logo" height="30px" />
                </div>
                <span class="b-title"><?= $sett->name ?></span>
            </a>
        </div>
        <a class="mobile-menu" id="mobile-header" href="javascript:">
            <i class="feather icon-more-horizontal"></i>
        </a>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li><a href="javascript:" class="full-screen" onclick="javascript:toggleFullScreen()"><i class="feather icon-maximize"></i></a></li>
                <!-- <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown">Dropdown</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="javascript:">Action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Another action</a></li>
                        <li><a class="dropdown-item" href="javascript:">Something else here</a></li>
                    </ul>
                </li> -->
                <li class="nav-item">
                    <div class="main-search">
                        <div class="input-group">
                            <input type="text" id="m-search" class="form-control" placeholder="Search . . .">
                            <a href="javascript:" class="input-group-append search-close">
                                <i class="feather icon-x input-group-text"></i>
                            </a>
                            <span class="input-group-append search-btn btn btn-primary">
                                <i class="feather icon-search input-group-text"></i>
                            </span>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <span class="name-header">
                    <a href="<?= base_url('logout') ?>" class="btn btn-danger" style="color: #fff;" title="Logout">Logout</a>
                    Date Now : <?= date('d/m/Y') ?>
                </span>
                <li>
                    <div class="dropdown drp-user">
                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?= base_url() ?>assets/template/images/user/avatar-2.jpg" class="img-radius" alt="User-Profile-Image">
                                <?php
                                $hash = $this->session->hash;
                                $name = $this->db->query("SELECT `name` FROM tb_user WHERE `hash` = '$hash'")->row()->name;
                                echo '<span>' . $name . '</span>';
                                ?>
                                <a href="<?= base_url('auth/logout') ?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>

                            </div>
                            <ul class="pro-body">
                                <?php
                                if ($level == 'staff') {
                                    echo '<li><a href="#" class="dropdown-item" data-toggle="modal" data-target="#modalMaintenance"><i class="feather icon-power"></i> Off This Website</a></li>';
                                }
                                ?>
                                <li><a href="<?= base_url('auth/logout') ?>" class="dropdown-item"><i class="feather icon-lock"></i> Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <!-- [ Header ] end -->

    <!-- Modal Maintenance -->
    <div class="modal fade" id="modalMaintenance" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Confirmation <i class="far fa-question-circle" style="color: #ffa426;"></i></h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
                </div>
                <div class="modal-body text-center">
                    <?= form_open('off-system', 'method="post"') ?>
                    Are you sure <b>Set Maintenance</b> this website ?
                    <br>
                    <!-- </div>
          <div class="modal-footer"> -->
                    <button type="button" class="btn btn-danger my-2" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>