<!DOCTYPE html>
<html>

<head>
    <title>Month</title>
    <style>
        body {
            font-family: 'Helvetica', 'Arial', sans-serif;
            min-width: 992px !important;
        }

        @media (min-width: 992px) {
            .container {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .container {
                max-width: 1140px;
            }
        }

        .container {
            min-width: 992px !important;
            border: 3px solid #00b3ff;
            border-radius: 18px;
            width: 800px;
            margin: 0 auto;
            margin-bottom: 2em;
            padding: .8em;
        }

        .logo {
            width: 200px;
            margin-top: 2em;
            margin-bottom: 20px;
        }

        .f-25 {
            font-size: 25px;
        }

        .f-12 {
            font-size: 12px;
        }

        .bold {
            font-weight: bold;
        }

        .blue {
            color: #00b3ff;
        }

        .total-order {
            border: 3px solid #00b3ff;
        }

        .center {
            text-align: center;
        }
    </style>
</head>

<body>
    <div class="container">
        <center><img src="https://internet-unifi.com/referral/assets/images/setting/logo-mt.png" class="logo"></center>
        <h3>Hello <b>(%kedai%)</b></h3>
        <table style="width: 100%; margin-top: 2em;">
            <tbody>
                <tr>
                    <td>
                        <center><b style="font-size: 24px; color: #00b3ff;">Your (March) (2021) e-Statement</b></center>
                    </td>
                </tr>
            </tbody>
        </table>
        <br><br>
        <b>TRANSACTION SUMMARY</b>
        <br>
        <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px;">
            <thead>
                <tr style="padding: 15px;">
                    <th>
                        <center>TOTAL ORDER</center>
                    </th>
                    <th>
                        <center>TOTAL COMPLETE PROCESS</center>
                    </th>
                    <th>
                        <center>TOTAL ORDER PAID</center>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr style="padding: 15px;">
                    <td style="color: #00b3ff;">
                        <center>2 Transactions</center>
                    </td>
                    <td style="color: #00b3ff;">
                        <center>5</center>
                    </td>
                    <td style="color: #00b3ff;">
                        <center>9</center>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px; margin-top: 15px;">
            <tbody>
                <tr style="padding: 15px;">
                    <td>
                        <center>TOTAL COMMISSION =</center>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; margin-top: 4em;">
            <tbody>
                <tr style="padding: 15px;">
                    <td>
                        <center><a href="#" style="color: red;">Do you have an enquiry?</a></center>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; margin-top: .5em;">
            <tbody>
                <tr style="padding: 15px;">
                    <td colspan="2" style="color: red;">
                        <center>Have your family members subscribe to e-Statement & save the environment.</center>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; margin-top: 2em;">
            <tbody>
                <tr style="padding: 15px;">
                    <td>
                        <center><img src="https://internet-unifi.com/referral/assets/images/setting/telp.png"> <b>081</b></center>
                    </td>
                    <td>
                        <center><img src="https://internet-unifi.com/referral/assets/images/setting/web.png"> <b>.web.id</b></center>
                    </td>
                    <td>
                        <center><img src="https://internet-unifi.com/referral/assets/images/setting/email.png"> <b>unifi@gmail.com</b></center>
                    </td>
                    <td>
                        <center>
                            <img src="https://internet-unifi.com/referral/assets/images/setting/appstore.png" width="100px">
                            <img src="https://internet-unifi.com/referral/assets/images/setting/playstore.png" width="130px">
                        </center>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; font-size: 12px !important; margin-top: .2em;">
            <tbody>
                <tr>
                    <td>
                        <center>Code : KD00012</center>
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>To unsubscribe, <a href="#" style="color: red;">click here</a></center>
                    </td>
                </tr>
                <tr>
                    <td>
                        <center>Copyright © <?= date('Y') ?> Muafakat Technology.(KT0474208-U). All rights reserved.</center>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    </div>
</body>

</html>