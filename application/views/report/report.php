<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Create Report</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <?php
                            if ($this->session->level == 'staff') {
                            ?>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Monthly Statement</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('sale-monthly', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Nama Perniagaan/Nama Individu</label>
                                                        <select class="form-control" name="user" required>
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            foreach ($all_user as $usr) {
                                                            ?>
                                                                <option value="<?= $usr->code ?>"><?= '[ ' . strtolower($usr->level) . ' ] - ' . ucwords($usr->name_sales_person) ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Month</label>
                                                        <select class="form-control" name="month" required>
                                                            <option value="">Nothing Selected</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">Augustust</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control" name="year" required>
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = $year; $i >= 2018; $i--) {
                                                            ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label style="color: transparent;">Nama Perniagaan</label>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-danger"><i class="feather icon-navigation"></i>Send To Email</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>All Sale</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('sale-all', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Month</label>
                                                        <select class="form-control" name="month" required>
                                                            <option value="">Nothing Selected</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">Augustust</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control" name="year" required>
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = $year; $i >= 2018; $i--) {
                                                            ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label style="color: transparent;">Nama Perniagaan</label>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-warning"><i class="feather icon-navigation"></i>Export Pdf</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php
                            if ($this->session->level == 'agent' || $this->session->level == 'staff') {
                            ?>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Sale By Kedai</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('sale-by-kedai', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <?php
                                                        $kode = $this->session->id_kedai;
                                                        if ($this->session->level == 'agent') {
                                                        ?>
                                                            <label>Nama Perniagaan</label>
                                                            <select class="form-control" name="filter" disabled>
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                $kedai = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$kode'")->result();
                                                                foreach ($kedai as $kd) {
                                                                ?>
                                                                    <option value="<?= $kd->kode_kedai ?>" selected><?= ucwords($kd->nama_perniagaan) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <label>Nama Perniagaan</label>
                                                            <select class="form-control" name="filter" required>
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                $kedai = $this->db->query("SELECT * FROM tb_kedai WHERE active = 'Y'")->result();
                                                                foreach ($kedai as $kd) {
                                                                ?>
                                                                    <option value="<?= $kd->kode_kedai ?>"><?= ucwords($kd->nama_perniagaan) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Month</label>
                                                        <select class="form-control" name="month" required>
                                                            <option value="">Nothing Selected</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">Augustust</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control" name="year" required>
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = $year; $i >= 2018; $i--) {
                                                            ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label style="color: transparent;">Nama Perniagaan</label>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-warning"><i class="feather icon-navigation"></i>Export Pdf</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                            <?php
                                            if (isset($by_kedai)) {
                                                if ($by_kedai->num_rows() < 1) {
                                                    echo '<div class="alert alert-danger">
                                                    Oops, data Not Found !!
                                                    </div>';
                                                } else {
                                                    echo '<div class="table-responsive">
                                                        <table class="table table-hovered">
                                                        <thead>
                                                            <tr>
                                                                <th>Name of Applicant</th>
                                                                <th>Package Subscribe</th>
                                                                <th>Segment</th>
                                                                <th>Date</th>
                                                                <th>Commission Earn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>';
                                                    foreach ($by_kedai->result() as $kd) {
                                                        echo '<tr>
                                                        <td>' . $kd->name_customer . '</td>
                                                        <td>' . $kd->name_package . '</td>
                                                        <td>' . $kd->name_segment . '</td>
                                                        <td>' . $kd->new_input . '</td>
                                                        <td align="right">RM ' . $kd->commision . '</td>
                                                        </tr>';
                                                    }
                                                    echo '</tbody>
                                                    </table>
                                                    </div>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php
                            if ($this->session->level == 'individu' || $this->session->level == 'staff') {
                            ?>
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Sale By Individu</h5>
                                        </div>
                                        <div class="card-body">
                                            <?= form_open('sale-by-individu', 'method="post"') ?>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <?php
                                                        $kode = $this->session->id_kedai;
                                                        if ($this->session->level == 'individu') {
                                                        ?>
                                                            <label>Nama</label>
                                                            <select class="form-control" name="filter" disabled>
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                $individu = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$kode'")->result();
                                                                foreach ($individu as $idv) {
                                                                ?>
                                                                    <option value="<?= $idv->kode_individu ?>" selected><?= ucwords($idv->nama) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <label>Nama</label>
                                                            <select class="form-control" name="filter" required>
                                                                <option value="">Nothing Selected</option>
                                                                <?php
                                                                $individu = $this->db->query("SELECT * FROM tb_sales WHERE active = 'Y'")->result();
                                                                foreach ($individu as $idv) {
                                                                ?>
                                                                    <option value="<?= $idv->kode_individu ?>"><?= ucwords($idv->nama) ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Month</label>
                                                        <select class="form-control" name="month" required>
                                                            <option value="">Nothing Selected</option>
                                                            <option value="1">January</option>
                                                            <option value="2">February</option>
                                                            <option value="3">March</option>
                                                            <option value="4">April</option>
                                                            <option value="5">May</option>
                                                            <option value="6">June</option>
                                                            <option value="7">July</option>
                                                            <option value="8">Augustust</option>
                                                            <option value="9">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Year</label>
                                                        <select class="form-control" name="year" required>
                                                            <option value="">Select Year</option>
                                                            <?php
                                                            $year = date('Y');
                                                            for ($i = $year; $i >= 2018; $i--) {
                                                            ?>
                                                                <option value="<?= $i ?>"><?= $i ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label style="color: transparent;">Nama Perniagaan</label>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-warning"><i class="feather icon-navigation"></i>Export Pdf</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <?= form_close() ?>
                                            <?php
                                            if (isset($by_individu)) {
                                                if ($by_individu->num_rows() < 1) {
                                                    echo '<div class="alert alert-danger">
                                                    Oops, data Not Found !!
                                                    </div>';
                                                } else {
                                                    echo '<div class="table-responsive">
                                                        <table class="table table-hovered">
                                                        <thead>
                                                            <tr>
                                                                <th>Name of Applicant</th>
                                                                <th>Package Subscribe</th>
                                                                <th>Segment</th>
                                                                <th>Date</th>
                                                                <th>Commission Earn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>';
                                                    foreach ($by_individu->result() as $in) {
                                                        echo '<tr>
                                                        <td>' . $in->name_customer . '</td>
                                                        <td>' . $in->name_package . '</td>
                                                        <td>' . $in->name_segment . '</td>
                                                        <td>' . $in->new_input . '</td>
                                                        <td align="right">RM ' . $in->commision . '</td>
                                                        </tr>';
                                                    }
                                                    echo '</tbody>
                                                    </table>
                                                    </div>';
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>