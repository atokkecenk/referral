<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Create Report</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Sale By Individu</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('sale-by-individu', 'method="post"') ?>
                                        <div class="row">

                                            <?php
                                            $kode = $this->session->id_kedai;
                                            if ($this->session->level == 'individu') {
                                                $individu = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$kode'")->row();
                                            ?>
                                                <input type="hidden" name="filter" value="<?= $individu->kode_individu ?>">
                                            <?php
                                            } else {
                                            ?>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <select class="form-control" name="filter" required>
                                                            <option value="">Nothing Selected</option>
                                                            <?php
                                                            $individu = $this->db->query("SELECT * FROM tb_sales WHERE active = 'Y'")->result();
                                                            $filter = (isset($_POST['filter'])) ? $_POST['filter'] : '';
                                                            foreach ($individu as $idv) {
                                                            ?>
                                                                <option value="<?= $idv->kode_individu ?>" <?php if ($filter == $idv->kode_individu) {
                                                                                                                echo "selected";
                                                                                                            } else {
                                                                                                                echo "";
                                                                                                            } ?>><?= ucwords($idv->nama) ?></option>
                                                            <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Month</label>
                                                    <select class="form-control" name="month" required>
                                                        <?php
                                                        $month = (isset($_POST['month'])) ? $_POST['month'] : '';
                                                        ?>
                                                        <option value="">Select Month</option>
                                                        <option value="1" <?php if ($month == '1') {
                                                                                echo 'selected';
                                                                            } ?>>January</option>
                                                        <option value="2" <?php if ($month == '2') {
                                                                                echo 'selected';
                                                                            } ?>>February</option>
                                                        <option value="3" <?php if ($month == '3') {
                                                                                echo 'selected';
                                                                            } ?>>March</option>
                                                        <option value="4" <?php if ($month == '4') {
                                                                                echo 'selected';
                                                                            } ?>>April</option>
                                                        <option value="5" <?php if ($month == '5') {
                                                                                echo 'selected';
                                                                            } ?>>May</option>
                                                        <option value="6" <?php if ($month == '6') {
                                                                                echo 'selected';
                                                                            } ?>>June</option>
                                                        <option value="7" <?php if ($month == '7') {
                                                                                echo 'selected';
                                                                            } ?>>July</option>
                                                        <option value="8" <?php if ($month == '8') {
                                                                                echo 'selected';
                                                                            } ?>>August</option>
                                                        <option value="9" <?php if ($month == '9') {
                                                                                echo 'selected';
                                                                            } ?>>September</option>
                                                        <option value="10" <?php if ($month == '10') {
                                                                                echo 'selected';
                                                                            } ?>>October</option>
                                                        <option value="11" <?php if ($month == '11') {
                                                                                echo 'selected';
                                                                            } ?>>November</option>
                                                        <option value="12" <?php if ($month == '12') {
                                                                                echo 'selected';
                                                                            } ?>>December</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Year</label>
                                                    <select class="form-control" name="year" required>
                                                        <option value="">Select Year</option>
                                                        <?php
                                                        $year = date('Y');
                                                        $pyear = (isset($_POST['year'])) ? $_POST['year'] : '';
                                                        for ($i = $year; $i >= 2018; $i--) {
                                                        ?>
                                                            <option value="<?= $i ?>" <?php if ($pyear == $i) {
                                                                                            echo "selected";
                                                                                        } ?>><?= $i ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label style="color: transparent;">Nama Perniagaan</label>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary"><i class="feather icon-search"></i>Search</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                        <?php
                                        if (isset($by_individu)) {

                                            if ($by_individu->num_rows() < 1) {
                                                echo '<div class="alert alert-danger mt-3">
                                                    Oops, data Not Found !!
                                                    </div>';
                                            } else {
                                                echo '<div class="row mt-3" style="background-color: #e1f5fe4a;border: 2px solid #d6e4ff; border-radius: 12px;">
                                                    <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-hovered">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4">
                                                                ' . form_open('report-individu', 'method="post" target="_blank"') . '
                                                                <input type="hidden" name="filter" value="' . $_POST['filter'] . '">
                                                                <input type="hidden" name="month" value="' . $_POST['month'] . '">
                                                                <input type="hidden" name="year" value="' . $_POST['year'] . '">
                                                                <button type="submit" class="btn btn-warning" target="_blank"><i class="feather icon-printer r-no-margin mr-2"></i> Export Pdf</button>
                                                                ' . form_close() . '
                                                                </th>
                                                                <th colspan="2" class="text-right">
                                                                <i class="feather icon-file-text blue"></i> Show result : ' . count($by_individu->result()) . ' data
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center">No.</th>
                                                                <th>Name of Applicant</th>
                                                                <th>Package Subscribe</th>
                                                                <th>Segment</th>
                                                                <th>Date</th>
                                                                <th class="text-center">Commission Earn</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>';
                                                $no = 1;
                                                $tot = 0;
                                                foreach ($by_individu->result() as $in) {
                                                    $dt_paid = ($in->date_paid == '') ? '' : $in->date_paid;
                                                    echo '<tr>
                                                        <td align="center">' . $no . '.</td>
                                                        <td>' . $in->name_customer . '</td>
                                                        <td>' . $in->name_package . '</td>
                                                        <td>' . $in->name_segment . '</td>
                                                        <td>' . $dt_paid . '</td>
                                                        <td align="right">RM ' . number_format($in->commision, 2) . '</td>
                                                        </tr>';
                                                    $tot += $in->commision;
                                                    $no++;
                                                }
                                                echo '</tbody>
                                                    <tfooter>
                                                        <tr>
                                                            <th colspan="4"></th>
                                                            <th class="text-center">TOTAL</th>
                                                            <th class="text-right">RM ' . number_format($tot, 2) . '</th>
                                                        </tr>
                                                    </tfooter>
                                                    </table>
                                                    </div>';
                                            }

                                            echo "</div></div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>