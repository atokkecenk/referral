<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Create Report</h5>
                                </div>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>"><i class="feather icon-home"></i></a></li>
                                    <li class="breadcrumb-item">Active</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Monthly Statement</h5>
                                    </div>
                                    <div class="card-body">
                                        <?= form_open('sale-monthly', 'method="post"') ?>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Credential</label>
                                                    <select class="form-control credentialMonthly" required>
                                                        <option value="">Nothing Selected</option>
                                                        <option value="agent">Kedai</option>
                                                        <option value="individu">Individu</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Nama Perniagaan/Nama Individu</label>
                                                    <div class="nspMonthly"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Month</label>
                                                    <select class="form-control" name="month" required>
                                                        <option value="">Select Month</option>
                                                        <option value="1">January</option>
                                                        <option value="2">February</option>
                                                        <option value="3">March</option>
                                                        <option value="4">April</option>
                                                        <option value="5">May</option>
                                                        <option value="6">June</option>
                                                        <option value="7">July</option>
                                                        <option value="8">Augustust</option>
                                                        <option value="9">September</option>
                                                        <option value="10">October</option>
                                                        <option value="11">November</option>
                                                        <option value="12">December</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Year</label>
                                                    <select class="form-control" name="year" required>
                                                        <option value="">Select Year</option>
                                                        <?php
                                                        $year = date('Y');
                                                        for ($i = $year; $i >= 2018; $i--) {
                                                        ?>
                                                            <option value="<?= $i ?>"><?= $i ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <!-- <label style="color: transparent;">Nama Perniagaan</label> -->
                                                <!-- <div class="form-group"> -->
                                                <button type="submit" class="btn btn-danger"><i class="feather icon-navigation"></i>Send To Email</button>
                                                <!-- </div> -->
                                            </div>
                                        </div>
                                        <?= form_close() ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>