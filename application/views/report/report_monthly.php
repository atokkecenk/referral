<?php

$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
$pdf->SetTitle('REPORT ALL SALE');
$pdf->SetHeaderMargin(30);
$pdf->SetTopMargin(10);
$pdf->setFooterMargin(20);
$pdf->SetAutoPageBreak(true);
$pdf->SetAuthor('Author');
$pdf->SetDisplayMode('real', 'default');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->AddPage();
$html = '<table cellspacing="1" bgcolor="#666666" cellpadding="2" style="font-size: 10px;">
                <tr bgcolor="#ffffff">
                    <th width="5%" align="center">No.</th>
                    <th width="30%" align="center">Name of Applicant</th>
                    <th width="26%" align="center">Package Subscribe</th>
                    <th width="13%" align="center">Segment</th>
                    <th width="13%" align="center">Date</th>
                    <th width="13%" align="center">Commission Earn</th>
                </tr>
        <tbody>
        </tbody>
        </table>';

$pdf->SetFont('helvetica', '', 10);
$html .= '<br>
        <br>
        <br>Checked By : 
        <br>Issued By : 
        <br>
        <br>Approve By :
        <br>Date :
        <br><table cellspacing="1" style="border: 1px solid #000;" cellpadding="2">
            <tr>
                <td colspan="2" width="50%">Received By</td>
            </tr>
            <tr>
                <td width="20%">Name</td>
                <td width="30%"> ALL SALE</td>
            </tr>
            <tr>
                 <td width="20%">Date</td>
                <td width="30%">' . date('d/m/Y') . '</td>
            </tr>
        </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Report Monthly.pdf', 'I');
exit();
