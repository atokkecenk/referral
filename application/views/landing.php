<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/<?= $setting->favicon ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    <?= $setting->lp_title ?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" /> -->
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> -->
  <link href="<?= base_url() ?>assets/landing-page/fa-icon/css/all.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="<?= base_url() ?>assets/landing-page/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/landing-page/css/now-ui-dashboard.css?v=1.3.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?= base_url() ?>assets/landing-page/demo/demo.css" rel="stylesheet" />
  <link href="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.css" rel="stylesheet">
</head>
<style>
  .btn-login {
    font-size: 17px;
    font-weight: bold;
    background-color: #4e73df;
    padding: 1.8em;
    border-radius: 16px;
    margin: 0 12px 0 12px;
  }

  .btn-login:hover,
  .btn-login:active {
    background-color: #3a62d5;
  }

  .btn-search {
    font-size: 16px;
    background-color: #4e73df;
    border-radius: 25px;
    padding: 13px 19px 13px 19px;
  }

  .btn-search:hover,
  .btn-search:active {
    background-color: #3a62d5;
  }

  .form-control-user {
    font-size: 15px;
  }

  .img-logo {
    width: 350px;
    margin-bottom: 15px;
  }

  .brand {
    margin-top: -150px;
  }

  .control-new {
    height: 50px !important;
    font-size: 14px !important;
    background-color: #fff;
  }

  .p-tb-2 {
    padding-top: 4px;
    padding-bottom: 4px;
  }

  .f-15 {
    font-size: 15px !important;
  }

  .fa-whatsapp {
    color: #25D366;
  }
</style>

<body class="offline-doc">
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
    <div class="container">
      <div class="navbar-wrapper">
        <div class="navbar-toggle">
          <button type="button" class="navbar-toggler">
            <span class="navbar-toggler-bar bar1"></span>
            <span class="navbar-toggler-bar bar2"></span>
            <span class="navbar-toggler-bar bar3"></span>
          </button>
        </div>
        <a class="navbar-brand" href="<?= base_url('signin-staff') ?>" title="Sign in Staff"><i class="fas fa-sign-in-alt" style="color: #fff;"></i> Staff Login</a>
        <a class="navbar-brand" href="<?= base_url('promo') ?>" title="Page promo"><i class="fas fa-tags" style="color: #fff;"></i> Promo</a>
        <a class="navbar-brand" href="#"><?= $setting->lp_text ?></a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
        <span class="navbar-toggler-bar navbar-kebab"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end" id="navigation">
        <ul class="navbar-nav">
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?= base_url('login') ?>" target="_blank">
              <i class="fas fa-sign-in-alt mr-2"></i>Portal Login
            </a>
          </li> -->
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="page-header clear-filter" filter-color="orange">
    <div class="page-header-image" style="background-image: url('<?= base_url('assets/landing-page/img/' . $setting->lp_background) ?>');"></div>
    <div class="container text-center">
      <div class="col-lg-12 ml-auto mr-auto">
        <div class="brand">
          <!-- <h1 class="title"> -->
          <!-- <?= $setting->lp_text ?> -->
          <!-- </h1> -->
          <?php
          if ($this->session->flashdata('success')) {
            echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
          } elseif ($this->session->flashdata('error')) {
            echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
          }
          ?>
          <img src="<?= base_url('assets/landing-page/img/' . $setting->logo_landing) ?>" class="img-logo">
          <div class="text-center">
            <button type="button" data-toggle="modal" data-target="#modalAgentLogin" class="btn btn-login"><img src="<?= base_url() ?>assets/landing-page/img/login.png" class="mr-3" width="60px">AGENT LOGIN</button>
          </div>
          <br />
          <?= form_open('search', 'method="post"') ?>
          <div class="row">
            <div class="col-md-3 mt-2">
              <select class="form-control control-new f-15 p-tb-3" name="filter" required oninvalid="this.setCustomValidity('Select One')" oninput="setCustomValidity('')">
                <option value="">Nothing Selected</option>
                <option value="individu">Individu</option>
                <option value="agent">Kedai</option>
              </select>
            </div>
            <div class="col-md-7">
              <input type="text" class="form-control control-new mt-2" name="search" placeholder="SEARCH SALES/AGENT/SHOP..." required oninvalid="this.setCustomValidity('Please Input This Field')" oninput="setCustomValidity('')">
            </div>
            <div class="col-md-2">
              <button type="submit" class="btn btn-search"><i class="fas fa-search mr-2"></i>SEARCH</button>
            </div>
          </div>
          <?= form_close() ?>
          <?php
          if (isset($search)) {
            if ($search->num_rows() < 1) {
              echo '
              <div class="row mt-3 mb-3">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body" style="color: #000 !important;">
                        <h4 style="padding: 0; margin: 0;"><strong>Data Not Found..</strong></h4>
                    </div>
                  </div>
                </div>
              </div>
              ';
            } else {
              echo '<div class="row mt-3 mb-3">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-body">
                        <div class="table-responsive">
                        <div class="table-responsive">
                          <table class="table table-bordered">
                            <thead class="text-primary">
                              <tr style="font-size: 11px; font-weight: bold;">';

              if ($_POST['filter'] == 'agent') {
                echo '<th style="align: left;" width="20%">Nama Perniagaan</th>
                      <th style="align: left;" width="17%">No Pendaftaran Syarikat</th>';
              } elseif ($_POST['filter'] == 'individu') {
                echo '<th style="align: left;" width="20%">Nama</th>
                      <th style="align: left;" width="17%">No KAD Pengenalan</th>';
              }
              echo '<th style="align: left;" width="23%">Alamat</th>
                      <th style="align: left;" width="15%">No Telephone</th>
                      <th style="align: left;" width="10%">Total Sale</th>
                      <th style="align: left;" width="35%">Gambar</th>
                      </tr>
                    </thead>
                    <tbody>';
              foreach ($search->result() as $src) {
                $no_order = ($src->order_number == '') ? 'Please update' : $src->order_number;
          ?>
                <tr style="color: #000 !important; font-size: 13px;">
                  <td align="center"><?= strtoupper($src->name_sales_person) ?></td>
                  <td align="center"><?= $src->nomor ?></td>
                  <td align="center"><?= ucwords($src->alamat) ?></td>
                  <td align="center"><?= $src->no_hp ?></td>
                  <td align="center"><?= $src->hitung ?></td>
                  <?php
                  if ($_POST['filter'] == 'agent') {
                    if ($src->foto == '') {
                      echo '<td><i>No Image</i></td>';
                    } else {
                      echo '<td align="center"><img src="' . base_url('assets/images/kedai/' . $src->foto) . '" class="img-responsive" width="100%"></td>';
                    }
                  } elseif ($_POST['filter'] == 'individu') {
                    if ($src->foto == '') {
                      echo '<td><i>No Image</i></td>';
                    } else {
                      echo '<td align="center"><img src="' . base_url('assets/images/sales/' . $src->foto) . '" class="img-responsive" width="100%"></td>';
                    }
                  }
                  ?>
                </tr>
          <?php
              }
              echo '</tbody>
                            </table>
                          </div>
                        <font style="color: #000; font-size: 16px;">Show <strong>' . $search->num_rows() . '</strong> result found</font>
                      </div>
                    </div>
                  </div>
                </div>';
            }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="container-fluid">
      <!-- <nav>
        <ul>
          <li>
            <a href="https://www.creative-tim.com">
              Creative Tim
            </a>
          </li>
          <li>
            <a href="http://presentation.creative-tim.com">
              About Us
            </a>
          </li>
          <li>
            <a href="http://blog.creative-tim.com">
              Blog
            </a>
          </li>
        </ul>
      </nav> -->
      <div class="copyright" id="copyright">
        <a href="https://web.whatsapp.com/send?phone=<?= $setting->no_hp ?>&text=Hello...." target="_blank" style="font-size: 22px; text-decoration: none; color: #fff;" title="Contact Us"><i class="fab fa-whatsapp fa-2x mr-2"></i>Whatsapp</a>
        <!-- &copy;
        <script>
          document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
        </script> -->
        <!-- <strong><?= $setting->name ?></strong> -->
      </div>
    </div>
  </footer>

  <!-- Modal Login Agent -->
  <div class="modal fade" id="modalAgentLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Sign In Agent</h5>
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> -->
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <?= form_open('check-agent', 'class="user" method="post"') ?>
              <label>Username</label>
              <div class="form-group mb-4">
                <input type="text" name="username" class="form-control form-control-user" placeholder="Enter Username..">
              </div>
              <div class="form-group mb-4">
                <label>Password</label>
                <input type="password" name="password" class="form-control form-control-user" placeholder="Enter Password..">
                <small style="color: red;">* For Default Password <b>ABC123</b></small>
              </div>
              <div class="form-group">
                <div class="custom-control custom-checkbox small">
                  <input type="checkbox" class="custom-control-input" id="customCheck">
                  <label class="custom-control-label" for="customCheck">Remember
                    Me</label>
                </div>
              </div>
              <button type="submit" class="btn btn-search btn-block" style="font-size: 14px; font-weight: bold;">
                Sign In
              </button>
              <?= form_close() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="<?= base_url() ?>assets/landing-page/js/core/jquery.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/popper.min.js"></script>
  <script src="<?= base_url() ?>assets/landing-page/js/core/bootstrap.min.js"></script>
  <!-- <script src="<?= base_url() ?>assets/landing-page/js/plugins/perfect-scrollbar.jquery.min.js"></script> -->
  <script src="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.js"></script>

  <!-- Chart JS -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/chartjs.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?= base_url() ?>assets/landing-page/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
  <!-- <script src="<?= base_url() ?>assets/landing-page/js/now-ui-dashboard.min.js?v=1.3.0" type="text/javascript"></script> -->
  <!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?= base_url() ?>assets/landing-page/demo/demo.js"></script>
  <script>
    const flashdata = $(".flash-data").data("flashdata");
    const flashtipe = $(".flash-data").data("flashtipe");

    if (flashdata && flashtipe == "success") {
      sweetAlert({
        title: "Success",
        text: flashdata,
        type: "success",
        showConfirmButton: false,
        timer: 2600,
      });
    } else if (flashdata && flashtipe == "error") {
      sweetAlert({
        title: "Error",
        text: flashdata,
        type: "error",
        showConfirmButton: false,
        timer: 2600,
      });
    }
  </script>
</body>

</html>