<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>ON YOUR SYSTEM</title>
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/48x48.png">
</head>
<style>
    body {
        font-family: 'Courier New', Courier, monospace !important;
        background-color: #efefef;
    }

    .container {
        width: 700px;
        border: 2px solid red;
        border-radius: 10px;
        padding: 3em 3em .6em 3em;
        text-align: center;
        font-size: 28px;
        color: red;
        margin: 0 auto;
        margin-top: 55px;
    }

    .img-maintenance {
        width: 250px;
        margin-bottom: 50px;
    }

    .logo {
        margin-top: 50px;
        width: 100px;
    }

    .footer {
        font-size: 16px;
        color: #000;
    }

    .text {
        padding: .7em 0 .7em 0;
        border-bottom: 4px solid red;
    }

    .btn {
        cursor: pointer;
        padding: 8px 16px 8px 16px;
        outline: none;
        border: 0;
        font-weight: bold;
        background-color: red;
        color: #fff;
        border-radius: 4px;
        transition: .5s;
    }

    .btn:hover {
        background-color: #000;
    }

    .btn-r {
        cursor: pointer;
        padding: 8px 16px 8px 16px;
        outline: none;
        border: 0;
        font-weight: bold;
        background-color: green;
        color: #fff;
        border-radius: 4px;
        transition: .5s;
    }

    .btn-r:hover {
        background-color: #000;
    }
</style>

<body>
    <?= form_open('on-system', 'method="post"') ?>
    <div class="container">
        <?php
        if (isset($msg)) {
            echo $msg;
        }
        ?>
        <p><b>TURN ON</b> YOUR SYSTEM</p>
        <?php
        $num1 = rand(1, 30);
        $num2 = rand(1, 30);
        echo $num1 . ' + ' . $num2 . ' = ';
        ?>
        <input type="hidden" name="num1" value="<?= $num1 ?>">
        <input type="hidden" name="num2" value="<?= $num2 ?>">
        <input type="number" min="0" max="50" name="questions" placeholder="Your questions" style="color: red; font-size: 14px; height: 27px; width: 130px;" required oninvalid="this.setCustomValidity('Input your questions')" oninput="this.setCustomValidity('')">
        <button type="submit" class="btn">OK</button>
        <?php
        if (isset($back)) {
            echo '<a href="' . base_url() . '" class="btn-r" style="font-size: 14px; text-decoration: none;" title="Back to website">Back to Website</a>';
        } else {
        ?>
            <button type="button" onclick="javascript:location.href = 'on'" class="btn-r">Reload</button>
        <?php
        }
        $web = $this->db->query("SELECT website FROM tb_setting WHERE id = 1")->row()->website;
        ?>
        <p style="color: #000; font-size: 16px; padding-top: 5em;"><?= $web.' ' ?> &copy; <?= date('Y') ?></p>
    </div>
    <?= form_close() ?>
</body>

</html>