<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Verify Your Account</title>
    <link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/setting/fav.png">
</head>
<style>
    body {
        font-family: Nunito, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
        background-color: #ffffff;
    }

    .container {
        width: 700px;
        /* border: 1px solid red; */
        border-radius: 10px;
        padding: 3em 3em 3em 3em;
        text-align: center;
        color: red;
        margin: 0 auto;
        margin-top: 55px;
    }

    .success {
        padding: 1.5em 1.5em .8em 1.5em;
        background-color: #d4edda;
        border-color: #c3e6cb;
        border-radius: 10px;
        color: #155724;
        font-size: 18px;
    }

    .danger {
        padding: 1.5em 1.5em .8em 1.5em;
        background-color: #f8d7da;
        border-color: #f5c6cb;
        border-radius: 10px;
        color: #721c24;
        font-size: 18px;
    }

    .btn {
        border-radius: 5px;
        padding: 3px 14px 8px 14px;
        text-decoration: none;
        color: #ffffff;
        font-size: 14px;
    }

    .btn-success {
        background-color: #0f6848;
    }

    .btn-danger {
        background-color: #721c24;
    }

    .footer {
        margin-top: 2.2em;
        font-size: 14px;
        color: #000;
    }
</style>

<body>
    <div class="container">
        <div class="<?= $color ?>">
            <?= $message ?>
            <br>
            <br>
            <a href="<?= base_url() ?>" class="btn <?= $btn ?>">Click to Login</a>
            <div class="footer">
                eReferral &copy; <?= date('Y') ?>
            </div>
        </div>
    </div>
</body>

</html>