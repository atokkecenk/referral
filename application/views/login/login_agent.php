<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login Page</title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url() ?>assets/sbadmin-2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url() ?>assets/sbadmin-2/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-6 col-lg-6 col-md-8">
                <?php
                if ($this->session->flashdata('success')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('success') . '" data-flashtipe="success"></div>';
                } elseif ($this->session->flashdata('error')) {
                    echo '<div class="flash-data" data-flashdata="' . $this->session->flashdata('error') . '" data-flashtipe="error"></div>';
                }
                ?>
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body" style="padding: 30px 0 40px 0;">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h3 text-gray-900 mb-4" style="font-weight: bold; text-align: left; color: #4e73df !important;">Sign In &rarr; Agent</h1>
                                    </div>
                                    <?= form_open('check-agent', 'class="user" method="post"') ?>
                                    <label>Username</label>
                                    <div class="form-group mb-4">
                                        <input type="text" name="username" class="form-control form-control-user" placeholder="Enter Username..">
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Password</label>
                                        <input type="password" name="password" class="form-control form-control-user" placeholder="Enter Password..">
                                        <small style="color: red;">* For Default Password <b>ABC123</b></small>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox small">
                                            <input type="checkbox" class="custom-control-input" id="customCheck">
                                            <label class="custom-control-label" for="customCheck">Remember
                                                Me</label>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block" style="font-size: 14px; font-weight: bold;">
                                        Sign In
                                    </button>
                                    <?= form_close() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?= base_url() ?>assets/sbadmin-2/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/sbadmin-2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= base_url() ?>assets/sbadmin-2/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= base_url() ?>assets/sbadmin-2/js/sb-admin-2.min.js"></script>
    <script src="<?= base_url() ?>assets/template/plugins/sweetalert/sweetalert.min.js"></script>

    <script>
        const flashdata = $(".flash-data").data("flashdata");
        const flashtipe = $(".flash-data").data("flashtipe");

        if (flashdata && flashtipe == "success") {
            sweetAlert({
                title: "Success",
                text: flashdata,
                type: "success",
                showConfirmButton: false,
                timer: 2600,
            });
        } else if (flashdata && flashtipe == "error") {
            sweetAlert({
                title: "Error",
                text: flashdata,
                type: "error",
                showConfirmButton: false,
                timer: 2600,
            });
        }
    </script>
</body>

</html>