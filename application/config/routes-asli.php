<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Landing';
$route['auth'] = 'Landing';
$route['signin-staff'] = 'Auth/staff_login';
$route['signin-agent'] = 'Auth/agent_login';
$route['check-staff'] = 'Auth/login_staff';
$route['check-agent'] = 'Auth/login_agent';
$route['logout'] = 'Auth/logout';
$route['dashboard'] = 'Home/dashboard';

$route['scan/(.+)'] = 'Eform/scan_eform/$1';
$route['get-package'] = 'Eform/get_package';
$route['get-daerah'] = 'Kedai/get_daerah';

$route['get-package-by-id/(.+)'] = 'Eform/get_package_by_id/$1';
$route['get-type-by-id/(.+)'] = 'Eform/get_type_by_id/$1';

$route['e-form'] = 'Eform';
$route['save-eform'] = 'Eform/save';
$route['save-edit-password'] = 'Home/edit_password';
$route['edit-usernamepassword'] = 'Home/edit_usernamepassword';
$route['save-edit-username'] = 'Home/edit_username';
$route['add-announcement'] = 'Announcement/add';
$route['save-announcement'] = 'Announcement/save';

$route['upload-image-kedai'] = 'Kedai/upload_image';
$route['upload-image-individu'] = 'Sales/upload_image';

// ANNOUNCEMENT
$route['view-announcement'] = 'Announcement/view';
$route['announcement'] = 'Announcement/view_announcement';
$route['status-announcement/(.+)'] = 'Announcement/update_announcement_status/$1';
$route['delete-annc'] = 'Home/delete/announcement';
$route['ins-announcement'] = 'Announcement/cek_ins_announcement';

// KELUHAN
$route['keluhan'] = 'Keluhan/view';
$route['save-keluhan'] = 'Keluhan/save';

// INDIVIDU
$route['add-sales'] = 'Sales/add';
$route['save-sales'] = 'Sales/save';
$route['view-sales'] = 'Sales/view';
$route['edit-individu/(.+)'] = 'Sales/edit/$1';
$route['save-edit-sales'] = 'Sales/save_edit';

// KEDAI
$route['add-kedai'] = 'Kedai/add';
$route['save-kedai'] = 'Kedai/save';
$route['view-kedai'] = 'Kedai/view';
$route['edit-kedai/(.+)'] = 'Kedai/edit/$1';
$route['save-edit-kedai'] = 'Kedai/save_edit';
$route['change-foto'] = 'Kedai/upload_image';

// USER
$route['get-all-user'] = 'Setting/get_all_user';
$route['view-user'] = 'Setting/view_user';
$route['delete-user'] = 'Setting/delete_user';
$route['reset-password'] = 'Setting/reset_password';
$route['add-new-user'] = 'Setting/add_new_user';

// ORDER
$route['view-order/(.+)'] = 'Order/view_order/$1';
$route['view-order-agent/(.+)'] = 'Order/view_order_agent/$1';
$route['view-order-individu/(.+)'] = 'Order/view_order_individu/$1';
$route['update-order-number'] = 'Order/update_order_number';
$route['edit-order/(.+)'] = 'Order/edit_order/$1';
$route['save-edit-order'] = 'Order/save_edit_order';
$route['save-order/(.+)'] = 'Order/save_order/$1';
$route['save-package'] = 'Order/save_package';
$route['update-package'] = 'Order/update_package';
$route['detail-order/(.+)'] = 'Order/view_order_detail/$1';
$route['delete-order/(.+)/(.+)'] = 'Order/delete_order/$1/$2';
$route['delete-kedai/(.+)'] = 'Kedai/delete/$1';
$route['delete-individu/(.+)'] = 'Sales/delete/$1';
$route['delete-package/(.+)'] = 'Setting/delete_package/$1';

// DELETE DATA
$route['delete'] = 'Delete/delete_filter';

// SEARCH
$route['search'] = 'Landing/search';

// COMISSION
$route['update-comission'] = 'Order/update_comission';
$route['update-referral-staff'] = 'Order/update_refferal_staff';


// DOWNLOAD FILE
$route['download/(.+)'] = 'Home/download/$1';
// VERIFY
$route['verify/(.+)'] = 'Verify/cek/$1';

// NOTIFICATION
$route['notification'] = 'Setting/notification';
$route['set-notif'] = 'Setting/set_notif_order_status';

// JSON DATA
$route['get-order-by-id/(.+)'] = 'Order/get_order_by_id/$1';

// REPORT
$route['report'] = 'Report/view_report';
$route['monthly-statement'] = 'Report/sale_monthly';
// $route['sale-all'] = 'Report/sale_all';
// REPORT FILE PDF
$route['sale-monthly'] = 'Report/send_email';
$route['report-kedai'] = 'Report/sale_by_kedai';
$route['report-individu'] = 'Report/sale_by_individu';
$route['report-all'] = 'Report/sale_all';
// REPORT VIEW
$route['all-sale'] = 'Report/report_all_sale';
$route['sale-by-kedai'] = 'Report/report_by_kedai';
$route['sale-by-individu'] = 'Report/report_by_individu';

// SETTING
$route['set-code'] = 'Setting/view_code';
$route['set-package'] = 'Setting/view_package';
$route['update-code'] = 'Setting/update_code';
$route['set-website'] = 'Setting/setting_website';
$route['set-text'] = 'Setting/setting_text';
$route['set-favicon'] = 'Setting/setting_favicon';
$route['set-logo'] = 'Setting/setting_logo';
$route['set-landing'] = 'Setting/setting_landing';
$route['set-landing-text'] = 'Setting/setting_landing_text';
$route['set-landing-logo'] = 'Setting/setting_landing_logo';
$route['set-landing-background'] = 'Setting/setting_landing_background';
$route['set-smtp'] = 'Setting/setting_smtp';
$route['set-smtp-save'] = 'Setting/setting_smtp_save';
$route['forgot-password'] = 'Auth/forgot_password_staff';

// MAINTENANCE
$route['maintenance'] = 'Maintenance';
$route['on'] = 'Maintenance/on_sistem';
$route['on-system'] = 'Maintenance/update_on_sistem';
$route['off-system'] = 'Maintenance/update_off_sistem';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
