<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Promo extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ModelPromo', 'mPromo');
		// $cek = $this->mSett->cek_maintenance()->maintenance;
		// if ($cek == '1') {
		// 	$this->session->sess_destroy();
		// 	redirect('maintenance');
		// }
	}

	public function add()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$this->render_page('menu/promo/add_promo', $data);
	}

	public function view()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['data'] = $this->mPromo->get_data_promo()->result();
		$this->render_page('menu/promo/view_promo', $data);
	}

	public function view_agent()
	{
		$hash = $this->session->hash;
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['data'] = $this->mPromo->get_promo_by_user($hash)->result();
		$this->render_page('menu/promo/view_promo_agent', $data);
	}

	public function save()
	{
		$id_kedai = $this->session->id_kedai;
		$level = $this->session->level;
		$hash = $this->session->hash;
		$desc = $this->input->post('desc');
		$wa_number = $this->input->post('wa_number');
		$config['upload_path']          = 'assets/landing-promo/';
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 0;
		$config['encrypt_name']         = TRUE;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			$this->session->set_flashdata('error', 'Failed to upload file !!');
			redirect('add-promo');
		} else {
			// $this->upload->do_upload('file');
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];

			$ins = $this->db->insert('tb_promo', [
				'kategori' => $level,
				'kode_user' => $id_kedai,
				'`file`' => $file_name,
				'`desc`' => $desc,
				'wa_number' => $wa_number,
				'tgl_input' => date('Y-m-d H:i:s'),
				'user_input' => $hash
			]);

			if ($ins) {
				$this->session->set_flashdata('success', 'Success to upload, please wait approval..');
				redirect('add-promo');
			}
		}
	}

	// public function save_approve($id = null, $reason = null)
	// {
	// 	$data = [
	// 		'approval' => 'Y',
	// 	];
	// 	$this->db->where('id', '1');
	// 	$upd = $this->db->update('tb_promo', $data);
	// 	// if ($upd) {
	// 	// 	$this->session->set_flashdata('success', 'Approve successfully..');
	// 	// 	redirect('view-promo', 'refresh');
	// 	// }
	// }

	public function save_approve()
	{
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$reason = $this->input->post('reason');
		$sts = isset($status) ? $status : 'Y';
		$rsn = isset($reason) ? $reason : null;
		$data = [
			'approval' => $sts,
			'reason' => $rsn,
			'date_approved' => date('Y-m-d H:i:s'),
			'user_approved' => $this->session->hash,
		];
		$this->db->where('id', $id);
		$upd = $this->db->update('tb_promo', $data);
		if ($upd) {
			$msg = ($sts == 'Y') ? 'Approved' : 'Not approved';
			$this->session->set_flashdata('success', $msg . ' Promo..');
			redirect('view-promo', 'refresh');
		}
	}

	public function delete()
	{
		$id = $this->input->post('id');
		$img = $this->input->post('img');

		$path = FCPATH. 'assets/landing-promo/' . $img;
		unlink($path);

		$this->db->where('id', $id);
		$del = $this->db->delete('tb_promo');

		if ($del) {
			$this->session->set_flashdata('success', 'Berhasil hapus data');
			redirect('view-promo', 'refresh');
		} else {
			$this->session->set_flashdata('error', 'Gagal hapus data promo !!');
			redirect('view-promo', 'refresh');
		}
	}
}
