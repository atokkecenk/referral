<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Report extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Pdf');
        $cek = $this->mSett->cek_maintenance()->maintenance;
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }

        require APPPATH . 'libraries/phpmailer/src/Exception.php';
        require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
        require APPPATH . 'libraries/phpmailer/src/SMTP.php';
    }

    public function view_report()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['all_user'] = $this->mRept->get_all_user()->result();
        $this->render_page('report/report', $data);
    }

    public function report_all_sale()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        if ($this->input->post('month') !== null) {
            $data['all_sale'] = $this->mRept->search_all();
            $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        }
        $this->render_page('report/report_all_sale', $data);
    }

    public function report_by_kedai()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        if ($this->input->post('filter') !== null) {
            $data['by_kedai'] = $this->mRept->search_by('agent');
            $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        }
        $this->render_page('report/report_kedai', $data);
    }

    public function report_by_individu()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        if ($this->input->post('filter') !== null) {
            $data['by_individu'] = $this->mRept->search_by('individu');
            $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        }
        $this->render_page('report/report_individu', $data);
    }

    public function sale_all()
    {
        $data['month'] = $this->input->post('month');
        $data['year'] = $this->input->post('year');
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['all_sale'] = $this->mRept->search_all();
        $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        $this->render_page('report/report_all', $data);
    }

    public function sale_monthly()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['all_user'] = $this->mRept->get_all_user()->result();
        $data['monthly_report'] = $this->mRept->monthly_report();
        $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        $this->render_page('report/monthly_statement', $data);
    }

    public function sale_by_kedai()
    {

        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['by_kedai'] = $this->mRept->search_by('agent');
        $data['nsp'] = $this->mRept->search_by('agent')->row()->name_sales_person;
        $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        $this->render_page('report/report_by_kedai', $data);
    }

    public function sale_by_individu()
    {

        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['by_individu'] = $this->mRept->search_by('individu');
        $data['nsp'] = $this->mRept->search_by('individu')->row()->name_sales_person;
        $data['sett'] = $this->db->query("SELECT * FROM tb_setting WHERE id = '1'")->row();
        $this->render_page('report/report_by_individu', $data);
    }

    public function test()
    {
        $this->load->view('report/monthly');
    }

    public function get_data_user()
    {
        $params = $this->input->post('params', true);
        $q = $this->db->select('a.id_kedai, IFNULL(b.nama_perniagaan, c.nama) AS nsp')
            ->from('tb_user a')
            ->join('tb_kedai b', 'a.id_kedai = b.kode_kedai', 'left')
            ->join('tb_sales c', 'a.id_kedai = c.kode_individu', 'left')
            ->where('a.level', $params)
            ->where('a.active', 'Y')
            ->get()->result();
        echo json_encode($q);
    }

    public function send_email()
    {
        $code = $this->input->post('user');
        $month = $this->input->post('month');
        $year = $this->input->post('year');

        $cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$code'")->row()->level;
        if ($cek == 'agent') {
            $get = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$code'")->row();
            $email = $get->email;
        } elseif ($cek == 'individu') {
            $get = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$code'")->row();
            $email = $get->email;
        }

        $cekkk = $this->mRept->total_monthly_report($code, $month, $year);
        if ($cekkk->num_rows() == 0) {
            $this->session->set_flashdata('error', 'Oops, No data in here !!');
            redirect('monthly-statement', 'refresh');
        } else {
            // $total = $this->total_monthly_report($code, $month, $year)->row();
            $total = $this->mRept->total_monthly_report($code, $month, $year)->row();
            $compl_order = $this->mRept->monthly_report('completed', $code, $month, $year)->row();
            $paid_order = $this->mRept->monthly_report('paid', $code, $month, $year)->row();
            $sett = $this->db->get_where("tb_setting", array('id' => 1))->row();

            $nsp = ($total->name_sales_person == '' || $total->name_sales_person == null) ? '0' : ucwords($total->name_sales_person);
            $bulan = ($total->new_bulan == '' || $total->new_bulan == null) ? '0' : $total->new_bulan;
            $tahun = ($total->tahun == '' || $total->tahun == null) ? '0' : $total->tahun;
            $t_order = ($total->total_order == '' || $total->total_order == null) ? '0' : $total->total_order;
            $complete = ($compl_order->total_order == '' || $compl_order->total_order == null) ? '0' : $compl_order->total_order;
            $paid = ($paid_order->total_order == '' || $paid_order->total_order == null) ? '0' : $paid_order->total_order;
            $commission = ($total->comms == '' || $total->comms == null) ? '0' : $total->comms;
            $codes = ($total->name_sales  == '' || $total->name_sales  == null) ? '0' : $total->name_sales;


            $smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
            $this->load->library('email');
            $config = array();
            $config['protocol']     = 'smtp';
            $config['smtp_host']    = $smtp->host;
            $config['smtp_user']    = $smtp->username;
            $config['smtp_pass']    = $smtp->password;
            $config['smtp_crypto']  = 'ssl';
            $config['mailtype']     = 'html'; // text or html
            $config['charset']          = 'utf-8';
            $config['send_multipart']   = FALSE;
            $config['wordwrap']         = TRUE;
            $config['priority']         = 1;
            $config['smtp_timeout']     = 15;
            $config['smtp_port']        = $smtp->port;
            $config['crlf']             = "\r\n";
            $config['newline']          = "\r\n";
            $config['validate']         = FALSE;
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            
            $this->email->from($smtp->from, $smtp->sender_name);
            $this->email->to($email);
            $this->email->subject("Monthly Statement");
            $this->email->message('<!DOCTYPE html>
                                    <html>
                                    <head>
                                        <title>Month</title>
                                        <style>
                                            body {
                                                font-family: "Helvetica", "Arial", sans-serif;
                                                min-width: 992px !important;
                                            }

                                            @media (min-width: 992px) {
                                                .container {
                                                    max-width: 960px;
                                                }
                                            }

                                            @media (min-width: 1200px) {
                                                .container {
                                                    max-width: 1140px;
                                                }
                                            }

                                            .container {
                                                min-width: 992px !important;
                                                border: 3px solid #00b3ff;
                                                border-radius: 18px;
                                                width: 800px;
                                                margin: 0 auto;
                                                margin-bottom: 2em;
                                                padding: .8em;
                                            }

                                            .logo {
                                                width: 200px;
                                                margin-top: 2em;
                                                margin-bottom: 20px;
                                            }

                                            .f-25 {
                                                font-size: 25px;
                                            }

                                            .f-12 {
                                                font-size: 12px;
                                            }

                                            .bold {
                                                font-weight: bold;
                                            }

                                            .blue {
                                                color: #00b3ff;
                                            }

                                            .total-order {
                                                border: 3px solid #00b3ff;
                                            }

                                            .center {
                                                text-align: center;
                                            }
                                        </style>
                                    </head>

                                    <body>
                                        <div class="container">
                                            <center><img src="https://internet-unifi.com/referral/assets/landing-page/img/' . $sett->logo_landing . '" class="logo" style="width: 250px;"></center>
                                            <h3>Hello <b>' . $nsp . '</b></h3>
                                            <table style="width: 100%; margin-top: 2em;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <center><b style="font-size: 24px; color: #00b3ff;">Your ' . $bulan . ' ' . $tahun . '  e-Statement</b></center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <br><br>
                                            <b>TRANSACTION SUMMARY</b>
                                            <br>
                                            <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px;">
                                                <thead>
                                                    <tr style="padding: 15px;">
                                                        <th>
                                                            <center>TOTAL ORDER</center>
                                                        </th>
                                                        <th>
                                                            <center>TOTAL COMPLETE PROCESS</center>
                                                        </th>
                                                        <th>
                                                            <center>TOTAL ORDER PAID</center>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr style="padding: 15px;">
                                                        <td style="color: #00b3ff;">
                                                            <center>' . $t_order . ' Transactions</center>
                                                        </td>
                                                        <td style="color: #00b3ff;">
                                                            <center>' . $complete . '</center>
                                                        </td>
                                                        <td style="color: #00b3ff;">
                                                            <center>' . $paid . '</center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px; margin-top: 15px;">
                                                <tbody>
                                                    <tr style="padding: 15px;">
                                                        <td>
                                                            <center>TOTAL COMMISSION = RM' . $commission . '</center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="width: 100%; margin-top: 4em;">
                                                <tbody>
                                                    <tr style="padding: 15px;">
                                                        <td>
                                                            <center><a href="#" style="color: red;">Do you have an enquiry?</a></center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="width: 100%; margin-top: .5em;">
                                                <tbody>
                                                    <tr style="padding: 15px;">
                                                        <td colspan="2" style="color: red;">
                                                            <center>Have your family members subscribe to e-Statement & save the environment.</center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="width: 100%; margin-top: 2em;">
                                                <tbody>
                                                    <tr style="padding: 15px;">
                                                        <td>
                                                            <center><img src="https://internet-unifi.com/referral/assets/images/setting/telp.png"> <b>' . $sett->telephone . '</b></center>
                                                        </td>
                                                        <td>
                                                            <center><img src="https://internet-unifi.com/referral/assets/images/setting/web.png"> <b>' . $sett->website . '</b></center>
                                                        </td>
                                                        <td>
                                                            <center><img src="https://internet-unifi.com/referral/assets/images/setting/email.png"> <b>' . $sett->email . '</b></center>
                                                        </td>
                                                        <td>
                                                            <center>
                                                                <img src="https://internet-unifi.com/referral/assets/images/setting/appstore.png" width="100px">
                                                                <img src="https://internet-unifi.com/referral/assets/images/setting/playstore.png" width="130px">
                                                            </center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table style="width: 100%; font-size: 12px !important; margin-top: .2em;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <center>Code : ' . $codes . '</center>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <center>To unsubscribe, <a href="#" style="color: red;">click here</a></center>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <center>Copyright © ' . date('Y') . 'Muafakat Technology.(KT0474208-U). All rights reserved.</center>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        </div>
                                    </body>
                                    </html>');
            //Send mail
            $this->email->send();


            // $smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
            // $response = false;
            // $mail = new PHPMailer();
            // $mail->isSMTP();
            // $mail->Host     = $smtp->host; //sesuaikan sesuai nama domain hosting/server yang digunakan
            // $mail->SMTPAuth = true;
            // $mail->Username = $smtp->username; // user email
            // $mail->Password = $smtp->password; // password email
            // $mail->SMTPSecure = 'ssl';
            // $mail->Port     = $smtp->port;
            // $mail->setFrom($smtp->from, $smtp->sender_name); // user email
            // $mail->addReplyTo($smtp->reply_to, $smtp->sender_name); //user email
            // $mail->addAddress($email); //email tujuan pengiriman email
            // $mail->Subject = 'Monthly Statement'; //subject email
            // $mail->isHTML(true);

            // $isi = '<!DOCTYPE html>
            //     <html>

            //     <head>
            //         <title>Month</title>
            //         <style>
            //             body {
            //                 font-family: "Helvetica", "Arial", sans-serif;
            //                 min-width: 992px !important;
            //             }

            //             @media (min-width: 992px) {
            //                 .container {
            //                     max-width: 960px;
            //                 }
            //             }

            //             @media (min-width: 1200px) {
            //                 .container {
            //                     max-width: 1140px;
            //                 }
            //             }

            //             .container {
            //                 min-width: 992px !important;
            //                 border: 3px solid #00b3ff;
            //                 border-radius: 18px;
            //                 width: 800px;
            //                 margin: 0 auto;
            //                 margin-bottom: 2em;
            //                 padding: .8em;
            //             }

            //             .logo {
            //                 width: 200px;
            //                 margin-top: 2em;
            //                 margin-bottom: 20px;
            //             }

            //             .f-25 {
            //                 font-size: 25px;
            //             }

            //             .f-12 {
            //                 font-size: 12px;
            //             }

            //             .bold {
            //                 font-weight: bold;
            //             }

            //             .blue {
            //                 color: #00b3ff;
            //             }

            //             .total-order {
            //                 border: 3px solid #00b3ff;
            //             }

            //             .center {
            //                 text-align: center;
            //             }
            //         </style>
            //     </head>

            //     <body>
            //         <div class="container">
            //             <center><img src="https://internet-unifi.com/referral/assets/landing-page/img/' . $sett->logo_landing . '" class="logo" style="width: 250px;"></center>
            //             <h3>Hello <b>' . $nsp . '</b></h3>
            //             <table style="width: 100%; margin-top: 2em;">
            //                 <tbody>
            //                     <tr>
            //                         <td>
            //                             <center><b style="font-size: 24px; color: #00b3ff;">Your ' . $bulan . ' ' . $tahun . '  e-Statement</b></center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <br><br>
            //             <b>TRANSACTION SUMMARY</b>
            //             <br>
            //             <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px;">
            //                 <thead>
            //                     <tr style="padding: 15px;">
            //                         <th>
            //                             <center>TOTAL ORDER</center>
            //                         </th>
            //                         <th>
            //                             <center>TOTAL COMPLETE PROCESS</center>
            //                         </th>
            //                         <th>
            //                             <center>TOTAL ORDER PAID</center>
            //                         </th>
            //                     </tr>
            //                 </thead>
            //                 <tbody>
            //                     <tr style="padding: 15px;">
            //                         <td style="color: #00b3ff;">
            //                             <center>' . $t_order . ' Transactions</center>
            //                         </td>
            //                         <td style="color: #00b3ff;">
            //                             <center>' . $complete . '</center>
            //                         </td>
            //                         <td style="color: #00b3ff;">
            //                             <center>' . $paid . '</center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <table style="width: 100%; font-size: 24px; font-weight: bold; border: 3px solid #00b3ff; padding: 15px; margin-top: 15px;">
            //                 <tbody>
            //                     <tr style="padding: 15px;">
            //                         <td>
            //                             <center>TOTAL COMMISSION = RM' . $commission . '</center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <table style="width: 100%; margin-top: 4em;">
            //                 <tbody>
            //                     <tr style="padding: 15px;">
            //                         <td>
            //                             <center><a href="#" style="color: red;">Do you have an enquiry?</a></center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <table style="width: 100%; margin-top: .5em;">
            //                 <tbody>
            //                     <tr style="padding: 15px;">
            //                         <td colspan="2" style="color: red;">
            //                             <center>Have your family members subscribe to e-Statement & save the environment.</center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <table style="width: 100%; margin-top: 2em;">
            //                 <tbody>
            //                     <tr style="padding: 15px;">
            //                         <td>
            //                             <center><img src="https://internet-unifi.com/referral/assets/images/setting/telp.png"> <b>' . $sett->telephone . '</b></center>
            //                         </td>
            //                         <td>
            //                             <center><img src="https://internet-unifi.com/referral/assets/images/setting/web.png"> <b>' . $sett->website . '</b></center>
            //                         </td>
            //                         <td>
            //                             <center><img src="https://internet-unifi.com/referral/assets/images/setting/email.png"> <b>' . $sett->email . '</b></center>
            //                         </td>
            //                         <td>
            //                             <center>
            //                                 <img src="https://internet-unifi.com/referral/assets/images/setting/appstore.png" width="100px">
            //                                 <img src="https://internet-unifi.com/referral/assets/images/setting/playstore.png" width="130px">
            //                             </center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //             <table style="width: 100%; font-size: 12px !important; margin-top: .2em;">
            //                 <tbody>
            //                     <tr>
            //                         <td>
            //                             <center>Code : ' . $codes . '</center>
            //                         </td>
            //                     </tr>
            //                     <tr>
            //                         <td>
            //                             <center>To unsubscribe, <a href="#" style="color: red;">click here</a></center>
            //                         </td>
            //                     </tr>
            //                     <tr>
            //                         <td>
            //                             <center>Copyright © ' . date('Y') . 'Muafakat Technology.(KT0474208-U). All rights reserved.</center>
            //                         </td>
            //                     </tr>
            //                 </tbody>
            //             </table>
            //         </div>

            //         </div>
            //     </body>

            //     </html>';
            // $mailContent = $isi; // isi email
            // $mail->Body = $mailContent;

            // Send email
            // if (!$mail->send()) {
                // echo 'Message could not be sent.';
                // echo 'Mailer Error: ' . $mail->ErrorInfo;
                // $this->session->set_flashdata('error', 'Send email failed !!. Mailer Error: ' . $mail->ErrorInfo);
                // redirect('monthly-statement');
            // } else {
                $this->session->set_flashdata('success', 'Send email to ' . $email . ' successfully..');
                redirect('monthly-statement');
            // }
        }
    }
}
