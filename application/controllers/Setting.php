<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cek = $this->mSett->cek_maintenance()->maintenance;
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }
    }

    function get_all_user()
    {
        $list = $this->mUser->get_datatables();
        $data = array();
        $no = @$_POST['start'];

        foreach ($list as $item) {
            $new_update =  ($item->last_update == '') ? '-' : $item->last_update;
            if ($item->verify == 'Y') {
                $vfy = '<i class="fas fa-check" style="color: green;"></i> Yes';
                $modal = '#modalEditPass';
            } else {
                $vfy = '<i class="fas fa-times" style="color: #f44236;"></i> No';
                $modal = '#modalVerifyMsg';
            }

            $lev = ($item->level == 'staff') ? 'administrator' : $item->level;

            $no++;
            $row = array();
            $row[] = $no . '.';
            $row[] = strtoupper($lev);
            $row[] = $item->id_kedai;
            $row[] = $item->username;
            $row[] = $vfy;
            $row[] = $item->input;
            $row[] = $new_update;
            $row[] = '<button type="button" class="btn btn-primary btn-edit-pass" title="Edit Password" data-id="' . $item->id . '" data-user="' . $item->username . '" data-toggle="modal" data-target="' . $modal . '"><i class="feather icon-edit r-no-margin"></i></button>
                        <button type="button" class="btn btn-warning btn-reset-pass" title="Reset Password" data-id="' . $item->id . '" data-toggle="modal" data-target="#modalResetPassword"><i class="feather icon-rotate-ccw r-no-margin"></i></button>
                        <a href="#" class="btn btn-danger btn-delete-user" title="Delete" data-id="' . $item->id . '" data-code="' . $item->id_kedai . '" data-toggle="modal" data-target="#modalDelete"><i class="feather icon-trash r-no-margin"></i></a>';
            $data[] = $row;
        }

        $datajson = array(
            "draw" => @$_POST['draw'],
            "recordsTotal" => $this->mUser->count_all(),
            "recordsFiltered" => $this->mUser->count_filtered(),
            "data" => $data,
        );

        // output to json format
        echo json_encode($datajson);
    }

    public function view_user()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $this->render_page('menu/staff/view/view_user', $data);
    }

    public function view_code()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['all_code'] = $this->mSett->get_all_code();
        $this->render_page('menu/staff/view/view_code', $data);
    }

    public function view_package()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['all_package'] = $this->mSett->get_all_package();
        $this->render_page('menu/staff/view/view_package', $data);
    }

    public function setting_website()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['setting'] = $this->mSett->get_setting()->result();
        $this->render_page('menu/staff/setting_web', $data);
    }

    public function setting_landing()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['setting'] = $this->mSett->get_setting()->result();
        $this->render_page('menu/staff/setting_landing', $data);
    }

    public function setting_smtp()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $this->session->level;
        $data['setting'] = $this->db->get('tb_setting_smtp')->result();
        $this->render_page('menu/staff/setting_smtp', $data);
    }

    public function setting_smtp_save()
    {
        $id = $this->input->post('id');
        $host = $this->input->post('host');
        $port = $this->input->post('port');
        $user = $this->input->post('user');
        $pass = $this->input->post('pass');
        $from = $this->input->post('from');
        $reply = $this->input->post('reply');
        $sender = $this->input->post('sender');
        $subject = $this->input->post('subject');

        $data = array(
            'host'          => $host,
            'username'      => $user,
            'password'      => $pass,
            'from'          => $from,
            'reply_to'      => $reply,
            'sender_name'   => $sender,
            'subject'   => $subject,
            'port'      => $port
        );
        $this->db->where('id', $id);
        $this->db->update('tb_setting_smtp', $data);
        $this->session->set_flashdata('success', 'Update SMTP Email successfully..');
        redirect('set-smtp');
    }


    public function notification()
    {
        $level = $this->session->level;
        $code = $this->session->id_kedai;
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['code'] = $this->session->id_kedai;
        $data['level'] = $level;
        if ($level == 'agent') {
            $cek = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$code'")->row();
            if ($cek->notif_order_email == 'Y') {
                $data['notif_order'] = "checked";
            } else {
                $data['notif_order'] = null;
            }
            $vw = 'menu/agent/view/setting';
        } else if ($level == 'individu') {
            $cek = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$code'")->row();
            if ($cek->notif_order_email == 'Y') {
                $data['notif_order'] = "checked";
            } else {
                $data['notif_order'] = null;
            }
            $vw = 'menu/individu/view/setting';
        }
        $this->render_page($vw, $data);
    }

    public function set_notif_order_status()
    {
        $level = $this->session->level;
        $code = $this->input->post('code');
        if ($level == 'agent') {
            $whr = 'kode_kedai';
            $tbl = 'tb_kedai';
            $cek = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$code'")->row();
            switch ($cek->notif_order_email) {
                case 'Y':
                    $upd = 'N';
                    break;

                case 'N':
                    $upd = 'Y';
                    break;
            }
        } elseif ($level == 'individu') {
            $whr = 'kode_individu';
            $tbl = 'tb_sales';
            $cek = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$code'")->row();
            switch ($cek->notif_order_email) {
                case 'Y':
                    $upd = 'N';
                    break;

                case 'N':
                    $upd = 'Y';
                    break;
            }
        }
        $this->db->where($whr, $code);
        $this->db->update($tbl, array('notif_order_email' => $upd));
        redirect('notification');
    }

    public function setting_text()
    {
        $title = $this->input->post('title');
        $name = $this->input->post('name');
        // $hp = $this->input->post('hp');
        $email = $this->input->post('email');
        $alamat = $this->input->post('alamat');
        $tlp = $this->input->post('tlp');
        $faks = $this->input->post('faks');
        $website = $this->input->post('website');
        $meta_description = $this->input->post('meta_description');
        $meta_keywords = $this->input->post('meta_keywords');
        $hash = $this->session->hash;

        $data = array(
            'title' => $title,
            'name'  => $name,
            'alamat' => $alamat,
            'telephone' => $tlp,
            'email' => $email,
            'faks' => $faks,
            'website' => $website,
            'meta_description'  => $meta_description,
            'meta_keyword'      => $meta_keywords,
            'user_edit' => $hash,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where(array('id' => 1));
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Save setting website successfully..');
        redirect('set-website');
    }

    public function setting_favicon()
    {
        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 1024;
        $config['max_width']            = 2500;
        $config['max_height']           = 2500;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('favicon');
        $upload_data = $this->upload->data();

        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '50%';
        $config['width'] = 35;
        $config['height'] = 35;
        $config['new_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $file_name = $upload_data['file_name'];

        $data = array(
            'favicon' => $file_name,
            'user_edit' => $this->session->hash,
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);

        $this->session->set_flashdata('success', 'Save setting website successfully..');
        redirect('set-website');
    }

    public function setting_logo()
    {
        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 1024;
        $config['max_width']            = 2500;
        $config['max_height']           = 2500;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('logo');
        $upload_data = $this->upload->data();

        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '50%';
        $config['width'] = 35;
        $config['height'] = 35;
        $config['new_image'] = 'assets/images/setting/' . $upload_data['file_name'];
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();

        $file_name = $upload_data['file_name'];

        $data = array(
            'logo' => $file_name,
            'user_edit' => $this->session->hash,
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);

        $this->session->set_flashdata('success', 'Save setting website successfully..');
        redirect('set-website');
    }

    public function setting_landing_text()
    {
        $title = $this->input->post('title');
        $no_hp = $this->input->post('hp');
        $text = $this->input->post('text');
        $hash = $this->session->userdata('hash');

        $data = array(
            'lp_title' => $title,
            'lp_text'  => $text,
            'no_hp'  => $no_hp,
            'user_edit' => $hash,
            'date_edit' => date('Y-m-d H:i:s')
        );

        $this->db->where(array('id' => 1));
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Save setting landing page successfully..');
        redirect('set-landing');
    }

    public function setting_landing_logo()
    {
        $config['upload_path']          = 'assets/landing-page/img/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('logo_landing');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];

        $data = array(
            'logo_landing' => $file_name,
            'user_edit' => $this->session->userdata('hash'),
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Save setting website successfully..');
        redirect('set-landing');
    }

    public function setting_landing_background()
    {
        $config['upload_path']          = 'assets/landing-page/img/';
        $config['allowed_types']        = 'jpeg|jpg|png|ico';
        $config['max_size']             = 5024;
        $config['max_width']            = 6000;
        $config['max_height']           = 6000;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);

        $this->upload->do_upload('background');
        $upload_data = $this->upload->data();
        $file_name = $upload_data['file_name'];

        $data = array(
            'lp_background' => $file_name,
            'user_edit' => $this->session->userdata('hash'),
            'date_edit' => date("Y-m-d H:i:s")
        );

        $this->db->where('id', 1);
        $this->db->update('tb_setting', $data);
        $this->session->set_flashdata('success', 'Save background landing page successfully..');
        redirect('set-landing');
    }

    public function update_code()
    {
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $this->db->where('id_code', $id);
        $this->db->update('tb_id_code', array(
            'code_string' => $code,
            'code' => '',
            'edit_date' => date('Y-m-d H:i:s')
        ));
        $this->session->set_flashdata('success', 'Set new code successfully..');
        redirect('set-code');
    }

    public function delete_package($id)
    {
        $this->db->where('sha1(id_pkg)', $id);
        $this->db->delete('tb_package');
        $this->session->set_flashdata('success', 'Delete data package successfully..');
        redirect('set-package');
    }

    public function delete_user()
    {
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai ='$code'")->row()->level;
        if ($cek == 'agent') {
            $this->db->where('kode_kedai', $code);
            $this->db->delete('tb_kedai');
        } elseif ($cek == 'individu') {
            $this->db->where('kode_individu', $code);
            $this->db->delete('tb_sales');
        }

        $whr = [
            'id' => $id,
            'id_kedai' => $code,
        ];
        // $this->db->where('id', $id);
        $this->db->where($whr);
        $this->db->update('tb_user', ['active' => 'N', 'verify' => 'N']);
        $this->session->set_flashdata('success', 'Delete data user successfully..');
        redirect('view-user');
    }

    public function reset_password()
    {
        $id = $this->input->post('id');
        $user = $this->db->query("SELECT username FROM tb_user WHERE id = '$id'")->row()->username;
        $this->db->where('id', $id);
        $this->db->update('tb_user', array(
            'password' => md5('ABC123')
        ));
        $this->session->set_flashdata('success', "Reset password username '" . $user . "' successfully..");
        redirect('view-user');
    }

    public function add_new_user()
    {
        $hash = $this->db->query("SELECT MAX(RIGHT(`hash`, 5)) as `no` FROM `tb_user` where `level` = 'user'")->row()->no;
        $hash2 = '00000' . ($hash + 1);
        $no_hash = 'user' . substr($hash2, -5);
        $user = $this->input->post('username', true);
        $new_user = str_replace(' ', '', $user);

        $this->db->insert('tb_user', array(
            'id_kedai' => '',
            'username' => $new_user,
            'password' => md5($this->input->post('password', true)),
            'name' => $this->input->post('nama', true),
            'hash' => $no_hash,
            'level' => 'user',
            'active' => 'Y',
            'verify' => 'Y',
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s')
        ));

        $this->session->set_flashdata('success', "Add new data user successfully..");
        redirect('view-user');
    }
}
