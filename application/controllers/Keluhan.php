<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keluhan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ModelKeluhan', 'mLuhan');
        $cek = $this->mSett->cek_maintenance()->maintenance;
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }
    }

    public function add()
    {
        $level = $this->session->level;
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['code'] = $this->session->id_kedai;
        $data['level'] = $level;
        $this->render_page('menu/staff/add/add_keluhan', $data);
    }

    public function view()
    {
        $level = $this->session->level;
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $level;
        if (in_array($level, ['staff'])) {
            $data['keluhan'] = $this->mLuhan->get_keluhan_all();
        } else {
            $data['keluhan'] = $this->mLuhan->get_keluhan_by_user();
        }
        $this->render_page('menu/staff/view/view_keluhan', $data);
    }

    public function edit($id)
    {
        $level = $this->session->level;
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['level'] = $level;
        $data['keluhan'] = $this->db->get_where('tb_keluhan', ['sha1(id)' => $id])->row();
        $this->render_page('menu/staff/add/add_keluhan_response', $data);
    }

    public function save_response()
    {
        $id = $this->input->post('id_keluhan');
        $response = $this->input->post('response');
        $this->db->where('id', $id);
        $upd = $this->db->update('tb_keluhan', [
            'respon' => $response,
            'status' => 'Y',
            'user_input_respon' => $this->session->hash,
            'date_input_respon' => date('Y-m-d H:i:s')
        ]);
        if ($upd) {
            $this->session->set_flashdata('success', 'Save response keluhan successfully..');
            redirect('keluhan', 'refresh');
        }
    }

    public function save()
    {
        $get_name = $this->db->get_where('tb_user', ['hash' => $this->session->hash])->row()->name;
        $sbj = $this->input->post('subject');
        $txt = $this->input->post('text');
        $ins = $this->db->insert('tb_keluhan', [
            'subject' => $sbj,
            'text' => $txt,
            'user_input' => $this->session->hash,
            'date_input' => date('Y-m-d H:i:s'),
        ]);
        // $admin = $this->db->query("SELECT * FROM `tb_user` where `level` in ('staff','user') and active = 'Y'")->result();
        // foreach ($admin as $key) {
        //     $this->db->insert('tb_keluhan', [
        //         'subject' => $sbj,
        //         'text' => $txt,
        //         'for' => $key->hash,
        //         'user_input' => $this->session->hash,
        //     ]);
        // }
        $this->send_email($get_name, $sbj, $txt);
        if ($ins) {
            $this->session->set_flashdata('success', 'Save data keluhan successfully..');
            redirect('keluhan', 'refresh');
        }
    }

    public function send_email($name, $subject, $text)
    {
        $smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
        $this->load->library('email');
        $config = array();
        $config['protocol']     = 'smtp';
        $config['smtp_host']    = $smtp->host;
        $config['smtp_user']    = $smtp->username;
        $config['smtp_pass']    = $smtp->password;
        $config['smtp_crypto']  = 'ssl';
        $config['mailtype']     = 'html'; // text or html
        $config['charset']          = 'utf-8';
        $config['send_multipart']   = FALSE;
        $config['wordwrap']         = TRUE;
        $config['priority']         = 1;
        $config['smtp_timeout']     = 15;
        $config['smtp_port']        = $smtp->port;
        $config['crlf']             = "\r\n";
        $config['newline']          = "\r\n";
        $config['validate']         = FALSE;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        
        $message = "<!DOCTYPE html>
                        <html lang='en'>
                        <head>
                          <title>Verification</title>
                        </head>
                        <body>
                        Notification Report/Request
                        Name : ".$name."
                        Subject : ".$subject."
                        Text : ".$text."
                        </body>
                    </html>"; 
        // $pesan = $this->input->post('pesan');
        //Load email library
        // $this->load->library('email');
        $this->email->from($smtp->from, $smtp->sender_name);
        $this->email->to($to_email);
        $this->email->subject('Notification Report/Request');
        $this->email->message($message);
        //Send mail
        $this->email->send();
    }
}
