<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Sales extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}

		require APPPATH . 'libraries/phpmailer/src/Exception.php';
		require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH . 'libraries/phpmailer/src/SMTP.php';
	}

	function get_all_sales()
	{
		$list = $this->mSales->get_datatables_sales();
		$data = array();
		$no = @$_POST['start'];

		foreach ($list as $item) {
			if ($item->foto == '') {
				$foto = '<font class="text-c-red" style="font-size: 12px;"><i>No image</i></font>';
			} else {
				$foto = '<img src="' . base_url('assets/images/sales/' . $item->foto) . '" class="img-responsive detailFoto" data-img="' . $item->foto . '" data-ket="individu" data-toggle="modal" data-target="#detailFoto" width="120px" style="cursor: pointer;" title="Click to Detail Image">';
			}

			if ($item->kode_individu == '') {
				$barcode = '<font class="text-c-red" style="font-size: 12px;"><i>No barcode</i></font>';
			} else {
				$barcode = '<img class="qr-code" src="' . base_url('assets/images/qr-code/QR-' . $item->kode_individu . '.png') . '" width="100px">';
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = strtoupper($item->nama);
			$row[] = $item->no_kad_pengenal;
			$row[] = ucwords($item->jantina);
			$row[] = $item->hp;
			$row[] = $item->alamat;
			$row[] = $item->email;
			$row[] = '<p>' . $barcode . '</p><a href="' . base_url('download/QR-' . $item->kode_individu . '.png') . '" class="btn btn-primary btn-sm" target="_blank"><i class="fas fa-download"></i>Download</a>';
			$row[] = '<p>' . $foto . '</p><button type="button" class="btn btn-danger btn-margin modalChangeFoto" data-mode="individu" data-id="' . $item->id . '" data-foto="' . $item->foto . '" data-toggle="modal" data-target="#modalChangeFotoIndividu" title="Change Foto">Change Foto</button>';
			$row[] = $item->nama_bank;
			$row[] = $item->no_account_bank;
			$row[] = '<a href="' . base_url('edit-individu/' . sha1($item->id)) . '" class="btn btn-primary" title="Edit"><i class="feather icon-edit r-no-margin"></i></a>
					 <button class="btn btn-danger mt-1 modalDelete" data-mode="sales" data-key="' . $item->id . '"><i class="feather icon-trash r-no-margin"></i></button>';
			$data[] = $row;
		}

		$datajson = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->mSales->count_all(),
			"recordsFiltered" => $this->mSales->count_filtered(),
			"data" => $data,
		);

		// output to json format
		echo json_encode($datajson);
	}

	public function add()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['bank'] = $this->mReg->get_all_bank()->result();
		$data['negeri'] = $this->mKedai->get_negeri();
		$data['level'] = $this->session->level;
		$this->render_page('menu/staff/add/add_sales', $data);
	}

	public function edit($params)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['bank'] = $this->mReg->get_all_bank()->result();
		$data['negeri'] = $this->mKedai->get_negeri();
		$id_negeri =  $this->mSales->get_data_individu_by_sha($params)->negeri;
		$data['daerah'] = $this->mKedai->get_daerah_by_whr($id_negeri);
		$data['data'] = $this->mSales->get_data_individu_by_sha($params);
		$this->render_page('menu/staff/edit/edit_sales', $data);
	}

	public function view()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$this->render_page('menu/staff/view/view_sales', $data);
	}

	public function save()
	{
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$verify = substr(str_shuffle($string), 0, 40);
		$this->form_validation->set_rules('jantina', 'Jantina', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
			$nm_individu = str_replace(' ', '', $nama);
			$depan = $this->db->query("SELECT * FROM tb_id_code WHERE jenis = 'individu'")->row()->code_string;
			// $code = $this->db->query("SELECT MAX( RIGHT ( kode_individu, 5 ) ) AS `no` FROM tb_sales WHERE kode_individu LIKE '$depan%'")->row();
			$code = $this->db->query("SELECT MAX( RIGHT ( id_kedai, 5 ) ) AS `no` FROM tb_user WHERE id_kedai LIKE '$depan%'")->row();
			$code2 = '00000' . ($code->no + 1);
			$new_code = $depan . substr($code2, -5);
			$username = strtolower($nm_individu) . substr($code2, -5);

			$this->load->library('ciqrcode'); //pemanggilan library QR CODE

			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = 'assets/'; //string, the default is application/cache/
			$config['errorlog']     = 'assets/'; //string, the default is application/logs/
			$config['imagedir']     = 'assets/images/qr-code/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);

			$image_name = 'QR-' . $new_code . '.png'; //buat name dari qr code sesuai dengan nim

			$params['data'] = base_url() . 'scan/' . $new_code; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
			$this->mReg->save_sales($image_name, $verify);

			// redirect('view-sales');
			$data['uri1'] = $this->uri->segment(1);
			$data['uri2'] = $this->uri->segment(2);
			$data['bank'] = $this->mReg->get_all_bank()->result();
			$data['negeri'] = $this->mKedai->get_negeri();
			$data['level'] = $this->session->level;
			$data['email_sess'] = [
				'id' 		=> 1,
				'nama' 		=> $nama,
				'username'	=> $username,
				'email'		=> $email,
				'verify'	=> $verify,
			];
			// $data['data_email_sess'] = $email;
			$this->render_page('menu/staff/add/add_sales', $data);
		}
	}

	public function send_email()
	{
		$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
		$this->load->library('email');
		$config = array();
		$config['protocol']     = 'smtp';
		$config['smtp_host']    = $smtp->host;
		$config['smtp_user']    = $smtp->username;
		$config['smtp_pass']    = $smtp->password;
		$config['smtp_crypto']  = 'ssl';
		$config['mailtype']     = 'html'; // text or html
		$config['charset']          = 'utf-8';
		$config['send_multipart']   = FALSE;
		$config['wordwrap']         = TRUE;
		$config['priority']         = 1;
		$config['smtp_timeout']     = 15;
		$config['smtp_port']        = $smtp->port;
		$config['crlf']             = "\r\n";
		$config['newline']          = "\r\n";
		$config['validate']         = FALSE;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		$from_email = "referral@internet-unifi.com";
		$to_email = $this->input->post('email');
		$user = $this->input->post('user');
		$nama = $this->input->post('nama');
		$verify = $this->input->post('verify');
		$message = "<!DOCTYPE html>
						<html lang='en'>
						<head>
						  <title>Verification</title>
						</head>
						<body>
						<p>
						Account Verification Needed.<br>
						Hi " . $nama . ",<br><br>
						Please verify your account so we know that it's really you.<br><br>
						<a href='" . base_url() . "verify/" . $verify . "' style='padding: 10px; border-radius: 6px; text-decoration: none; font-size: 16px; font-weight: bold; color: #000; background-color: #007bff;'>Verify Your Account Now</a><br><br>
						After Verification Successful, Please used temporary username and password below.<br><br>
						Username : " . $user . "<br>
						Password default : ABC123<br><br>
						Or copy this link <a href='" . base_url() . "verify/" . $verify . "'>" . base_url() . "verify/" . $verify . "</a>
						</p>
						</body>
					</html>";
		// $pesan = $this->input->post('pesan');
		//Load email library
		// $this->load->library('email');
		$this->email->from($smtp->from, $smtp->sender_name);
		$this->email->to($to_email);
		$this->email->subject('VERIFY YOUR ACCOUNT');
		$this->email->message($message);
		//Send mail
		$this->email->send();
		$this->session->set_flashdata('success', 'Save Data And Send Email successfully..');
		redirect('view-sales');
	}

	public function save_backup()
	{
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$verify = substr(str_shuffle($string), 0, 40);
		$this->form_validation->set_rules('jantina', 'Jantina', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->add();
		} else {
			$nm_individu = str_replace(' ', '', $nama);
			$depan = $this->db->query("SELECT * FROM tb_id_code WHERE jenis = 'individu'")->row()->code_string;
			$code = $this->db->query("SELECT MAX( RIGHT ( kode_individu, 5 ) ) AS `no` FROM tb_sales WHERE kode_individu LIKE '$depan%'")->row();
			$code2 = '00000' . ($code->no + 1);
			$new_code = $depan . substr($code2, -5);
			$username = strtolower($nm_individu) . substr($code2, -5);

			$this->load->library('ciqrcode'); //pemanggilan library QR CODE

			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = 'assets/'; //string, the default is application/cache/
			$config['errorlog']     = 'assets/'; //string, the default is application/logs/
			$config['imagedir']     = 'assets/images/qr-code/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);

			$image_name = 'QR-' . $new_code . '.png'; //buat name dari qr code sesuai dengan nim

			// $url = $this->db->query("SELECT link_scan_qrcode AS link_scan FROM tb_setting WHERE id = '1'")->row()->link_scan;
			$params['data'] = base_url() . 'scan/' . $new_code; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High
			$params['size'] = 10;
			$params['savename'] = FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

			$this->mReg->save_sales($image_name, $verify);

			$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
			// PHPMailer object
			$response = false;
			$mail = new PHPMailer();
			// SMTP configuration
			$mail->isSMTP();
			$mail->Host     = $smtp->host; //sesuaikan sesuai nama domain hosting/server yang digunakan
			$mail->SMTPAuth = true;
			$mail->Username = $smtp->username; // user email
			$mail->Password = $smtp->password; // password email
			$mail->SMTPSecure = 'ssl';
			$mail->Port     = $smtp->port;
			$mail->setFrom($smtp->from, $smtp->sender_name); // user email
			$mail->addReplyTo($smtp->reply_to, $smtp->sender_name); //user email
			// Add a recipient
			$mail->addAddress($email); //email tujuan pengiriman email
			// Email subject
			$mail->Subject = 'VERIFY YOUR ACCOUNT'; //subject email
			// Set email format to HTML
			$mail->isHTML(true);
			// Email body content
			$new_nama = strtoupper(strtolower($nama));
			$mailContent = "<p>
						Account Verification Needed.<br>
						Hi " . $new_nama . ",<br><br>
						Please verify your account so we know that it's really you.<br><br>
						<a href='" . base_url() . "verify/" . $verify . "' style='padding: 10px; border-radius: 6px; text-decoration: none; font-size: 16px; font-weight: bold; color: #000; background-color: #007bff;'>Verify Your Account Now</a><br><br>
						After Verification Successful, Please used temporary username and password below.<br><br>
						Username : " . $username . "<br>
						Password default : ABC123<br><br>
						Or copy this link <a href='" . base_url() . "verify/" . $verify . "'>" . base_url() . "verify/" . $verify . "</a>
						</p>"; // isi email
			$mail->Body = $mailContent;

			// Send email
			if (!$mail->send()) {
				echo 'Message could not be sent.';
				echo 'Mailer Error: ' . $mail->ErrorInfo;
			} else {
				echo 'Message has been sent';
			}

			$this->session->set_flashdata('success', 'Save Sales successfully..');
			redirect('view-sales');
		}
	}

	public function save_edit()
	{
		$this->mReg->save_edit_sales();
		$this->session->set_flashdata('success', 'Update data individu successfully..');
		redirect('view-sales');
	}

	public function upload_image()
	{
		$config['upload_path']          = 'assets/images/sales/';
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 5024;
		$config['max_width']            = 6000;
		$config['max_height']           = 6000;
		$config['remove_space']         = TRUE;
		$config['encrypt_name']         = TRUE;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');
		$upload_data = $this->upload->data();
		$file_name = $upload_data['file_name'];

		$kd_individu = $this->input->post('kd_kedai');
		$this->db->where('kode_individu', $kd_individu);
		$this->db->update('tb_sales', array(
			'foto' => $file_name
		));
		$this->session->set_flashdata('success', 'Upload image successfully..');
		redirect('dashboard');
	}

	function get_daerah()
	{
		$id = $this->input->post('id', true);
		$data = $this->mKedai->get_daerah($id);
		echo json_encode($data);
	}

	public function delete($code)
	{
		$file = $this->db->query("SELECT foto FROM tb_sales WHERE kode_individu = '$code'")->row();
		unlink('assets/images/sales/' . $file->foto);
		$this->db->where('kode_individu', $code);
		$this->db->delete('tb_sales');

		$this->db->where('id_kedai', $code);
		$this->db->delete('tb_user');
		$this->session->set_flashdata('success', 'Delete data successfully..');
		redirect('view-sales');
	}
}
