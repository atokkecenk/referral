<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}

		require APPPATH . 'libraries/phpmailer/src/Exception.php';
		require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH . 'libraries/phpmailer/src/SMTP.php';
	}

	public function staff_login()
	{
		if ($this->session->userdata('authenticated') && $this->session->userdata('ereferral'))
			redirect('dashboard');

		$get = $this->db->query("SELECT * FROM tb_setting WHERE id = 1")->row();
		$data['setting'] = $get->favicon;
		$data['email'] = $get->email;
		$data['credential'] = 'Staff';
		$this->load->view('login/login_staff', $data);
	}

	public function agent_login()
	{
		if ($this->session->userdata('authenticated') && $this->session->userdata('ereferral'))
			redirect('dashboard');

		$data['credential'] = 'Agent';
		$this->load->view('login/login_agent', $data);
	}

	public function login_staff()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$user = $this->mUser->get($username);

		if (empty($user)) {
			$this->session->set_flashdata('error', 'Username not found !!');
			redirect('signin-staff');
		} else {
			if ($user->verify == 'N') {
				$this->session->set_flashdata('error', 'Please check your email and verify this account !!');
				redirect('signin-staff');
			} else {
				if ($password == $user->password) {
					$session = array(
						'authenticated' => true,
						'ereferral' => true,
						'username' => $user->username,
						'password' => $user->password,
						'name' => $user->name,
						'level' => $user->level,
						'hash' => $user->hash,
						'id' => $user->id,
						'id_kedai' => $user->id_kedai
					);

					// $cek_annc_active = $this->mDash->cek_announcement_active();
					// if ($cek_annc_active->num_rows() > 0) {
					// 	foreach ($cek_annc_active->result() as $key) {
					// 		$this->mDash->update_announcement($key->id);
					// 	}
					// }

					$this->session->set_flashdata('success', 'Welcome ' . $user->name);
					$this->session->set_userdata($session);
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('error', 'Wrong Password !!');
					redirect('signin-staff');
				}
			}
		}
	}

	public function login_agent()
	{
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$user = $this->mUser->get($username);

		if (empty($user)) {
			$this->session->set_flashdata('error', 'Username not found !!');
			redirect(base_url());
		} else {
			if ($user->verify == 'N') {
				$this->session->set_flashdata('error', 'Please check your email and verify this account !!');
				redirect(base_url());
			} else {
				if ($password == $user->password) {
					$session = array(
						'authenticated' => true,
						'ereferral' => true,
						'username' => $user->username,
						'password' => $user->password,
						'name' => $user->name,
						'level' => $user->level,
						'hash' => $user->hash,
						'id' => $user->id,
						'id_kedai' => $user->id_kedai
					);

					// $cek_annc_active = $this->mDash->cek_announcement_active();
					// if ($cek_annc_active->num_rows() > 0) {
					// 	foreach ($cek_annc_active->result() as $key) {
					// 		$this->mDash->update_announcement($key->id);
					// 	}
					// }

					$this->session->set_flashdata('success', 'Welcome ' . $user->name);
					$this->session->set_userdata($session);
					redirect('dashboard');
				} else {
					$this->session->set_flashdata('error', 'Wrong Password !!');
					redirect(base_url());
				}
			}
		}
	}

	public function forgot_password_staff()
	{
		$email = $this->input->post('email');
		$hash = $this->input->post('hash');

		// PHPMailer object
		$response = false;
		$mail = new PHPMailer();
		// SMTP configuration
		$mail->isSMTP();
		$mail->Host     = 'leona.sg.domainesia.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
		$mail->SMTPAuth = true;
		$mail->Username = 'referral@internet-unifi.com'; // user email
		$mail->Password = 'F@rid012'; // password email
		$mail->SMTPSecure = 'ssl';
		$mail->Port     = 465;
		$mail->setFrom('referral@internet-unifi.com', 'eReferral'); // user email
		$mail->addReplyTo('referral@internet-unifi.com', ''); //user email
		// Add a recipient
		$mail->addAddress($email); //email tujuan pengiriman email
		// Email subject
		$mail->Subject = 'RECOVERY PASSWORD'; //subject email
		// Set email format to HTML
		$mail->isHTML(true);
		$user = $this->db->query("SELECT * FROM tb_user WHERE `hash` = '$hash'")->row()->username;
		// Email body content
		$mailContent = "<p>
						Recovery Password.<br><br>
						Hi <b>".$user."</b>,<br><br>
						Username : ". $user. "<br>
						Password Recovery : ABC123<br><br>
                        Url <b>" . base_url() . "</b><br>
						</p>"; // isi email
		$mail->Body = $mailContent;

		// Send email
		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}

		$this->db->where('hash', $hash);
		$this->db->update('tb_user', array(
			'password' => md5('ABC123')
		));

		$this->session->set_flashdata('success', "Forgot password successfully send, please check your email..");
		redirect('signin-staff');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
