<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verify extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cek = $this->mSett->cek_maintenance()->maintenance;
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }
    }

    public function cek($code)
    {
        $uri1 = $this->uri->segment(1);
        $uri2 = $this->uri->segment(2);
        $data['code'] = $code;
        
        if ($uri1 == 'verify') {
            $cek = $this->db->query("SELECT * FROM tb_verify where verify = '$uri2'");
            if ($cek->num_rows() == 0) {
                $data['message'] = 'The code to verify is <b>Wrong !!</b>';
                $data['color'] = 'danger';
                $data['btn'] = 'btn-danger';
            } else {
                if ($cek->row()->used == 'Y') {
                    $data['message'] = 'This code already <b>in use !!</b>';
                    $data['color'] = 'danger';
                    $data['btn'] = 'btn-danger';
                } else {
                    $id = $cek->row()->owner;
                    $this->db->query("UPDATE tb_verify SET used = 'Y' WHERE verify = '$uri2'");
                    $this->db->query("UPDATE tb_user SET verify = 'Y' WHERE id_kedai = '$id'");
                    $data['message'] = 'This account is success to <b>verify</b>.';
                    $data['color'] = 'success';
                    $data['btn'] = 'btn-success';
                }
            }
        }
        $this->load->view('verify', $data);
    }
}
