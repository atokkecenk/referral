<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Flyer extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->level = $this->session->level;
        $this->username = $this->session->username;
        $cek = $this->mSett->cek_maintenance()->maintenance;
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }
    }

    public function index()
    {
        $data['uri1'] = $this->uri->segment(1);
        $data['uri2'] = $this->uri->segment(2);
        $data['hash'] = sha1($this->session->hash);
        $data['level'] = $this->session->level;
        if (in_array($this->level, ['staff', 'user'])) {
            $data['flyer'] = $this->db->get('tb_flyer')->result();
        } else {
            $data['flyer'] = $this->db->get_where('tb_flyer', ['active' => 'Y'])->result();
        }
        $this->render_page('flyer/view_flyer', $data);
    }

    public function update_template()
    {
        $id = $this->input->post('id_tmp');
        $tmp_name = $this->input->post('tmp_name');
        $note = $this->input->post('note');

        $config['upload_path']          = 'assets/images/setting/';
        $config['allowed_types']        = 'jpeg|jpg|png';
        $config['max_size']             = 0;
        $config['remove_space']         = TRUE;
        $config['encrypt_name']         = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('file')) {
            $this->db->where('id', $id);
            $this->db->update('tb_flyer', array(
                'name'          => $tmp_name,
                'keterangan'    => $note,
            ));
        } else {
            $upload_data = $this->upload->data();
            $file_name = $upload_data['file_name'];

            $this->db->where('id', $id);
            $this->db->update('tb_flyer', array(
                'name'          => $tmp_name,
                'keterangan'    => $note,
                'file'          => $file_name,
            ));
        };

        $this->session->set_flashdata('success', 'Yeaa, update file template successfully..');
        redirect('flyer', 'refresh');
    }

    public function potrait_1($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 1])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_1');
        $pdf = new Potrait_1('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 1');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(17, 261);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/setting/qr.png', 144.5, 227, 54, 54, 'PNG', '', '', true, 100, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
                $nama = $qr->nama_perniagaan;
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
                $nama = $qr->nama;
            }
            $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 144.5, 227, 54, 54, 'PNG', '', '', true, 100, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_2($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 2])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_2');
        $pdf = new Potrait_2('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 2');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(86, 267);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/setting/qr.png', 8.7, 233.2, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
                $nama = $qr->nama_perniagaan;
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
                $nama = $qr->nama;
            }
            $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 8.7, 233.2, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_3($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 3])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_3');
        $pdf = new Potrait_3('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 3');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(54.5, 151);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/setting/qr.png', 78.1, 228.3, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
                $nama = $qr->nama_perniagaan;
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
                $nama = $qr->nama;
            }
            $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'C', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 78.1, 228.3, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_4($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 4])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_4');
        $pdf = new Potrait_4('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 4');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(1.7, 285.5);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            $pdf->StartTransform();
            $pdf->Rotate(90);
            $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'L', 0, '', 0);
            $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 9, 227, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
                $nama = $qr->nama_perniagaan;
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
                $nama = $qr->nama;
            }
            $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'L', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 9, 227, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_5($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 5])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_5');
        $pdf = new Potrait_5('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 5');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(208, 183);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            $pdf->StartTransform();
            $pdf->Rotate(-90);
            $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'R', 0, '', 0);
            $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 147.6, 227.1, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
                $nama = $qr->nama_perniagaan;
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
                $nama = $qr->nama;
            }
            $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'R', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 147.6, 227.1, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_6($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 6])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_6');
        $pdf = new Potrait_6('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 6');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(208, 183);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            // $pdf->StartTransform();
            // $pdf->Rotate(-90);
            // $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'R', 0, '', 0);
            // $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 147.4, 227.1, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
            }
            // $pdf->Cell(100, 0, '( ' . strtoupper($nama) . ' )', 0, 1, 'R', 0, '', 0);
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 147.4, 227.1, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_7($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 7])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_7');
        $pdf = new Potrait_7('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 7');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(1.7, 285.5);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            // $pdf->StartTransform();
            // $pdf->Rotate(90);
            // $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'L', 0, '', 0);
            // $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 11, 227, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
            }
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 11, 227, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_8($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 8])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_8');
        $pdf = new Potrait_8('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 8');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(50, 190);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            // $pdf->StartTransform();
            // $pdf->Rotate(90);
            // $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'L', 0, '', 0);
            // $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 82, 175.5, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
            }
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 82, 175.5, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }

    public function potrait_9($mode, $hash)
    {
        $file_name = $this->db->get_where('tb_flyer', ['id' => 9])->row()->name;
        $get = $this->db->get_where('tb_user', ['sha1(hash)' => $hash])->row();
        $this->load->library('Potrait_9');
        $pdf = new Potrait_9('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetTitle('POTRAIT 9');
        $pdf->SetHeaderMargin(0);
        $pdf->SetTopMargin(0);
        $pdf->setFooterMargin(0);
        $pdf->SetAutoPageBreak(false);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->AddPage();
        // $pdf->setJPEGQuality(75);
        // Print a text
        $html = '<span style="background-color:yellow;color:blue;">&nbsp;PAGE 1&nbsp;</span>
            <p stroke="0.2" fill="true" strokecolor="yellow" color="blue" style="font-family:helvetica;font-weight:bold;font-size:26pt;">
                You can set a full page background.
            </p>
            ';
        $pdf->SetXY(50, 190);
        $pdf->SetFont('helvetica', 'B', 11);
        $pdf->SetTextColor(255, 255, 255);
        if (in_array($this->level, ['staff', 'user'])) {
            // $pdf->StartTransform();
            // $pdf->Rotate(90);
            // $pdf->Cell(100, 0, '( ' . strtoupper($get->name) . ' )', 0, 1, 'L', 0, '', 0);
            // $pdf->StopTransform();
            $pdf->Image('assets/images/setting/qr.png', 81.4, 230.4, 55, 55, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        } else {
            if ($this->level == 'agent') {
                $qr = $this->db->get_where('tb_kedai', ['kode_kedai' => $get->id_kedai])->row();
            } elseif ($this->level == 'individu') {
                $qr = $this->db->get_where('tb_sales', ['kode_individu' => $get->id_kedai])->row();
            }
            $pdf->Image('assets/images/qr-code/' . $qr->qrcode, 11, 227, 54, 54, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
        }
        // $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($file_name . '.pdf', $mode);
        exit();
    }
}
