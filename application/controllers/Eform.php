<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Eform extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}

		require APPPATH . 'libraries/phpmailer/src/Exception.php';
		require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH . 'libraries/phpmailer/src/SMTP.php';
	}

	public function index()
	{
		$data['sme'] = $this->mEform->get_segment('S');
		$data['consumer'] = $this->mEform->get_segment('C');
		$this->load->view('eform/eform_input', $data);
	}

	public function scan_eform($id)
	{
		$data['data'] = $this->mEform->get_data_user($id)->row();
		$data['type'] = $this->mEform->get_all_type();
		$this->load->view('eform/eform_input', $data);
	}

	function get_package()
	{
		$id = $this->input->post('id', true);
		$data = $this->mEform->get_segment($id);
		echo json_encode($data);
	}

	function get_package_by_id($id)
	{
		$data = $this->mEform->get_package_by_id($id);
		echo json_encode($data);
	}

	function get_type_by_id($id)
	{
		$data = $this->mEform->get_type_by_id($id);
		echo json_encode($data);
	}

	public function login($data)
	{
		$this->load->view('login/login', $data);
	}

	public function save()
	{
		$id_kedai = $this->input->post('kedai');
		$kode_kedai = $this->input->post('name_sales');
		$name_sales = $this->input->post('name_sales_string');
		$name_customer = $this->input->post('name_customer');
		$number_ic = $this->input->post('number_ic');
		$segment = $this->input->post('segment');
		$contact1 = $this->input->post('contact1');
		$contact2 = $this->input->post('contact2');
		$email = $this->input->post('email');
		$install_address = $this->input->post('install_address');
		$billing_address = $this->input->post('billing_address');
		$package = $this->input->post('package');
		$type = $this->input->post('type');
		$del_number = $this->input->post('del_number');
		$date = $this->input->post('date');
		$extr = explode('/', $date);
		$new_date = $extr[2] . '-' . $extr[1] . '-' . $extr[0];

		$pkg = $this->mEform->get_package_by_id($package)->package;
		$typ = $this->mEform->get_type_by_id($type)->value;

		$cek = $this->mEform->cek_duplicate_ic($number_ic);
		if ($cek->num_rows() > 0) {
			$this->session->set_flashdata('error', '<b>Oops,</b> duplicate Number IC !!');
			redirect('scan/' . $kode_kedai);
		} else {
			switch ($segment) {
				case 'S':
					$sgmt = 'Business (SME)';
					break;

				case 'C':
					$sgmt = 'Residential (CONSUMER)';
					break;
			}

			if ($del_number == '') {
				$del = '-';
			} else {
				$del = $del_number;
			}

			// $cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$id_kedai'")->row();
			// if ($cek->level == 'agent') {
			// 	$kd = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$id_kedai'")->row();
			// 	$nsp = $kd->nama_perniagaan;
			// } elseif ($cek->level = 'individu') {
			// 	$sls = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$id_kedai'")->row();
			// 	$nsp = $sls->nama;
			// }
			$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$kode_kedai'")->row()->level;
			if ($cek == 'agent') {
				$get = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$kode_kedai'")->row();
				$name = $get->nama_perniagaan;
				$email2 = $get->email;
			} elseif ($cek == 'individu') {
				$get = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$kode_kedai'")->row();
				$name = $get->nama;
				$email2 = $get->email;
			}

			$via =  base_url() . 'scan/' . $kode_kedai;
			$this->mEform->save();  // Save ke database

			$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
			$this->load->library('email');
			$config = array();
			$config['protocol']     = 'smtp';
			$config['smtp_host']    = $smtp->host;
			$config['smtp_user']    = $smtp->username;
			$config['smtp_pass']    = $smtp->password;
			$config['smtp_crypto']  = 'ssl';
			$config['mailtype']     = 'html'; // text or html
			$config['charset']          = 'utf-8';
			$config['send_multipart']   = FALSE;
			$config['wordwrap']         = TRUE;
			$config['priority']         = 1;
			$config['smtp_timeout']     = 15;
			$config['smtp_port']        = $smtp->port;
			$config['crlf']             = "\r\n";
			$config['newline']          = "\r\n";
			$config['validate']         = FALSE;
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			
			$this->email->from($smtp->from, $smtp->sender_name);
			$this->email->to($email);
			$this->email->subject("UNIFI APPLICATION FORM (DIGITAL-FORM)");
			$this->email->message("<!DOCTYPE html>
									<html lang='en'>
									<head>
									  <title>Verification</title>
									</head>
									<body>
									<p>
									E-Reff Agent: " . $name . "<br>
									Segment: " . $sgmt . "<br>
									Customer Name: " . $name_customer . "<br>
									Number IC: " . $number_ic . "<br>
									Contact Number: " . $contact1 . "<br>
									2nd Contact Number: " . $contact2 . "<br>
									Email: " . $email . "<br>
									Installation Address: " . $install_address . "<br>
									Billing Address: " . $billing_address . "<br>
									Package to be subscribed: " . $pkg . "<br>
									Installation Type: " . $typ . "<br>
									Del Number: " . $del . "<br>
									Preffered Installation Date: " . $new_date . "<br><br>
									Term and Condition<br>
									☑ I herreby consent to subscribe the service with subscription contract of 24 month.<br>
									☑ I have read, understand and agree to be bound by the Term and Condition of service.<br>
									☑ I agree to pay advance payment within 10 days after Installation complete.<br>
									☑ I herreby consent TM representative to proceed and process my order. Kindly notify me if there is any issues pertaining to my request.<br><br>
									via " . $via . "
									</p>
									</body>
									</html>");
			//Send mail
			$this->email->send();

			
			$this->send_email_to_agent($email2, $name);

			$text = 'E-Reff%20Agent%20Name:%20' . $name;
			$text .= '%0ASegment:%20' . $sgmt;
			$text .= '%0ACustomer%20Name:%20' . $this->replace_space($name_customer);
			$text .= '%0ANumber%20IC:%20' . $number_ic;
			$text .= '%0AContact%20Number:%20' . $contact1;
			$text .= '%0A2nd%20Contact%20Number:%20' . $contact2;
			$text .= '%0AEmail%20Address:%20' . $email;
			$text .= '%0AInstallation%20Address:%20' . $this->replace_space($install_address);
			$text .= '%0ABilling%20Address:%20' . $this->replace_space($billing_address);
			$text .= '%0APackage%20to%20be%20subscribed:%20' . $this->replace_space($pkg);
			$text .= '%0AInstallation%20Type:%20' . $this->replace_space($typ);
			$text .= '%0APreffered%20Installation%20Date:%20' . $new_date;
			$text .= '%0ADel Number: ' . $del;
			$text .= '%0A';
			$text .= '%0ATerm%20and%20Condition';
			$text .= '%0A☑%20I herreby consent to subscribe the service with subscription contract of 24 month.';
			$text .= '%0A☑%20I have read, understand and agree to be bound by the Term and Condition of service.';
			$text .= '%0A☑%20I agree to pay advance payment within 10 days after Installation complete.';
			$text .= '%0A☑%20I herreby consent TM representative to proceed and process my order. Kindly notify me if there is any issues pertaining to my request.';
			$text .= '%0A%0A';

			$useragent = $_SERVER['HTTP_USER_AGENT'];
			if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {

				redirect('whatsapp://send/?phone=60164447563&text=' . $text . 'via%20' . $via, 'refresh'); // Send to WA
			} else {
				$this->session->set_flashdata('success', 'Send DIGITAL-FORM successfully..');
				// redirect('https://wa.me/60164447563?text='.$text. 'via%20' . $via); // Send to WA
				redirect('https://web.whatsapp.com/send?text='.$text. 'via%20' . $via.'&phone=60164447563'); // Send to WA
			}
			
		}
	}

	public function replace_space($string)
	{
		$data = str_replace(' ', '%20', $string);
		return $data;
	}

	public function send_email_to_agent($email, $name)
	{
		$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
		$this->load->library('email');
		$config = array();
		$config['protocol']     = 'smtp';
		$config['smtp_host']    = $smtp->host;
		$config['smtp_user']    = $smtp->username;
		$config['smtp_pass']    = $smtp->password;
		$config['smtp_crypto']  = 'ssl';
		$config['mailtype']     = 'html'; // text or html
		$config['charset']          = 'utf-8';
		$config['send_multipart']   = FALSE;
		$config['wordwrap']         = TRUE;
		$config['priority']         = 1;
		$config['smtp_timeout']     = 15;
		$config['smtp_port']        = $smtp->port;
		$config['crlf']             = "\r\n";
		$config['newline']          = "\r\n";
		$config['validate']         = FALSE;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		
		$this->email->from($smtp->from, $smtp->sender_name);
		$this->email->to($email);
		$this->email->subject("UNIFI APPLICATION FORM (DIGITAL-FORM)");
		$this->email->message("Dear <b>" . strtoupper($name) . ",</b><br>
								We Have received new application from your referral Link.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.");
		//Send mail
		$this->email->send();
	}
}
