<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $cek = $this->mSett->cek_maintenance()->maintenance;
        $this->load->model('ModelPromo', 'mPromo');
        if ($cek == '1') {
            $this->session->sess_destroy();
            redirect('maintenance');
        }
    }

    public function index()
    {
        $data['setting'] = $this->mLand->get_setting();
        $this->load->view('landing', $data);
    }

    public function promo()
    {
        $data['setting'] = $this->mLand->get_setting();
        $data['slider'] = $this->mPromo->get_slider()->result();
        $data['jumlah'] = $this->mPromo->get_promo_approved('')->num_rows();
        $data['data'] = $this->mPromo->get_promo_approved('')->result();
        $this->load->view('page_promo', $data);
    }
    
    public function promo_search()
    {
        $src = $this->input->post('search');
        $data['setting'] = $this->mLand->get_setting();
        $data['slider'] = $this->mPromo->get_slider()->result();
        $data['jumlah'] = $this->mPromo->get_promo_approved($src)->num_rows();
        $data['data'] = $this->mPromo->get_promo_approved($src)->result();
        $this->load->view('page_promo', $data);
    }

    public function search()
    {
        $filter = $this->input->post('filter');
        $search = $this->input->post('search');

        $data['setting'] = $this->mLand->get_setting();
        $data['search'] = $this->mLand->search();
        $this->load->view('landing', $data);
    }
}
