<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Order extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}

		require APPPATH . 'libraries/phpmailer/src/Exception.php';
		require APPPATH . 'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH . 'libraries/phpmailer/src/SMTP.php';
	}

	public function get_order_log_staff($mode)
	{
		$list = $this->mOrder->dt_order_staff($mode);
		$data = array();
		$no = @$_POST['start'];

		foreach ($list as $item) {
			if ($item->order_number == '') {
				$txtempty = ' <font style="font-size: 12px;">please update</font>';
			} else {
				$txtempty = ' ' . $item->order_number;
			}

			$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$item->name_sales'")->row();
			if ($cek->level == 'agent') {
				$get = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$item->name_sales'")->row();
				$nsp = ucwords(strtolower($get->nama_perniagaan));
			} elseif ($cek->level = 'individu') {
				$nsp = ucwords(strtolower($item->nama_sales_kedai));
			}

			switch ($item->status) {
				case "pending":
					$color = "#ffeb00fa";
					$sts = $item->status;
					break;

				case "port-full":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "upcoming-projects":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "processing":
					$color = "#748892";
					$sts = $item->status;
					break;

				case "paid":
					$color = "#1de9b6";
					$sts = $item->status;
					break;

				case "cancel":
					$color = "#f44236";
					$sts = $item->status;
					break;

				case "rejected":
					$color = "#37474f";
					$sts = $item->status;
					break;

				case "completed":
					$color = "#3ebfea";
					$sts = $item->status;
					break;
			}
			$btn_detail_1 = '<a href="' . base_url('detail-order/' . sha1($item->id)) . '" class="btn btn-danger btn-margin" title="Click to View Detail"><i class="feather icon-eye" style="margin: 0;"></i></a>';
			$btn_detail_2 = '<button type="button" class="btn btn-info btn-view-detail-user" data-toggle="modal" data-target="#modalDetailOrder" data-id="' . $item->id . '" data-mode="' . $item->status . '" title="Click to Add Status Order"><i class="feather icon-arrow-up-right" style="margin: 0 !important;"></i></button>';

			$no++;
			$row = array();
			$row[] = $no . '.';
			$row[] = $btn_detail_1 . '&nbsp;' . $btn_detail_2 . $txtempty;
			$row[] = $item->name_customer;
			$row[] = $nsp;
			$row[] = $item->name_package;
			$row[] = $item->name_segment;
			$row[] = 'RM ' . $item->commision;
			$row[] = '<i class="fas fa-circle f-10 m-r-10" style="color: ' . $color . ';"></i>' . ucwords($sts);
			$row[] = $item->remarks;
			$row[] = $item->new_input;

			if ($this->session->level == 'staff') {
				$row[] = '<button class="btn btn-danger mt-1 modalDelete" data-mode="order" data-key="' . $item->id . '"><i class="feather icon-trash r-no-margin"></i></button>';
				// $row[] = '<a href="' . base_url('delete-order/' . $item->status . '/' . sha1($item->id)) . '" class="btn btn-danger" title="Delete Order"><i class="feather icon-trash r-no-margin"></i></a>';
			} else {
				$row[] = '';
			}
			$data[] = $row;
		}

		$datajson = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->mOrder->count_all_by_staff($mode),
			"recordsFiltered" => $this->mOrder->count_filtered_by_staff($mode),
			"data" => $data,
		);

		// output to json format
		echo json_encode($datajson);
	}

	public function get_order_log_agent($mode)
	{
		$list = $this->mOrder->dt_order_no_staff($mode);
		$data = array();
		$no = @$_POST['start'];

		foreach ($list as $item) {
			if ($item->order_number == '') {
				$txtempty = ' <font style="font-size: 12px;">please update</font>';
			} else {
				$txtempty = ' ' . $item->order_number;
			}

			$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$item->name_sales'")->row();
			if ($cek->level == 'agent') {
				$get = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$item->name_sales'")->row();
				$nsp = ucwords(strtolower($get->nama_perniagaan));
			} elseif ($cek->level = 'individu') {
				$nsp = ucwords(strtolower($item->nama_sales_kedai));
			}

			switch ($item->status) {
				case "pending":
					$color = "#ffeb00fa";
					$sts = $item->status;
					break;

				case "port-full":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "upcoming-projects":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "processing":
					$color = "#748892";
					$sts = $item->status;
					break;

				case "paid":
					$color = "#1de9b6";
					$sts = $item->status;
					break;

				case "cancel":
					$color = "#f44236";
					$sts = $item->status;
					break;

				case "rejected":
					$color = "#37474f";
					$sts = $item->status;
					break;

				case "completed":
					$color = "#3ebfea";
					$sts = $item->status;
					break;
			}

			$no++;
			$row = array();
			$row[] = $no . '.';
			$row[] = $txtempty;
			$row[] = $item->name_customer;
			$row[] = $nsp;
			$row[] = $item->name_package;
			$row[] = $item->name_segment;
			$row[] = 'RM ' . $item->commision;
			$row[] = '<i class="fas fa-circle f-10 m-r-10" style="color: ' . $color . ';"></i>' . ucwords($sts);
			$row[] = $item->remarks;
			$row[] = $item->new_input;
			$data[] = $row;
		}

		$datajson = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->mOrder->count_all_no_staff($mode),
			"recordsFiltered" => $this->mOrder->count_filtered_no_staff($mode),
			"data" => $data,
		);

		// output to json format
		echo json_encode($datajson);
	}

	public function get_order_log_individu($mode)
	{
		$list = $this->mOrder->dt_order_individu($mode);
		$data = array();
		$no = @$_POST['start'];

		foreach ($list as $item) {
			if ($item->order_number == '') {
				$txtempty = ' <font style="font-size: 12px;">please update</font>';
			} else {
				$txtempty = ' ' . $item->order_number;
			}

			$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id_kedai = '$item->name_sales'")->row();
			if ($cek->level == 'agent') {
				$get = $this->db->query("SELECT nama_perniagaan FROM tb_kedai WHERE kode_kedai = '$item->name_sales'")->row();
				$nsp = ucwords(strtolower($get->nama_perniagaan));
			} elseif ($cek->level = 'individu') {
				$nsp = ucwords(strtolower($item->nama_sales_kedai));
			}

			switch ($item->status) {
				case "pending":
					$color = "#ffeb00fa";
					$sts = $item->status;
					break;

				case "port-full":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "upcoming-projects":
					$color = "#007bff";
					$repl =  str_replace("-", " ", $item->status);
					$sts = $repl;
					break;

				case "processing":
					$color = "#748892";
					$sts = $item->status;
					break;

				case "paid":
					$color = "#1de9b6";
					$sts = $item->status;
					break;

				case "cancel":
					$color = "#f44236";
					$sts = $item->status;
					break;

				case "rejected":
					$color = "#37474f";
					$sts = $item->status;
					break;

				case "completed":
					$color = "#3ebfea";
					$sts = $item->status;
					break;
			}

			$no++;
			$row = array();
			$row[] = $no . '.';
			$row[] = $txtempty;
			$row[] = $item->name_customer;
			$row[] = $nsp;
			$row[] = $item->name_package;
			$row[] = $item->name_segment;
			$row[] = 'RM ' . $item->commision;
			$row[] = '<i class="fas fa-circle f-10 m-r-10" style="color: ' . $color . ';"></i>' . ucwords($sts);
			$row[] = $item->remarks;
			$row[] = $item->new_input;
			$data[] = $row;
		}

		$datajson = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->mOrder->count_all_individu($mode),
			"recordsFiltered" => $this->mOrder->count_filtered_individu($mode),
			"data" => $data,
		);

		// output to json format
		echo json_encode($datajson);
	}

	public function get_order_by_id($id)
	{
		$data = $this->mOrder->get_order_by_id($id);
		echo json_encode($data);
	}

	public function view_order($mode)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['mode'] = $mode;
		$this->render_page('menu/staff/view/view_order', $data);
	}

	public function view_order_agent($mode)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['mode'] = $mode;
		$this->render_page('menu/agent/view/view_order', $data);
	}

	public function view_order_individu($mode)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['mode'] = $mode;
		$this->render_page('menu/individu/view/view_order', $data);
	}

	public function view_order_detail($idsha)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['data'] = $this->mOrder->get_order_by_idsha($idsha);
		$this->render_page('menu/staff/view/view_detail_order', $data);
	}

	public function edit_order($idsha)
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['order'] = $this->mOrder->get_order_by_idsha($idsha);
		$data['type'] = $this->mEform->get_all_type();
		$this->render_page('menu/staff/edit/edit_order', $data);
	}

	public function update_order_number()
	{
		$id = $this->input->post('id', true);
		$order_num = $this->input->post('order_num', true);

		$get_eform = $this->db->query("SELECT * FROM tb_eform WHERE id = '$id'")->row();
		$cek_level = $this->mUser->get_level_by_kode($get_eform->name_sales)->level;
		if ($cek_level == 'agent') {
			$nama = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$get_eform->name_sales'")->row();
			$new_nama = $nama->nama_perniagaan;
		} elseif ($cek_level == 'individu') {
			$nama = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$get_eform->name_sales'")->row();
			$new_nama = $nama->nama;
		}

		$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								Please Be inform that your Customer <b>" . $get_eform->name_customer . "</b> / <b>" . $order_num . "</b> (Order Number) is under processing.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";
		// Send to email
		$this->send_mail_status($nama->email, $txt_email);

		$this->db->where('id', $id);
		$this->db->update('tb_eform', array(
			'order_number' => $order_num,
			'status' => 'processing',
			'edit_by' => $this->session->hash,
			'edit_date' => date('Y-m-d H:i:s')
		));

		$this->db->insert('tb_history', array(
			'note_1' => 'Update order number',
			'note_2' => 'Change to ' . $order_num,
			'status' => 'update',
			'order_number' => $order_num,
			'id' => $id,
			'user_input' => $this->session->hash,
			'date_input' => date('Y-m-d H:i:s')
		));

		$this->session->set_flashdata('success', 'Update order number successfully..');
		redirect('edit-order/' . sha1($id));
	}

	public function update_comission()
	{
		$id = $this->input->post('id', true);
		$comission = $this->input->post('comission', true);
		$mode = $this->input->post('mode', true);
		$this->db->where('id', $id);
		$this->db->update('tb_eform', array(
			'commision' => $comission,
			'edit_by' => $this->session->hash,
			'edit_date' => date('Y-m-d H:i:s')
		));

		$this->db->insert('tb_history', array(
			'note_1' => 'Update comission',
			'note_2' => 'Add comission' . $comission,
			'status' => 'add-comission',
			'id' => $id,
			'user_input' => $this->session->hash,
			'date_input' => date('Y-m-d H:i:s')
		));
		$this->session->set_flashdata('success', 'Update comission successfully..');
		redirect('view-order/' . $mode);
	}
	
	public function update_refferal_staff()
	{
		$id = $this->input->post('id', true);
		$refferral_staff = $this->input->post('refferral_staff', true);
		$staff_commission = $this->input->post('staff_commission', true);
		$mode = $this->input->post('mode', true);
		$commision 		= $this->db->query("SELECT * FROM tb_eform WHERE id = '$id'")->row();
		$total_commisionn	= $commision->commision;
		$this->db->where('id', $id);
		$this->db->update('tb_eform', array(
			'refferral_staff' => $refferral_staff,
			'staff_commission' => $total_commisionn,
			'edit_by' => $this->session->hash,
			'edit_date' => date('Y-m-d H:i:s')
		));

		$this->db->insert('tb_history', array(
			'note_1' => 'Update referral staff commision',
			'note_2' => 'Add referral staff commision' . $refferral_staff . $total_commisionn ,
			'status' => 'add-referral-staff',
			'id' => $id,
			'user_input' => $this->session->hash,
			'date_input' => date('Y-m-d H:i:s')
		));
		$this->session->set_flashdata('success', 'Update referral staff commisio successfully..');
		redirect('view-order/' . $mode);
	}

	public function save_order($mode = null)
	{
		$hash = $this->session->hash;
		$id_kedai = $this->session->id_kedai;
		$id = $this->input->post('id', true);
		$code = $this->input->post('code', true);
		$email = $this->input->post('email', true);

		$get_user = $this->db->query("SELECT * FROM tb_user WHERE `hash` = '$hash'")->row();
		$cek_level = $this->mUser->get_level_by_kode($code)->level;
		$get_eform = $this->db->query("SELECT * FROM tb_eform WHERE id = '$id'")->row();
		if ($cek_level == 'agent') {
			$nama = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$code'")->row();
			$cek_send_email = $nama->notif_order_email;
			$new_nama = $nama->nama_perniagaan;
		} elseif ($cek_level == 'individu') {
			$nama = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$code'")->row();
			$cek_send_email = $nama->notif_order_email;
			$new_nama = $nama->nama;
		}

		if ($mode !== 'paid') {
			switch ($mode) {
				case 'pending':
					$note1 = "Order pending";
					$note2 = null;
					$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								We Have received new application from your referral Link.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";
					if ($cek_send_email == 'Y') {
						// Send to email
						$this->send_mail_status($email, $txt_email);
					}
					break;

				case 'port-full':
					$note1 = "Order Port Full";
					$note2 = null;
					$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								We wish to inform that this customer <b>".strtoupper($get_eform->name_customer)."</b> cannot be served temporary due to Port Full. Please check this customer under Waiting List - port full category.<br>
								Thank you.";
					if ($cek_send_email == 'Y') {
						// Send to email
						$this->send_mail_status($email, $txt_email);
					}
					break;

				case 'upcoming-projects':
					$note1 = "Order Upcoming Projects";
					$note2 = null;
					$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								We wish to inform that this customer <b>".strtoupper($get_eform->name_customer)."</b> cannot be served temporary due to No Infra-Upcoming project. Please check this customer under Waiting List - Upcoming Project category.<br>
								Thank you.";
					if ($cek_send_email == 'Y') {
						// Send to email
						$this->send_mail_status($email, $txt_email);
					}
					break;

				case 'cancel':
					$note1 = "Order cancel";
					$note2 = null;
					break;

				case 'rejected':
					$note1 = "Order rejected";
					$note2 = null;
					break;

				case 'completed':
					$note1 = "Order completed";
					$note2 = null;
					$txt_email = "Hi <b>" . strtoupper($new_nama) . ",</b><br><br>
								Congratulation, <br><br>
								Your Customer <b>" . $get_eform->name_customer . "</b> / <b>" . $get_eform->order_number . "</b> (Order Number) has been completed and we will arrange a payment via bank transfer.<br><br>
								Regards,<br>
								E-Referral<br><br>update-order-number
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";
					if ($cek_send_email == 'Y') {
						// Send to email
						$this->send_mail_status($email, $txt_email);
					}
					break;
			}

			$order_num = $this->db->query("SELECT order_number as no FROM tb_eform where id = '$id'")->row()->no;
			$this->db->where('id', $id);
			$this->db->update('tb_eform', array(
				'status' => $mode,
				'edit_by' => $this->session->hash,
				'edit_date' => date('Y-m-d H:i:s')
			));

			$this->db->insert('tb_history', array(
				'note_1' => $note1,
				'note_2' => $note2,
				'status' => $mode,
				'id' => $id,
				'order_number' => $order_num,
				'user_input' => $this->session->hash,
				'date_input' => date('Y-m-d H:i:s')
			));

			redirect('edit-order/' . sha1($id), 'refresh');
		} elseif ($mode == 'paid') {
			$comission = $this->input->post('comission', true);

			$config['upload_path']          = 'assets/images/order/receipt/';
			$config['allowed_types']        = 'jpeg|jpg|png|pdf';
			$config['max_size']             = 0;
			$config['max_width']            = 6000;
			$config['max_height']           = 6000;
			$config['remove_space']         = TRUE;
			$config['encrypt_name']         = TRUE;
			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('receipt')) {
				$this->db->where('id', $id);
				$this->db->update('tb_eform', array(
					'status' => 'paid',
					'commision' => $comission,
					'edit_by' => $this->session->hash,
					'edit_date' => date('Y-m-d H:i:s')
				));
			} else {
				$upload_data = $this->upload->data();
				$file_name = $upload_data['file_name'];

				$this->db->where('id', $id);
				$this->db->update('tb_eform', array(
					'status' => 'paid',
					'receipt' => $file_name,
					'commision' => $comission,
					'edit_by' => $this->session->hash,
					'edit_date' => date('Y-m-d H:i:s')
				));
			}

			$txt_email = "Hi <b>" . strtoupper($new_nama) . ",</b><br><br>
								We Successfully Transferred <b>RM" . $comission . "</b> to your bank account.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";

			if ($cek_send_email == 'Y') {
				$code = $this->db->query("SELECT name_sales FROM tb_eform WHERE id = '$id'")->row()->name_sales;
				$level = $this->db->query("SELECT `level` FROM tb_user where id_kedai = '$code'")->row()->level;
				if ($level == 'agent') {
					$em = $this->db->query("SELECT email FROM tb_kedai where kode_kedai = '$code'")->row()->email;
				} elseif($level == 'individu') {
					$em = $this->db->query("SELECT email FROM tb_sales where kode_individu = '$code'")->row()->email;
				}
				// Send to email
				$this->send_mail_status($em, $txt_email);
			}

			$this->db->insert('tb_history', array(
				'note_1' => 'Order paid',
				'note_2' => 'Add Commission RM ' . number_format($comission, 2),
				'status' => 'paid',
				'id' => $id,
				'user_input' => $this->session->hash,
				'date_input' => date('Y-m-d H:i:s')
			));

			$this->session->set_flashdata('success', 'Save order PAID successfully..');
			redirect('edit-order/' . sha1($id));
		}
	}

	public function save_edit_order()
	{
		$id = $this->input->post('id', true);
		$status = $this->input->post('addStatusOrder');
		$order_number = $this->input->post('order_number', true);
		$name_customer = $this->input->post('name_customer', true);
		$email = $this->input->post('email', true);
		$remarks = $this->input->post('remarks', true);

		$get_eform = $this->db->query("SELECT * FROM tb_eform WHERE id = '$id'")->row();
		$cek_level = $this->mUser->get_level_by_kode($get_eform->name_sales)->level;
		if ($cek_level == 'agent') {
			$nama = $this->db->query("SELECT * FROM tb_kedai WHERE kode_kedai = '$get_eform->name_sales'")->row();
			$cek_send_email = $nama->notif_order_email;
			$new_nama = $nama->nama_perniagaan;
		} elseif ($cek_level == 'individu') {
			$nama = $this->db->query("SELECT * FROM tb_sales WHERE kode_individu = '$get_eform->name_sales'")->row();
			$cek_send_email = $nama->notif_order_email;
			$new_nama = $nama->nama;
		}

		switch ($status) {
			case 'cancel':
				$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								We are Sorry to inform that Your Customer <b>" . strtoupper($name_customer) . "</b> / <b>" . $order_number . "</b> (Order Number) has been cancel due to <i>" . $remarks . "</i>.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";
				break;

			case 'rejected':
				$txt_email = "Dear <b>" . strtoupper($new_nama) . ",</b><br>
								We are Sorry to inform that Your Customer <b>" . strtoupper($name_customer) . "</b> / <b>" . $order_number . "</b> (Order Number) has been Rejected due to <i>" . $remarks . "</i>.<br><br>
								Regards,<br>
								E-Referral<br><br>
								Url <b>" . base_url() . "</b><br>
								Please visit url to view more detail.";
				break;
		}

		if ($cek_send_email == 'Y') {
			// Send to email
			$this->send_mail_status($nama->email, $txt_email);
		}
		$this->mOrder->save_edit_order();
		$this->session->set_flashdata('success', 'Update data order successfully..');
		redirect('view-order/' . $status);
	}

	public function delete_order($mode, $id)
	{
		$this->db->insert('tb_history', array(
			'note_1' => 'Delete order',
			'status' => 'delete',
			'id' => $id,
			'user_input' => $this->session->hash,
			'date_input' => date('Y-m-d H:i:s')
		));

		$this->db->where('sha1(id)', $id);
		$this->db->delete('tb_eform');
		$this->session->set_flashdata('success', 'Delete data order successfully..');
		redirect('view-order/' . $mode);
	}

	public function save_package()
	{
		$this->mOrder->save_package();
		$this->session->set_flashdata('success', 'Save data package successfully..');
		redirect('set-package', 'refresh');
		// redirect('Setting/view_package', 'refresh');
	}

	public function update_package()
	{
		$id = $this->input->post('id', true);
		$this->db->where('id_pkg', $id);
		$this->db->update('tb_package', array(
			'type' => $this->input->post('type', true),
			'package' => $this->input->post('package', true),
			'date_input' => date('Y-m-d H:i:s')
		));
		$this->session->set_flashdata('success', 'Update data package successfully..');
		redirect('set-package');
	}

	public function send_mail_status_old()
	{
		// PHPMailer object
		$response = false;
		$mail = new PHPMailer();
		// SMTP configuration
		$mail->isSMTP();
		$mail->Host     = 'mail.internet-unifi.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
		$mail->SMTPAuth = true;
		$mail->Username = 'referral@internet-unifi.com'; // user email
		$mail->Password = 'F@rid012'; // password email
		$mail->SMTPSecure = 'ssl';
		$mail->Port     = 465;
		$mail->setFrom('referral@internet-unifi.com', 'eReferral'); // user email
		$mail->addReplyTo('referral@internet-unifi.com', ''); //user email
		// Add a recipient
		$mail->addAddress('atokkecenk@gmail.com'); //email tujuan pengiriman email
		// Email subject
		$mail->Subject = 'Notification E-referral'; //subject email
		// Set email format to HTML
		$mail->isHTML(true);
		// Email body content
		$mailContent = "<p>Percobaan mengirim email.</p>"; // isi email
		$mail->Body = $mailContent;

		// Send email
		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}
	}

	public function send_mail_status($to_email, $message)
	{
		$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
		$this->load->library('email');
		$config = array();
		$config['protocol']     = 'smtp';
		$config['smtp_host']    = $smtp->host;
		$config['smtp_user']    = $smtp->username;
		$config['smtp_pass']    = $smtp->password;
		$config['smtp_crypto']  = 'ssl';
		$config['mailtype']     = 'html'; // text or html
		$config['charset']          = 'utf-8';
		$config['send_multipart']   = FALSE;
		$config['wordwrap']         = TRUE;
		$config['priority']         = 1;
		$config['smtp_timeout']     = 15;
		$config['smtp_port']        = $smtp->port;
		$config['crlf']             = "\r\n";
		$config['newline']          = "\r\n";
		$config['validate']         = FALSE;
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		
		$this->email->from($smtp->from, $smtp->sender_name);
		$this->email->to($to_email);
		$this->email->subject($smtp->subject);
		$this->email->message($message);
		//Send mail
		$this->email->send();
	}

	public function cek_send()
	{
		$smtp = $this->db->select('*')->from('tb_setting_smtp')->get()->row();
		// PHPMailer object
		$response = false;
		$mail = new PHPMailer();
		// SMTP configuration
		$mail->isSMTP();
		$mail->Host     = $smtp->host; //sesuaikan sesuai nama domain hosting/server yang digunakan
		$mail->SMTPAuth = true;
		$mail->Username = $smtp->username; // user email
		$mail->Password = $smtp->password; // password email
		$mail->SMTPSecure = 'ssl';
		$mail->Port     = $smtp->port;
		$mail->setFrom($smtp->from, $smtp->sender_name); // user email
		$mail->addReplyTo($smtp->reply_to, $smtp->sender_name); //user email
		// Add a recipient
		$mail->addAddress('atokkecenk@gmail.com'); //email tujuan pengiriman email
		// Email subject
		$mail->Subject = $smtp->subject; //subject email
		// Set email format to HTML
		$mail->isHTML(true);
		// Email body content
		$mailContent = "<p>Referral Mail Yes/No</p>"; // isi email
		$mail->Body = $mailContent;

		// Send email
		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}
	}
}
