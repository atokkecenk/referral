<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maintenance extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->view('maintenance');
  }

  public function on_sistem()
  {
    $this->load->view('maintenance_on');
  }

  public function update_on_sistem()
  {
    $num1 = $this->input->post('num1');
    $num2 = $this->input->post('num2');
    $quest = $this->input->post('questions');

    $cek = $this->db->query("SELECT maintenance FROM tb_setting WHERE id = 1 ")->row()->maintenance;

    $jml = $num1 + $num2;
    if ($jml !== (int)$quest) {
      $data['msg'] = '<p style="color: red; margin-bottom: 2em; "><b>Sorry</b>, cek your question !!</p>';
      $this->load->view('maintenance_on', $data);
    } elseif ($jml == $quest) {
      if ($cek == 0) {
        $sts = 1;
      } else {
        $sts = 0;
      }

      $this->db->where('id', 1);
      $this->db->update('tb_setting', array('maintenance' => $sts));
      $data['msg'] = '<p style="color: green; margin-bottom: 2em;">Congratulations, system is <b>On</b>.</p>';
      $data['back'] = 'yes';
      $this->load->view('maintenance_on', $data);
    }
  }

  public function update_off_sistem()
  {
    $this->db->where('id', 1);
    $this->db->update('tb_setting', array('maintenance' => 1));
    $this->session->sess_destroy();
    redirect(base_url());
  }
}
