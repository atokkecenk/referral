<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Announcement extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}
	}

	public function get_all_announcement()
	{
		$list = $this->mAnnc->get_datatables();
		$data = array();
		$no = @$_POST['start'];

		foreach ($list as $item) {
			switch ($item->publish) {
				case 'publish':
					$view = '<a href="#" class="modalDraftAnnc" data-id="' . sha1($item->id) . '" data-toggle="modal" data-target="#modalDraftAnnc" style="color: #9591a5;" title="Click To Draft"><i class="feather icon-eye m-r-5" style="color: #04a9f5;"></i>Publish</a>';
					break;

				case 'draft':
					$view = '<a href="#" class="modalPublishAnnc" data-id="' . sha1($item->id) . '" data-toggle="modal" data-target="#modalPublishAnnc" style="color: #9591a5;" title="Click To Publish"><i class="feather icon-slash m-r-5" style="color: #dc3545;"></i>Draft</a>';
					break;
			}

			switch ($item->masa) {
				case 'U':
					$new_masa = 'Unlimited';
					$date = null;
					break;

				case 'D':
					$new_masa = 'By Date';
					$date = $item->new_start . ' until ' . $item->new_end;
					break;
			}

			if ($item->foto == '') {
				$image = null;
			} else {
				if ($item->tipe == 'video') {
					$image = "<video class='video' controls>
						<source src='" . base_url('assets/images/announcement/' . $item->foto) . "' type='video/mp4'>
						Your browser does not support the video tag.
						</video>
						";
				} else {
					$image = '<p><img src="' . base_url('assets/images/announcement/' . $item->foto) . '" style="width: 100%;"></p>';
				}
			}

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->subject;
			$row[] = '<div class="alert alert-danger">' . $image . '<p>' . $item->text . '</p></div>';
			$row[] = $view;
			$row[] = $new_masa;
			$row[] = $date;
			$row[] = '<button type="button" class="btn btn-danger btn-delete-modif modalDeleteAnnc" data-toggle="modal" data-target="#modalDeleteAnnc" data-id="' . sha1($item->id) . '" title="Delete"><i class="feather icon-trash" style="margin: 0 !important;"></i></a>';
			$data[] = $row;
		}

		$datajson = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->mAnnc->count_all(),
			"recordsFiltered" => $this->mAnnc->count_filtered(),
			"data" => $data,
		);

		// output to json format
		echo json_encode($datajson);
	}

	public function add()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$this->render_page('menu/staff/add/add_announcement', $data);
	}

	public function add_video()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$this->render_page('menu/staff/add/add_announcement_video', $data);
	}


	public function view()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$this->render_page('menu/staff/view/view_announcement', $data);
	}

	public function save()
	{
		$text = $this->input->post('text');
		$config['upload_path']          = 'assets/images/announcement/';
		$config['allowed_types']        = 'jpeg|jpg|png';
		$config['max_size']             = 0;
		$config['max_width']            = 6000;
		$config['max_height']           = 6000;
		$config['remove_space']         = TRUE;
		$config['encrypt_name']         = TRUE;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('image')) {

			$cek = $this->db->select('*')
				->from('tb_user')
				->where('active', 'Y')
				// ->where('verify', 'Y')
				->get()->result();

			foreach ($cek as $val) {
				$this->db->insert('tb_history', array(
					'note_1' => 'Add Announcement',
					'note_2' => $text,
					'status' => 'N',
					'id' => $val->id,
					'user_input' => $this->session->hash,
					'date_input' => date('Y-m-d H:i:s'),
				));
			}

			$this->mAnnc->save($file_name = '');
			$this->session->set_flashdata('success', 'Save announcement successfully..');
			redirect('add-announcement');
		} else {
			$this->upload->do_upload('image');
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];

			$cek = $this->db->select('*')
				->from('tb_user')
				->where('active', 'Y')
				// ->where('verify', 'Y')
				->get()->result();

			foreach ($cek as $val) {
				$this->db->insert('tb_history', array(
					'note_1' => 'Add Announcement With Image',
					'note_2' => $text,
					'status' => 'N',
					'id' => $val->id,
					'user_input' => $this->session->hash,
					'date_input' => date('Y-m-d H:i:s'),
				));
			}

			$this->mAnnc->save($file_name);
			$this->session->set_flashdata('success', 'Save announcement successfully..');
			redirect('add-announcement');
		}
	}

	public function save_video()
	{
		$text = $this->input->post('text');
		$config['upload_path']          = 'assets/images/announcement/';
		$config['allowed_types']        = 'mp4';
		$config['max_size']             = 0;
		$config['encrypt_name']         = TRUE;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('image')) {

			$cek = $this->db->select('*')
				->from('tb_user')
				->where('active', 'Y')
				// ->where('verify', 'Y')
				->get()->result();

			foreach ($cek as $val) {
				$this->db->insert('tb_history', array(
					'note_1' => 'Add Announcement',
					'note_2' => $text,
					'status' => 'N',
					'id' => $val->id,
					'user_input' => $this->session->hash,
					'date_input' => date('Y-m-d H:i:s'),
				));
			}

			$this->mAnnc->save($file_name = '');
			$this->session->set_flashdata('success', 'Save announcement successfully..');
			redirect('add-announcement-video');
		} else {
			$this->upload->do_upload('image');
			$upload_data = $this->upload->data();
			$file_name = $upload_data['file_name'];

			$cek = $this->db->select('*')
				->from('tb_user')
				->where('active', 'Y')
				// ->where('verify', 'Y')
				->get()->result();

			foreach ($cek as $val) {
				$this->db->insert('tb_history', array(
					'note_1' => 'Add Announcement With Video',
					'note_2' => $text,
					'status' => 'N',
					'id' => $val->id,
					'user_input' => $this->session->hash,
					'date_input' => date('Y-m-d H:i:s'),
				));
			}
			$this->mAnnc->save($file_name);
			$this->session->set_flashdata('success', 'Save announcement successfully..');
			redirect('add-announcement-video');
		}
	}

	public function view_announcement()
	{
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['announcement'] = $this->mDash->get_all_announcement_active();
		$this->render_page('announcement', $data);
	}

	public function update_announcement_status($mode)
	{
		$id = $this->input->post('id');
		$this->db->where(array('sha1(id)' => $id));
		$this->db->update('tb_announcement', array('publish' => $mode));
		$this->session->set_flashdata('success', 'Set status announcement successfully..');
		redirect('view-announcement');
	}

	public function cek_ins_announcement()
	{
		$id_sess = $this->session->id;
		$this->db->where('id', $id_sess);
		$this->db->like('note_1', 'Add Announcement', 'after');
		$this->db->update('tb_history', array(
			'status' => 'Y'
		));
	}
}
