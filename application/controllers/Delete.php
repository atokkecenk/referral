<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Delete extends MY_Controller
{
    public function delete_filter()
    {
        $mode = $this->input->post('mode');
        switch ($mode) {
            case 'package':
                $whr = 'id_pkg';
                $key = $this->input->post('key');
                $table = 'tb_package';
                $rdr = 'set-package';
                break;

            case 'sales':
                $whr = 'id';
                $key = $this->input->post('key');
                $kode = $this->db->get_where('tb_sales', array('id' => $key))->row()->kode_individu;
                $this->db->where('id_kedai', $kode);
                $this->db->update('tb_user', array('active' => 'N'));
                $table = 'tb_sales';
                $rdr = 'view-sales';
                break;

            case 'kedai':
                $whr = 'id';
                $key = $this->input->post('key');
                $kode = $this->db->get_where('tb_kedai', array('id' => $key))->row()->kode_kedai;
                $this->db->where('id_kedai', $kode);
                $this->db->update('tb_user', array('active' => 'N'));
                $table = 'tb_kedai';
                $rdr = 'view-kedai';
                break;

            case 'order':
                $whr = 'id';
                $key = $this->input->post('key');
                $table = 'tb_eform';
                $rdr = 'view-order/';
                break;

            case 'keluhan':
                $whr = 'id';
                $key = $this->input->post('key');
                $table = 'tb_keluhan';
                $rdr = 'keluhan';
                break;

            case 'flyer':
                $whr = 'id';
                $key = $this->input->post('key');
                $table = 'tb_flyer';
                $rdr = 'flyer';
                break;
        }

        if ($mode == 'flyer') {
            $sts = $this->input->post('status');
            $this->db->where($whr, $key);
            $this->db->update($table, ['active' => $sts]);
            $this->session->set_flashdata('success', 'Update data template successfully..');
            redirect($rdr, 'refresh');
        } else {
            $this->db->where($whr, $key);
            $this->db->delete($table);
            $this->session->set_flashdata('success', 'Delete data successfully..');
            if ($this->input->post('rdr') !== '') {
                redirect($rdr . $this->input->post('rdr'));
            } else {
                redirect($rdr, 'refresh', null);
            }
        }
    }
}
