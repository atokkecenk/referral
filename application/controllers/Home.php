<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$cek = $this->mSett->cek_maintenance()->maintenance;
		if ($cek == '1') {
			$this->session->sess_destroy();
			redirect('maintenance');
		}
	}

	public function dashboard()
	{
		$hash = $this->session->hash;
		$id = $this->db->query("SELECT id_kedai FROM tb_user WHERE `hash` = '$hash'")->row()->id_kedai;
		$data['uri1'] = $this->uri->segment(1);
		$data['uri2'] = $this->uri->segment(2);
		$data['level'] = $this->session->level;
		$data['cek_verify'] = $this->mDash->cek_verify($id)->row();
		$data['data'] = $this->mDash->get_data_kedai($this->session->id_kedai)->row();
		$data['data2'] = $this->mDash->get_data_individu($this->session->id_kedai)->row();
		$data['data_user'] = $this->mDash->get_data_user($this->session->id)->row();
		$data['comm_complete'] = $this->mDash->get_complete_staff('completed')->row()->sum_commission;
		$data['comm_paid'] = $this->mDash->get_complete_staff('paid')->row()->sum_commission;
		// dashboard panel
		$data['pending'] = $this->mDash->get_order_by_status('pending');
		$data['portfull'] = $this->mDash->get_order_by_status('port-full');
		$data['upprojects'] = $this->mDash->get_order_by_status('upcoming-projects');
		$data['processing'] = $this->mDash->get_order_by_status('processing');
		$data['paid'] = $this->mDash->get_order_by_status('paid');
		$data['cancel'] = $this->mDash->get_order_by_status('cancel');
		$data['rejected'] = $this->mDash->get_order_by_status('rejected');
		$data['completed'] = $this->mDash->get_order_by_status('completed');
		$data['all_order'] = $this->mDash->get_all_order_by_status();

		// dashboard no staff
		$data['ns_pending'] = $this->mDash->get_order_by_status_no_staff('pending');
		$data['ns_portfull'] = $this->mDash->get_order_by_status_no_staff('port-full');
		$data['ns_upprojects'] = $this->mDash->get_order_by_status_no_staff('upcoming-projects');
		$data['ns_processing'] = $this->mDash->get_order_by_status_no_staff('processing');
		$data['ns_paid'] = $this->mDash->get_order_by_status_no_staff('paid');
		$data['ns_cancel'] = $this->mDash->get_order_by_status_no_staff('cancel');
		$data['ns_rejected'] = $this->mDash->get_order_by_status_no_staff('rejected');
		$data['ns_completed'] = $this->mDash->get_order_by_status_no_staff('completed');
		$data['ns_all_order'] = $this->mDash->get_all_order_by_status_no_staff();
		$data['ns_comm_complete'] = $this->mDash->get_complete_no_staff('completed')->row()->sum_commission;
		$data['ns_comm_paid'] = $this->mDash->get_complete_no_staff('paid')->row()->sum_commission;

		$data['count_kedai'] = $this->mDash->count_kedai();
		$data['count_individu'] = $this->mDash->count_individu();
		$this->render_page('dashboard', $data);
	}

	public function download($filename)
	{
		force_download('assets/images/qr-code/' . $filename, NULL);
	}

	public function edit_password()
	{
		$id = $this->input->post('id');
		$password = $this->input->post('password');
		$this->db->where('id', $id);
		$this->db->update('tb_user', array(
			'password' => md5($password)
		));
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function edit_usernamepassword()
	{
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$new_username = str_replace(' ', '', $username);
		$password = $this->input->post('password');
		$this->db->where('id', $id);
		$this->db->update('tb_user', array(
			'username' => $new_username,
			'password' => md5($password)
		));

		$cek = $this->db->query("SELECT `level` FROM tb_user WHERE id = '$id' ")->row()->level;
		if ($cek == 'staff') {
			$this->session->sess_destroy();
			redirect(base_url());
		} else {
			redirect('view-user');
		}
	}

	public function edit_username()
	{
		$id = $this->input->post('id');
		$username = $this->input->post('username');
		$new_username = str_replace(' ', '', $username);
		$this->db->where('id', $id);
		$this->db->update('tb_user', array(
			'username' => $new_username
		));
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function delete($where)
	{
		$level = $this->session->level;
		$id = $this->input->post('id');

		if ($where == 'announcement') {
			$data = array('status' => 'N');
			$where2 = 'sha1(id)';
			$table = 'tb_announcement';
			$rdr = 'view-announcement';
		}

		$this->db->where($where2, $id);
		$this->db->update($table, $data);
		$this->session->set_flashdata('success', 'Delete data successfully..');
		redirect($rdr);
	}
}
