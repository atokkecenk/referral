const flashdata = $(".flash-data").data("flashdata");
const flashtipe = $(".flash-data").data("flashtipe");

if (flashdata && flashtipe == "success") {
	sweetAlert({
		title: "Success",
		text: flashdata,
		type: "success",
		showConfirmButton: false,
		timer: 2600,
	});
} else if (flashdata && flashtipe == "error") {
	sweetAlert({
		title: "Error",
		text: flashdata,
		type: "error",
		showConfirmButton: false,
		timer: 2600,
	});
}

// $("#date, #startDate, #endDate").datepicker({
// 	format: "dd/mm/yyyy",
// 	autoclose: true,
// 	todayHighlight: true,
// 	todayBtn: "linked",
// });

$("#btnCopyURL").click(function () {
	var copyText = document.getElementById("source");
	copyText.select();
	copyText.setSelectionRange(0, 99999); /* For mobile devices */
	document.execCommand("copy");
});

function showPass() {
	var pass = document.getElementById("password");
	if (pass.type === "password") {
		pass.type = "text";
		$("#passIcon").html('<i class="feather icon-eye"></i>');
	} else {
		pass.type = "password";
		$("#passIcon").html('<i class="feather icon-eye-off"></i>');
	}
}
$("#passIcon").html('<i class="feather icon-eye-off"></i>');

$(document).ready(function () {
	$(".js-example-basic-single").select2();

	$("#startDate, #endDate").datepicker({
		format: "dd/mm/yyyy",
		autoclose: true,
		todayHighlight: true,
		todayBtn: "linked",
	});

	$("#masa").on("change", function () {
		var masa = $(this).val();
		if (masa == "D") {
			$("#setByDate1, #setByDate2").show();
		} else {
			$("#setByDate1, #setByDate2").hide();
		}
	});

	$("#viewImageSales, #viewImageKedai")
		.attr("src", "assets/images/noimage.png")
		.attr("width", 150);

	$(".imageSales").change(function () {
		imageSales(this);
		$("#messageImageSales").html(
			"<i class='feather icon-image' style='color: #04a9f5;'></i> <font style='font-weight: bold;'>Image ready to upload..</font>"
		);
	});

	function imageSales(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$("#viewImageSales").attr("src", e.target.result).attr("width", 150);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".imageKedai").change(function () {
		imageKedai(this);
		$("#messageImageKedai").html(
			"<i class='feather icon-image' style='color: #04a9f5;'></i> <font style='font-weight: bold;'>Image ready to upload..</font>"
		);
	});

	function imageKedai(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$("#viewImageKedai").attr("src", e.target.result).attr("width", 150);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(".imageAnnc").change(function () {
		imageAnnc(this);
	});

	function imageAnnc(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$("#messageImageAnnouncement").html(
					'<img src="' + e.target.result + '" width="300px" style="border: 1px solid #3f4d67; margin-bottom: 10px;">'
				);
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on("click", ".modalDraftAnnc", function () {
		var id = $(this).data("id");
		$("#idDraftAnnc").val(id);
	});

	$(document).on("click", ".modalPublishAnnc", function () {
		var id = $(this).data("id");
		$("#idPublishAnnc").val(id);
	});

	$(document).on("click", ".modalDeleteAnnc", function () {
		var id = $(this).data("id");
		$("#idAnnc").val(id);
	});

});
