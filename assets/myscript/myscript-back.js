const flashdata = $(".flash-data").data("flashdata");

const flashtipe = $(".flash-data").data("flashtipe");

if (flashdata && flashtipe == "success") {
  sweetAlert({
    title: "Success",

    text: flashdata,

    type: "success",

    showConfirmButton: false,

    timer: 3000,
  });
} else if (flashdata && flashtipe == "error") {
  sweetAlert({
    title: "Error",

    text: flashdata,

    type: "error",

    showConfirmButton: false,

    timer: 3000,
  });
}

// $("#date, #startDate, #endDate").datepicker({

// 	format: "dd/mm/yyyy",

// 	autoclose: true,

// 	todayHighlight: true,

// 	todayBtn: "linked",

// });

$("#btnCopyURL").click(function () {
  var copyText = document.getElementById("source");

  copyText.select();

  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  document.execCommand("copy");
});

$(document).ready(function () {
  $(".js-example-basic-single").select2();

  $("#startDate, #endDate").datepicker({
    format: "dd/mm/yyyy",

    autoclose: true,

    todayHighlight: true,

    todayBtn: "linked",
  });

  $("#masa").on("change", function () {
    var masa = $(this).val();

    if (masa == "D") {
      $("#setByDate1, #setByDate2").show();
    } else {
      $("#setByDate1, #setByDate2").hide();
    }
  });

  $("#viewImageSales, #viewImageKedai, #viewImageIndividu")
    .attr("src", "assets/images/noimage.png")

    .attr("width", 150);

  $(".imageSales").change(function () {
    imageSales(this);

    $("#messageImageSales").html(
      "<i class='feather icon-image' style='color: #04a9f5;'></i> <font style='font-weight: bold;'>Image ready to upload..</font>"
    );
  });

  $(document).on("click", ".modalDraftAnnc", function () {
    var id = $(this).data("id");

    $("#idDraftAnnc").val(id);
  });

  $(document).on("click", ".modalPublishAnnc", function () {
    var id = $(this).data("id");

    $("#idPublishAnnc").val(id);
  });

  $(document).on("click", ".modalDeleteAnnc", function () {
    var id = $(this).data("id");

    $("#idAnnc").val(id);
  });

  $("#tbl-view-sales").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,
    sorting: [[0, "DESC"]],

    ajax: {
      url: "Sales/get_all_sales",

      type: "POST",
    },

    columnDefs: [
      {
        targets: [0, 7, 10],

        className: "text-center",
      },

      {
        orderable: false,

        targets: [10],
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $("#tbl-view-kedai").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,
    sorting: [[0, "DESC"]],

    ajax: {
      url: "Kedai/get_all_kedai",

      type: "POST",
    },

    columnDefs: [
      {
        targets: [0, 9, 12],

        className: "text-center",
      },

      {
        orderable: false,

        targets: [12],
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $("#tbl-view-announcement").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,
    sorting: [[0, "DESC"]],

    ajax: {
      url: "Announcement/get_all_announcement",

      type: "POST",
    },

    columnDefs: [
      {
        targets: [0],

        className: "text-center",
      },

      {
        orderable: false,

        targets: [6],
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  var mode_view = $("#modeViewOrder").val();

  $("#tbl-view-order").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,

    sorting: [[0, "DESC"]],

    ajax: {
      url: "../Order/get_order_log_staff/" + mode_view,

      type: "POST",
    },

    columnDefs: [
      {
        targets: [6],

        className: "text-right",
      },

      {
        targets: [0, 10],

        className: "text-center",
      },

      {
        orderable: false,

        targets: [10],
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $("#tbl-view-order-agent").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,

    sorting: [[0, "DESC"]],

    ajax: {
      url: "../Order/get_order_log_agent/" + mode_view,

      type: "POST",
    },

    columnDefs: [
      {
        targets: [6],

        className: "text-right",
      },

      {
        targets: [0],

        className: "text-center",
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $("#tbl-view-order-individu").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,

    sorting: [[0, "DESC"]],

    ajax: {
      url: "../Order/get_order_log_individu/" + mode_view,

      type: "POST",
    },

    columnDefs: [
      {
        targets: [6],

        className: "text-right",
      },

      {
        targets: [0],

        className: "text-center",
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $("#tbl-view-user").DataTable({
    responsive: true,

    autoWidth: false,

    processing: true,

    serverSide: true,

    sorting: [[0, "DESC"]],

    ajax: {
      url: "get-all-user",

      type: "POST",
    },

    columnDefs: [
      {
        targets: [0, 4, 7],

        className: "text-center",
      },

      {
        orderable: false,

        targets: [6],
      },
    ],

    language: {
      zeroRecords: "No Data Found !!",
    },
  });

  $(document).on("keyup", ".numberIc, .kadPengenal", function () {
    var nilai = $(this).val();

    if (nilai.length == 6 || nilai.length == 9) {
      this.value += "-";
    }
  });

  $(document).on("click", ".btn-delete-user", function () {
    var id = $(this).data("id");
    var code = $(this).data("code");
    $(".idUser").val(id);
    $(".idCode").val(code);
  });

  $(document).on("click", ".btn-reset-pass", function () {
    var id = $(this).data("id");
    $(".idUser").val(id);
  });

  $(document).on("click", ".btn-view-detail-user", function () {
    var color;

    var id = $(this).data("id");

    var mode = $(this).data("mode");

    $.ajax({
      url: "../get-order-by-id/" + id,

      type: "GET",

      dataType: "JSON",

      success: function (data) {
        switch (mode) {
          case "pending":
            color = "#ffeb00fa";

            break;

          case "port-full":
            color = "#007bff";
            break;

          case "upcoming-projects":
            color = "#007bff";
            break;

          case "processing":
            color = "#748892";

            break;

          case "paid":
            color = "#1de9b6";

            break;

          case "cancel":
            color = "#f44236";

            break;

          case "rejected":
            color = "#37474f";

            break;

          case "completed":
            color = "#3ebfea";

            break;
        }

        $(".idDetailOrder").val(data.id);

        // $(".codeDetailOrder").val(data.name_sales);

        $("#ordernumDetailOrder").val(data.order_number);

        $("#comissionDetailOrder").val(data.commision);
        
        //$("#comissionRefferralStaff").val(data.refferral_staff);

        $(".modeviewDetailOrder").val(mode_view);

        $("#extDetailOrder").html(
          "<table class='table' style='margin-top: 0px;'><thead><tr><th width='33%'>Customer Name</th><th width='2%'>:</th><th width='65%' style='font-weight: normal;'> " +
            data.name_customer +
            "</th></tr><tr><th width='33%'>Package</th><th width='2%'>:</th><th width='65%' style='font-weight: normal;'> " +
            data.name_package +
            "</th></tr><tr><th width='33%'>Installation Date</th><th width='2%'>:</th><th width='65%' style='font-weight: normal;'>" +
            data.new_install +
            "</th></tr><tr><th width='33%'>Submitted Date</th><th width='2%'>:</th><th width='65%' style='font-weight: normal;'>" +
            data.new_input +
            "</th></tr><tr><th width='33%'>Status</th><th width='2%'>:</th><th width='65%' style='font-weight: normal;'> <i class='fas fa-circle f-10 m-r-10' style='color: " +
            color +
            ";'></i> " +
            data.status.toUpperCase() +
            "</th></tr></thead><tbody></tbody></table><a href='../edit-order/" +
            data.id_sha +
            "' class='btn btn-danger btn-block'><i class='feather icon-edit'></i>Edit Order</a>"
        );
      },

      error: function (jqXHR, textStatus, errorThrown) {
        alert("Error, get data from ajax !!");
      },
    });
  });

  $(document).on("click", ".btn-status-order", function () {
    var txt;

    var url;

    var mode = $(this).data("mode");

    var email = $(".emailNotifOrder").val();

    // $("#addStatusOrder").val(mode);

    switch (mode) {
      case "pending":
        url = "../save-order/pending";

        txt = "Set order PENDING successfully..";

        break;

      case "port-full":
        url = "../save-order/port-full";
        txt = "Set order PORT FULL successfully..";
        break;

      case "upcoming-projects":
        url = "../save-order/upcoming-projects";
        txt = "Set order UPCOMING PROJECTS successfully..";
        break;

      case "processing":
        txt =
          "Set order status PROCESSING successfully\nPlease input remarks !!";

        var id = $(this).data("id");

        var ordernum = $(this).data("ordernum");

        $(".idDetailOrder").val(id);

        $("#ordernumDetailOrder").val(ordernum);

        break;

      case "paid":
        txt = "Set order status PAID successfully..";

        var id = $(this).data("id");

        var code = $(this).data("code");
        var comms = $(this).data("comms");

        $(".idDetailOrder").val(id);

        $(".codeDetailOrder").val(code);
        $(".commsDetailOrder").val(comms);

        break;

      case "cancel":
        url = "../save-order/cancel";

        txt = "Set order CANCEL successfully..";

        break;

      case "rejected":
        url = "../save-order/rejected";

        txt = "Set order REJECTED successfully..";

        break;

      case "completed":
        url = "../save-order/completed";

        txt = "Set order COMPLETED successfully..";

        break;
    }

    if (
      mode == "pending" ||
      mode == "port-full" ||
      mode == "upcoming-projects" ||
      mode == "cancel" ||
      mode == "rejected" ||
      mode == "completed"
    ) {
      var id = $(this).data("id");

      var code = $(this).data("code");

      $.ajax({
        type: "POST",

        url: url,

        data: {
          id: id,

          code: code,

          email: email,
        },

        beforeSend: function () {
          if (
            mode == "pending" ||
            mode == "port-full" ||
            mode == "upcoming-projects" ||
            mode == "completed"
          ) {
            sweetAlert({
              title: "Loading",

              text: "Please wait, process sending to email..",

              type: "info",

              showConfirmButton: false,
            });
          } else if (mode == "cancel" || mode == "rejected") {
            sweetAlert({
              title: "Loading",

              text:
                "Please insert remarks coloumn after this and click button Save..",

              type: "info",

              showConfirmButton: false,

              timer: 2600,
            });
          }
        },

        success: function () {
          sweetAlert({
            title: "Success",

            text: txt,

            type: "success",

            showConfirmButton: false,

            timer: 2600,
          });

          $("#addStatusOrder").val(mode);

          if (
            mode == "cancel" ||
            mode == "port-full" ||
            mode == "upcoming-projects" ||
            mode == "rejected"
          ) {
            location.reload();
          } else if (mode == "completed") {
            location.href = "../view-order/" + mode;
          }
        },
      });

      return false;
    }
  });

  $(document).on("change", ".switchNotif", function () {
    var code = $(this).data("code");

    $.ajax({
      type: "POST",

      url: "set-notif",

      data: {
        code: code,
      },

      success: function () {
        sweetAlert({
          title: "Success",

          text: "Set setting notification successfully..",

          type: "success",

          showConfirmButton: false,

          timer: 2600,
        });
      },
    });

    return false;
  });

  // $(
  //   "#tbl-view-code, #tbl-dashboard-kedai, #tbl-dashboard-individu, #tbl-view-package"
  // ).DataTable();

  var id_daerah = $(".id_daerah").val();
  $("#editDaerah").attr("name", "daerah");
  $(".negeri").change(function () {
    var _url;
    var id = $(this).val();
    $("#editDaerah").hide();
    $("#editDaerah").attr("name", "");

    if (id_daerah == null || id_daerah == "") {
      _url = "get-daerah";
    } else {
      _url = "../get-daerah";
    }

    $.ajax({
      url: _url,

      method: "POST",

      data: {
        id: id,
      },

      async: true,

      dataType: "json",

      success: function (data) {
        var html = "";

        var i;

        html += '<select class="form-control" name="daerah">';

        html += '<option value = "" > Nothing Selected </option>';

        for (i = 0; i < data.length; i++) {
          html +=
            "<option value=" +
            data[i].id_daerah +
            " >" +
            data[i].nama +
            "</option>";
        }

        html += "</select>";

        $(".daerah").html(html);
      },
    });

    return false;
  });

  if (id_daerah == null || id_daerah == "") {
    $(".daerah").html(
      '<select class="form-control"><option value = "" > Nothing Selected </option></select>'
    );
  }

  var cek = document.getElementById("addStatusOrder").value;

  if (cek == "processing" || cek == "cancel" || cek == "rejected") {
    $("#inputRemarks").focus();

    document.getElementById("inputRemarks").style.border = "1px solid red";
  }
});

// BATAS READY

$(document).on("click", ".modalEditPackage", function () {
  var id = $(this).data("id");
  var type = $(this).data("type");
  var package = $(this).data("package");
  $("#typeId").val(id);
  $(".editPackage").val(package);
  $("#selectType").val(type);
});

$(document).on("click", ".modalChangeFoto", function () {
  var id = $(this).data("id");
  var mode = $(this).data("mode");
  var foto = $(this).data("foto");

  $(".idChageFoto").val(id);
  if (mode == "kedai") {
    $(".changeFotoView").html(
      '<img src="assets/images/kedai/' + foto + '" width="250px">'
    );
  } else if (mode == "individu") {
    $(".changeFotoView").html(
      '<img src="assets/images/sales/' + foto + '" width="250px">'
    );
  }
});

$(".changeFoto").change(function () {
  changeFoto(this);
});

function changeFoto(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $(".changeFotoView").html(
        '<img src="' +
          e.target.result +
          '" width="250px" style="border: 1px solid #3f4d67; margin-bottom: 10px;">'
      );
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function imageSales(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#viewImageSales").attr("src", e.target.result).attr("width", 150);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".imageKedai").change(function () {
  imageKedai(this);

  $("#messageImageKedai").html(
    "<i class='feather icon-image' style='color: #04a9f5;'></i> <font style='font-weight: bold;'>Image ready to upload..</font>"
  );
});

function imageKedai(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#viewImageKedai").attr("src", e.target.result).attr("width", 150);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".imageIndividu").change(function () {
  imageIndividu(this);

  $("#messageImageIndividu").html(
    "<i class='feather icon-image' style='color: #04a9f5;'></i> <font style='font-weight: bold;'>Image ready to upload..</font>"
  );
});

function imageIndividu(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#viewImageIndividu").attr("src", e.target.result).attr("width", 150);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".imageAnnc").change(function () {
  imageAnnc(this);
});

function imageAnnc(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#messageImageAnnouncement").html(
        '<img src="' +
          e.target.result +
          '" width="300px" style="border: 1px solid #3f4d67; margin-bottom: 10px;">'
      );
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function uploadReceiptPaid(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#uploadReceiptTeks").html(
        '<i class="feather icon-check-circle" style="color: #04a9f5;"></i> File ready to upload..'
      );
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".uploadReceiptPaid").change(function () {
  uploadReceiptPaid(this);
});

function favicon(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#favicon")
        .attr("src", e.target.result)

        .attr("width", 100)

        .attr("height", 100);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".favicon").change(function () {
  favicon(this);
});

function logo(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#logo")
        .attr("src", e.target.result)

        .attr("width", 100)

        .attr("height", 100);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".logo").change(function () {
  logo(this);
});

function logoLanding(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#logoLanding").attr("src", e.target.result).attr("width", 250);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".logoLanding").change(function () {
  logoLanding(this);
});

function landingBackground(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $("#landingBackground").attr("src", e.target.result).attr("width", 510);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(".landingBackground").change(function () {
  landingBackground(this);
});

$(document).on("click", ".btn-edit-pass", function () {
  var id = $(this).data("id");
  var user = $(this).data("user");
  $("#editPassword").val(id);
  $("#username").val(user);
});

$(document).on("click", ".modalDelete", function () {
  var mode = $(this).data("mode");
  var key = $(this).data("key");
  $(".mode").val(mode);
  $(".key").val(key);
  $("#modalDelete").modal("show");
});

function insAnnc() {
	// $.post('ins-announcement', { id:id }, function(response) {});
	$.post("ins-announcement", function (response) {});
}
